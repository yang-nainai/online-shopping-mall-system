package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yangdacheng
 * @title: WinXinEntity
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/22 7:09 下午
 */
@ApiModel(value = "微信实体类",description = "微信实体类")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WinXinEntity {
    @ApiModelProperty(value = "获取的access_token")
    private String access_token;
    @ApiModelProperty(value = "jsapi_ticket")
    private String ticket ;
    @ApiModelProperty(value = "noncestr")
    private String noncestr;
    @ApiModelProperty(value = "时间戳")
    private String timestamp;
    @ApiModelProperty(value = "str")
    private String str;
    @ApiModelProperty(value = "signature")
    private String signature;

}

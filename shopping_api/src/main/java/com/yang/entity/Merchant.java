package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商家表 (Merchant)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "商家表 (Merchant)实体类",description = "商家表 (Merchant)实体类")
public class Merchant implements Serializable {
    private static final long serialVersionUID = 985032738931454091L;
    /**
    * 商家编号
    */
    @ApiModelProperty(value = "商家编号")
    private String merchantId;
    /**
    * 商家名称
    */
    @ApiModelProperty(value = " 商家名称")
    private String merchantName;
    /**
    * 商家图标
    */
    @ApiModelProperty(value = "商家图标")
    private String merchantHomeImg;
    /**
    * 商家基本信息编号
    */
    @ApiModelProperty(value = "商家基本信息编号")
    private String userId;

    /**
     * 商家信息
     */
    @ApiModelProperty(value = "商家信息")
    private User user;
}
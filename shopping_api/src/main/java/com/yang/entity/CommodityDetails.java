package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 商品图片详情表(CommodityDetails)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@ApiModel(value = "商品图片详情表(CommodityDetails)实体类")
public class CommodityDetails implements Serializable {
    private static final long serialVersionUID = -80510401875703702L;
    /**
    * 商品详情图片编号
    */
    @ApiModelProperty(value = "商品详情图片编号")
    private String commodityDetailsId;
    /**
    * 商品编号
    */
    @ApiModelProperty(value = "商品编号")
    private String commodityId;
    /**
    * 商品详细图片1
    */
    @ApiModelProperty(value = "商品详细图片1")
    private String babyDetailImageOne;
    /**
    * 商品详细图片2
    */
    @ApiModelProperty(value = "商品详细图片2")
    private String babyDetailImageTwo;
    /**
    * 商品详细图片3
    */
    @ApiModelProperty(value = "商品详细图片3")
    private String babyDetailImageThree;
    /**
    * 商品详细图片4
    */
    @ApiModelProperty(value = "商品详细图片4")
    private String babyDetailImageFour;
    /**
    * 商品详细图片5
    */
    @ApiModelProperty(value = "商品详细图片5")
    private String babyDetailImageFive;
    /**
    * 商品详细图片6
    */
    @ApiModelProperty(value = "商品详细图片6")
    private String babyDetailImageSix;
    /**
    * 商品详细图片7
    */
    @ApiModelProperty(value = "商品详细图片7")
    private String babyDetailImageSeven;
    /**
    * 商品详细图片8
    */
    @ApiModelProperty(value = "商品详细图片8")
    private String babyDetailImageEight;
    /**
    * 商品详细图片9
    */
    @ApiModelProperty(value = "商品详细图片9")
    private String babyDetailImageNine;
}
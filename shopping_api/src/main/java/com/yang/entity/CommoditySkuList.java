package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 商品sku的集合 (CommoditySkuList)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@ApiModel(value = "商品sku的集合 (CommoditySkuList)实体类",description = "商品sku的集合 (CommoditySkuList)实体类")
public class CommoditySkuList implements Serializable {
    private static final long serialVersionUID = 822342280933096677L;
    /**
    * 商品sku集合的编号
    */
    @ApiModelProperty(value = "商品sku集合的编号")
    private String skuListId;
    /**
    * 商品sku颜色的编号
    */
    @ApiModelProperty(value = "商品sku颜色的编号")
    private String skuColorId;
    /**
    * 商品sku大小的编号
    */
    @ApiModelProperty(value = "商品sku大小的编号")
    private String skuSizeId;
    /**
    * 商品的编号
    */
    @ApiModelProperty(value = "商品的编号")
    private String commodityId;
    /**
    * 商品sku的价格
    */
    @ApiModelProperty(value = "商品sku的价格")
    private Float commodityPrice;
    /**
    * 商品对应的库存
    */
    @ApiModelProperty(value = "商品对应的库存")
    private Integer commodityStockNum;
    /**
    * 商品sku对应的商品名称
    */
    @ApiModelProperty(value = "商品sku对应的商品名称")
    private String commodityListName;

    /**
     * 商品 sku 颜色类
     */
    @ApiModelProperty(value = "商品 sku 颜色类")
    private CommoditySkuColor commoditySkuColor;

    /**
     * 商品 sku 大小类
     */
    @ApiModelProperty(value = "商品 sku 大小类")
    private CommoditySkuSize commoditySkuSize;
}
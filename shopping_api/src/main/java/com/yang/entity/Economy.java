package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 优惠表(Economy)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "优惠表(Economy)实体类",description = "优惠表(Economy)实体类")
public class Economy implements Serializable {
    private static final long serialVersionUID = -22992399062216158L;
    /**
    * 优惠价格编号
    */
    @ApiModelProperty(value = "优惠价格编号")
    private String economyId;
    /**
    * 优惠价格名称
    */
    @ApiModelProperty(value = "优惠价格名称")
    private String economyTagName;
    /**
    * 优惠价格背景颜色
    */
    @ApiModelProperty(value = "优惠价格背景颜色")
    private String economyBackground;
    /**
    * 优惠字体颜色
    */
    @ApiModelProperty(value = "优惠字体颜色")
    private String economyFontColor;
    /**
    * 优惠后的价格
    */
    @ApiModelProperty(value = "优惠后的价格")
    private Float economyPrice;
    /**
    * 优惠对象
    */
    @ApiModelProperty(value = "优惠对象")
    private String economyPerson;
}
package com.yang.entity.utils;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author yangdacheng
 * @title: FileUploadBean 文件上传bean
 * @projectName shopping_api
 * @description: TODO
 * @date 2022/4/14 11:19 上午
 */
@Component
@Data
@ApiModel(value = "首页轮播图类",description = "首页轮播图类")
public class FileUploadBean {
    @ApiModelProperty(name = "文件上传地址",value = "imgFileUrl")
    @Value("${fileUpload.imgFileUrl}")
    public String imgFileUrl; //文件上传地址
    @ApiModelProperty(name = "聊天文件上传地址",value = "messageFileUrl")
    @Value("${fileUpload.messageFileUrl}")
    public String messageFileUrl; //聊天文件上传地址
}

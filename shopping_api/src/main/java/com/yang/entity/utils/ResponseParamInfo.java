package com.yang.entity.utils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author yangdacheng
 * @title: ResponseParamInfo 返回消息类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/10/16 1:26 下午
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "ResponseParamInfo 返回消息类",description = "ResponseParamInfo 返回消息类")
@Accessors(chain = true)
public class ResponseParamInfo implements Serializable {
    /**
     * 返回状态码
     */
    @ApiModelProperty(value = "返回状态码")
    private int responseCode;

    /**
     * 返回消息
     */
    @ApiModelProperty(value = " 返回消息")
    private String responseText;

    /**
     * 返回数据
     */
    @ApiModelProperty(value = "返回数据")
    private Object responseData;
}

package com.yang.entity;

import com.yang.utils.ImageUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 商品的sku属性表（颜色）(CommoditySkuColor)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@ApiModel(value = "商品的sku属性表（颜色）(CommoditySkuColor)实体类",description = "商品的sku属性表（颜色）(CommoditySkuColor)实体类")
public class CommoditySkuColor implements Serializable {
    private static final long serialVersionUID = -60046689655149614L;
    /**
    * sku属性颜色编号
    */
    @ApiModelProperty(value = "sku属性颜色编号")
    private String skuColorId;
    /**
    * 商品sku的key名称（颜色）
    */
    @ApiModelProperty(value = "商品sku的key名称（颜色）")
    private String skuKeyName;
    /**
    * 商品sku对应的名称
    */
    @ApiModelProperty(value = "商品sku对应的名称")
    private String skuColorName;
    /**
    * 商品sku对应的图片
    */
    @ApiModelProperty(value = "商品sku对应的图片")
    private String skuImgUrl;
    /**
    * 商品sku对应的预览图片
    */
    @ApiModelProperty(value = "商品sku对应的预览图片")
    private String skuPreviewImgUrl;
    /**
    * 商品的编号
    */
    @ApiModelProperty(value = "商品的编号")
    private String commodityId;


    public CommoditySkuColor(String skuColorName) {
        this.skuColorName = skuColorName;
    }

    public CommoditySkuColor(String skuKeyName, String skuColorName, String skuImgUrl) {
        this.skuKeyName = skuKeyName;
        this.skuColorName = skuColorName;
        this.skuImgUrl = skuImgUrl;
    }
}
package com.yang.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * 商品表 用来记入一些商品的基本信息(Commodity)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@ApiModel(value = "商品表 用来记入一些商品的基本信息(Commodity)实体类",description = "商品表 用来记入一些商品的基本信息(Commodity)实体类")
public class Commodity implements Serializable {
    private static final long serialVersionUID = 784627099445371158L;
    /**
    * 商品编号
    */
    @ApiModelProperty(value = "商品编号")
    private String commodityId;
    /**
    * 商品名称
    */
    @ApiModelProperty(value = "商品名称")
    private String commodityName;
    /**
    * 商品销量
    */
    @ApiModelProperty(value = "商品销量")
    private Integer commoditySales;
    /**
    * 商品创建时间
    */
    @ApiModelProperty(value = "商品创建时间")
    private Date commodityBirthTime;


    /**
     * 商品 类型
     */
    @ApiModelProperty(value = "商品 类型")
    private List<CommodityType> commodityTypeList;
    /**
     * 商品详情图片
     */
    @ApiModelProperty(value = "商品详情图片")
    private CommodityDetails commodityDetails;
    /**
     * 商品展示图片
     */
    @ApiModelProperty(value = "商品展示图片")
    private CommodityShow commodityShow;

    /**
     * 商品的 sku 属性 颜色
     */
    @ApiModelProperty(value = "商品的 sku 属性 颜色")
    private List<CommoditySkuColor> commoditySkuColorList;

    /**
     * 商品的 sku 属性 大小
     */
    @ApiModelProperty(value = "商品的 sku 属性 大小")
    private List<CommoditySkuSize> commoditySkuSizeList;

    /**
     * 商品的 sku 组合
     */
    @ApiModelProperty(value = "商品的 sku 组合")
    private List<CommoditySkuList> commoditySkuListList;

    /**
     * 商品的 标签
     */
    @ApiModelProperty(value = "商品的 标签")
    private CommodityTag commodityTag;

    /**
     * 商家 商品
     */
    @ApiModelProperty(value = "商家 商品")
    private MerchantBaby merchantBaby;

    /**
     * 商家信息
     */
    @ApiModelProperty(value = "商家信息")
    private Merchant merchant;

    public Commodity(String commodityId, String commodityName, Integer commoditySales, Date commodityBirthTime) {
        this.commodityId = commodityId;
        this.commodityName = commodityName;
        this.commoditySales = commoditySales;
        this.commodityBirthTime = commodityBirthTime;
    }
}
package com.yang.entity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户订单表(OrderUser)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "用户订单表(OrderUser)实体类",description = "用户订单表(OrderUser)实体类")
public class OrderUser implements Serializable {
    private static final long serialVersionUID = 108647863970119234L;
    /**
    * 用户订单表
    */
    @ApiModelProperty(value = " 用户订单表")
    private String userOrderId;
    /**
    * 对应的商家商品编号（可获取商品信息和商家信息）
    */
    @ApiModelProperty(value = "对应的商家商品编号（可获取商品信息和商家信息）")
    private String merchantBabyId;
    /**
    * 获取用户地址信息（配送地址）
    */
    @ApiModelProperty(value = "获取用户地址信息（配送地址）")
    private String userAddressId;
    /**
    * 订单编号
    */
    @ApiModelProperty(value = "订单编号")
    private String orderId;
    /**
     * 商品编号
     */
    @ApiModelProperty(value = "商品编号")
    private String commodityId;
    /**
     * 商品sku list编号
     */
    @ApiModelProperty(value = "商品sku list编号")
    private String commodityListId;

    /**
     * 用户编号 用来判断是谁的订单
     */
    @ApiModelProperty(value = "用户编号 用来判断是谁的订单")
    private String userId;

    /**
     * 基础订单信息
     */
    @ApiModelProperty(value = "基础订单信息")
    private OrderBase orderBase;
    /**
     * 商品信息
     */
    @ApiModelProperty(value = "商品信息")
    private MerchantBaby merchantBaby;
    /**
     * 地址信息
     */
    @ApiModelProperty(value = "地址信息")
    private UserAddress userAddress;
    /**
     * 商品sku信息
     */
    @ApiModelProperty(value = "商品sku信息")
    private CommoditySkuList commoditySkuList;

    /**
     * redis key 过期时间
     */
    @ApiModelProperty(value = "redis key 过期时间")
    private Long pastTime;

    /**
     * 商家编号
     */
    @ApiModelProperty(value = "商家编号")
    private String merchantID;

    public OrderUser(String userOrderId, String merchantBabyId, String userAddressId, String orderId, String commodityId,
                     String commodityListId, String userId,String merchantID) {
        this.userOrderId = userOrderId;
        this.merchantBabyId = merchantBabyId;
        this.userAddressId = userAddressId;
        this.orderId = orderId;
        this.commodityId = commodityId;
        this.commodityListId = commodityListId;
        this.userId = userId;
        this.merchantID = merchantID;
    }
}
package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商品标签表(CommodityTag)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "商品标签表(CommodityTag)实体类",description = "商品标签表(CommodityTag)实体类")
public class CommodityTag implements Serializable {
    private static final long serialVersionUID = 240535724108909902L;
    /**
    * 商品标签编号
    */
    @ApiModelProperty(value = "商品标签编号")
    private String commodityTagId;
    /**
    * 商品编号
    */
    @ApiModelProperty(value = "商品编号")
    private String commodityId;
    /**
    * 标签编号
    */
    @ApiModelProperty(value = "标签编号")
    private String tagId;

    /**
     * 标签类
     */
    @ApiModelProperty(value = "标签类")
    private Tag tag;
}
package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 订单基础表(OrderBase)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "订单基础表(OrderBase)实体类",description = "订单基础表(OrderBase)实体类")
public class OrderBase implements Serializable {
    private static final long serialVersionUID = -58085549480634693L;
    /**
    * 订单编号
    */
    @ApiModelProperty(value = "订单编号")
    private String orderId;
    /**
    * 订单当前状态（待付款....）
    */
    @ApiModelProperty(value = "订单当前状态（待付款....）")
    private String orderState;
    /**
    * 购买商品数量
    */
    @ApiModelProperty(value = " 购买商品数量")
    private Integer orderBuyNum;
    /**
    * 购买商品所需要支付的钱
    */
    @ApiModelProperty(value = "购买商品所需要支付的钱")
    private Float orderNeedPay;
    /**
    * 购买商品实际支付的钱
    */
    @ApiModelProperty(value = "购买商品实际支付的钱")
    private Float orderPay;
    /**
    * 商品的支付状态（未支付....）
    */
    @ApiModelProperty(value = "商品的支付状态（未支付....")
    private String orderPayState;
    /**
    * 商品支付的方式（微信支付/支付宝支付...）
    */
    @ApiModelProperty(value = "商品支付的方式（微信支付/支付宝支付...）")
    private String orderPayMethod;
}
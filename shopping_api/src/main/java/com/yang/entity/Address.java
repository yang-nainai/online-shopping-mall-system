package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 收货人&地址表(Address)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:56
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "收货人&地址表(Address)实体类",description = "收货人&地址表(Address)实体类")
public class Address implements Serializable {
    private static final long serialVersionUID = -29975083618931741L;
    /**
    * 地址编号
    */
    @ApiModelProperty(value = "地址编号")
    private String addressId;
    /**
    * 地址省份
    */
    @ApiModelProperty(value = "地址省份")
    private String addressProvince;
    /**
    * 地址城市
    */
    @ApiModelProperty(value = "地址城市")
    private String addressCity;
    /**
    * 地址县/区
    */
    @ApiModelProperty(value = "地址县/区")
    private String addressCounty;
    /**
    * 详细地址
    */
    @ApiModelProperty(value = "详细地址")
    private String addressDetailed;
    /**
    * 收货人姓名
    */
    @ApiModelProperty(value = "收货人姓名")
    private String addressPerson;
    /**
    * 收货人电话
    */
    @ApiModelProperty(value = "收货人电话")
    private String addressTelephone;
}
package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商品类型编号 用来存放一些商品类型(Type)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "商品类型编号 用来存放一些商品类型(Type)实体类",description = "商品类型编号 用来存放一些商品类型(Type)实体类")
public class Type implements Serializable {
    private static final long serialVersionUID = 984189083959780619L;
    /**
    * 商品类型编号
    */
    @ApiModelProperty(value = "商品类型编号")
    private String typeId;
    /**
    * 商品类型名称
    */
    @ApiModelProperty(value = "商品类型名称")
    private String typeName;
}
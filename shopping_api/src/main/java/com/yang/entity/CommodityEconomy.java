package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商品优惠价格(CommodityEconomy)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "商品优惠价格(CommodityEconomy)实体类",description = "商品优惠价格(CommodityEconomy)实体类")
public class CommodityEconomy implements Serializable {
    private static final long serialVersionUID = 344825839434298265L;
    /**
    * 商品优惠编号
    */
    @ApiModelProperty(value = "商品优惠编号")
    private String commodityEconomyId;
    /**
    * 商品编号
    */
    @ApiModelProperty(value = "商品编号")
    private String commodityId;
    /**
    * 优惠价格编号
    */
    @ApiModelProperty(value = "优惠价格编号")
    private String economyId;
}
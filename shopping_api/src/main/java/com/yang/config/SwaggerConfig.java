package com.yang.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author yangdacheng
 * @title: SwaggerConfig
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/22 3:41 下午
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("阳奈")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yang.controller"))
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfoBuilder()
                .description("little pig 接口测试文档")
                .contact(new Contact("阳奈","http://littlepig.xyz:5209",
                        "2465047559@qq.com"))
                .version("v1.0")
                .title("API接口文档")
                .license("little pig项目")
                .licenseUrl("http://littlepig.xyz:5209").build());
    }
}

package com.yang.config;

import com.yang.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

/**
 * @author yangdacheng
 * @title: MySecurity
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/10/9 11:10 上午
 */
@Configuration
public class MySecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserServiceImpl userService;



    /**
     * 加密操作
     */
    @Bean
    PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    /**
     *  授权
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService);
    }


    /**
     * 认证
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //设置不拦截首页登录页以及静态资源
        http.authorizeRequests()
                .antMatchers("/alipay/**").permitAll()
                .antMatchers("/commodity/**").permitAll()
                .antMatchers("/orderUser/**").permitAll()
                .antMatchers("/userAddress/**").permitAll()
                .antMatchers("/userCart/**").permitAll()
                .antMatchers("/user/**").permitAll()
                .antMatchers("/searchHistory/**").permitAll()
                .antMatchers("/weiXin/**").permitAll()
                .antMatchers("/weChart/**").permitAll()
                .antMatchers("/message/**").permitAll()
                .antMatchers("/a/**").permitAll()
                .antMatchers("/es/**").permitAll()
                .antMatchers("/management/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .cors()
                .and()
                .csrf()
                .disable();
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS,"/**");
    }

    @Bean
    public HttpFirewall httpFirewall() {
        return new DefaultHttpFirewall();
    }
}

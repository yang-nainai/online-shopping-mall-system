package com.yang.utils.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author yangdacheng
 * @title: InputNumberInfo
 * @projectName shopping_api
 * @description: TODO
 * @date 2022/5/27 8:55 上午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class InputNumberInfo {
    private String inputLabel;      //标签名称
    private Integer inputValue;     //标签数量
    private Float inputMoney;       //购买价格
}

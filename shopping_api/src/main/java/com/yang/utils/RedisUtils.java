package com.yang.utils;

import com.alibaba.fastjson.JSONObject;
import com.yang.entity.UserSearchHistory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdacheng
 * @title: RedisUtils redis工具
 * @projectName shopping_api
 * @description: TODO
 * @date 2022/4/15 9:11 上午
 */
@Component
public class RedisUtils {

    @Resource
    private RedisTemplate redisTemplate;


    public void insertNewSearchItem(String userID,String searchName){
        /**
         * 1、将用户搜索的记录 添加到 redis 缓存中
         * 2、去 mysql 数据库中查询用户搜索的记录 模糊查询
         * 3、将获取的数据返回出去
         */
        UserSearchHistory userSearchHistory = new UserSearchHistory(userID, searchName);
        String strUserSearchHistory = JSONObject.toJSONString(userSearchHistory); //将对象转换为字符串
        String redisKey = userID + "search"; //redis key
        //查看缓存中是否在这个搜索记录 存在就移除 并且在头部添加
        List<String> userSearchList = redisTemplate.opsForList().range(redisKey, 0, -1);
        List<UserSearchHistory> userSearchHistories = new ArrayList<>(); //用来存放在redis 读取到的集合
        List<UserSearchHistory> insertRedis = new ArrayList<>(); //添加到redis集合中
        for (int index = 0; index < userSearchList.size(); index++) {
            UserSearchHistory listHistory = JSONObject.parseObject(userSearchList.get(index), UserSearchHistory.class);
            if (!listHistory.getUserSearchHistory().equals(searchName)) {
                userSearchHistories.add(listHistory);
            }
        }
        //没有重复的元素
        if (userSearchHistories.size() == userSearchList.size()){
            //获取缓存中的key 如果长度大于10个就移除最后一个
            Long searchSize = redisTemplate.opsForList().size(redisKey);  //当前搜索记录大小
            //如果长度为10个就需要移除最右边的元素
            if (searchSize == 10) {
                //redisTemplate.opsForList().rightPop(redisKey);
                redisTemplate.opsForList().rightPopAndLeftPush(redisKey, strUserSearchHistory);
            } else {
                redisTemplate.opsForList().leftPush(redisKey, strUserSearchHistory); //存放到缓存中去
            }
        }else{
            //删除原有的redis值
            redisTemplate.delete(redisKey);
            //如果用户搜索的值存在在原有的搜索集合中
            insertRedis.add(userSearchHistory); //将用户新收拾的添加到头部
            //将原来集合中的内容添加到新的集合
            insertRedis.addAll(userSearchHistories);
            //将现在的所有值添加到搜索中
            for (UserSearchHistory history : insertRedis) {
                String userHistory = JSONObject.toJSONString(history);
                redisTemplate.opsForList().rightPush(redisKey, userHistory);
            }
        }
    }
}

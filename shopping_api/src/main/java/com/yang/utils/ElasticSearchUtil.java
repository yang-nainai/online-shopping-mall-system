package com.yang.utils;

import com.alibaba.fastjson.JSON;
import com.yang.entity.Commodity;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

/**
 * @author yangdacheng
 * @title: ElasticSearchUtil  es工具类
 * @projectName shopping_api
 * @description: TODO
 * @date 2022/4/13 9:23 下午
 */
@Component
public class ElasticSearchUtil {

    @Resource
    private RestHighLevelClient client;


    /**
     * 创建Index索引
     * @param indexName 索引名称
     * @return 判断是否创建成功
     * @throws IOException 文件异常
     * 测试： 成功
     */
    public Boolean createEsIndex(String indexName) throws IOException {
        CreateIndexRequest request = new CreateIndexRequest(indexName);
        CreateIndexResponse response = client.indices().create(request, RequestOptions.DEFAULT);
        System.out.println(response);
        return response.isAcknowledged();
    }

    /**
     * 判断索引是否存在
     * 测试： 成功
     * @param indexName 索引名称
     * @return 判断索引是否存在
     * @throws IOException 文件异常
     */
    public Boolean existIndex(String indexName) throws IOException {
        GetIndexRequest request = new GetIndexRequest(indexName);
        return client.indices().exists(request, RequestOptions.DEFAULT);
    }

    /**
     * 删除索引
     * 测试： 成功
     * @param indexName 索引名称
     * @return 是否删除成功
     * @throws IOException 文件异常
     */
    public Boolean deleteIndex(String indexName) throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest(indexName);
        AcknowledgedResponse delete = client.indices().delete(request, RequestOptions.DEFAULT);
        return delete.isAcknowledged();
    }

    /**
     * 将数据添加到索引中去 -- 添加多条
     * @param indexName 索引名称
     * @param commodities 商品名称
     * @throws IOException 文件异常
     */
    public Boolean insertListDocument(String indexName, List<Commodity> commodities) throws IOException {
        BulkRequest request = new BulkRequest();
        request.timeout(TimeValue.timeValueSeconds(60)); //设置超时时间
        request.timeout("60s");
        for (int index = 0; index < commodities.size(); index++) {
            IndexRequest indexRequest = new IndexRequest().index(indexName);
            indexRequest.id(commodities.get(index).getCommodityId());
            IndexRequest source = indexRequest.source(JSON.toJSONString(commodities.get(index)), XContentType.JSON);
            System.out.println(source);
            request.add(indexRequest);
        }
        BulkResponse response = client.bulk(request, RequestOptions.DEFAULT);
        return !response.hasFailures();
    }

    /**
     * 将数据添加到索引中去 -- 添加一条
     * @param indexName
     * @param commodity
     * @throws IOException
     */
    public void insertDocument(String indexName,Commodity commodity) throws IOException{
        IndexRequest request = new IndexRequest(indexName);
        request.id(commodity.getCommodityId()); ////设置编号
        request.timeout(TimeValue.timeValueSeconds(30)); //设置超时时间
        request.timeout("30s");
        request.source(JSON.toJSONString(commodity),XContentType.JSON); //设置格式
        IndexResponse response = client.index(request, RequestOptions.DEFAULT);
        System.out.println(response.status());
    }

    /**
     * 判断文档是否存在
     * @param indexName 索引名称
     * @param documentID 文档编号
     * @return
     * @throws IOException
     */
    public Boolean existDocument(String indexName,String documentID) throws IOException {
        GetRequest request = new GetRequest(indexName);
        request.fetchSourceContext(new FetchSourceContext(false)); //不放回上下文
        return client.exists(request, RequestOptions.DEFAULT);
    }

    /**
     * 更新文档信息
     * @param indexName 所有下标
     * @param documentID 文档编号
     * @param object 更新类型
     * @return 是否成功
     * @throws IOException
     */
    public Boolean updDocument(String indexName,String documentID,Object object) throws IOException {
        UpdateRequest request = new UpdateRequest(indexName,documentID);
        request.timeout(TimeValue.timeValueSeconds(30));
        request.doc(JSON.toJSONString(object),XContentType.JSON);
        UpdateResponse response = client.update(request, RequestOptions.DEFAULT);
        return response.status().getStatus() == 200;
    }

    /**
     * 删除文档 -- 根据编号
     * @param indexName
     * @param documentID
     * @return
     * @throws IOException
     */
    public Boolean deleteDocument(String indexName,String documentID) throws IOException {
        DeleteRequest request = new DeleteRequest(indexName);
        request.id(documentID);
        DeleteResponse response = client.delete(request, RequestOptions.DEFAULT);
        return response.status().getStatus() == 200;
    }

    /**
     * 根据对应的key 获取对应的 value 测试：成功
     * @param indexName 索引名称
     * @param searchKey 搜索key
     * @param searchValue 搜索的值
     * @param highLight 是否高亮
     * @throws IOException
     */
    public List<Object> searchObject(String indexName, String searchKey, String searchValue, Boolean highLight) throws IOException {
        List<Object> objects = new ArrayList<>();
        SearchRequest request = new SearchRequest(indexName);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder(searchKey,searchValue);
        searchSourceBuilder.query(matchQueryBuilder);
        if (highLight){
            HighlightBuilder highlightBuilder = new HighlightBuilder();
            highlightBuilder.preTags("<span style='color:#ff4400'>");
            highlightBuilder.postTags("</span>");
            highlightBuilder.field(searchKey);
            highlightBuilder.requireFieldMatch(true); //设置多个高亮
            searchSourceBuilder.highlighter(highlightBuilder);
        }
        request.source(searchSourceBuilder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            if (highLight){
                //解析高亮
                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                HighlightField title = highlightFields.get(searchKey);
                if (!Objects.isNull(title)){
                    Text[] fragments = title.fragments();
                    String new_title = "";
                    for (Text text : fragments) {
                        new_title += text;
                    }
                    sourceAsMap.put(searchKey,new_title);
                }
            }
            objects.add(sourceAsMap);
        }
        return objects;
    }

}

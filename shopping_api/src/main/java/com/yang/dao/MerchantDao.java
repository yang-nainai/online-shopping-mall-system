package com.yang.dao;

import com.yang.entity.Merchant;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 商家表
(Merchant)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface MerchantDao {

    /**
     * 根据商家编号获取商家信息
     * @param merchantId 商家编号
     * @return 商家信息
     */
    @Select("SELECT * FROM merchant WHERE merchant_id = #{merchantId}")
    @Results(value = {
            @Result(property = "merchantId",column = "merchant_id",id = true),
            @Result(property = "merchantName",column = "merchant_name"),
            @Result(property = "merchantHomeImg",column = "merchant_home_img"),
            @Result(property = "userId",column = "user_id"),
            @Result(property = "user",column = "user_id",one = @One(
                    select = "com.yang.dao.UserDao.queryById"
            )),
    },id = "merchantMap")
    Merchant queryMerchantById(String merchantId);

    /**
     * 根据用户编号获取商家信息
     * @param userID
     * @return
     */
    @Select("SELECT * FROM merchant WHERE user_id = #{userID}")
    @ResultMap("merchantMap")
    Merchant queryMerchantByUserID(String userID);

    /**
     * 根据商家编号修改商家信息
     * @param merchant
     * @return
     */
    @Update("UPDATE merchant SET " +
            "merchant_name = #{merchantName},merchant_home_img = #{merchantHomeImg} WHERE " +
            "merchant_id = #{merchantId}")
    Integer updateMerchant(Merchant merchant);
}
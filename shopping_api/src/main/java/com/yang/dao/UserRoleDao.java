package com.yang.dao;

import com.yang.entity.UserRole;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 用户角色表
(UserRole)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface UserRoleDao {

    /**
     * 根基用户编号获取用户角色信息
     * @param userId
     * @return
     */
    @Select("SELECT * FROM user_role WHERE user_role_uid = #{userId}")
    @Results({
            @Result(property = "userRoleId",column = "user_role_id",id = true),
            @Result(property = "userRoleUid",column = "user_role_uid"),
            @Result(property = "userRoleRid",column = "user_role_rid"),
            @Result(property = "role",column = "user_role_rid",one = @One(
                    select = "com.yang.dao.RoleDao.queryByRid"
            )),
    })
    UserRole queryByUserId(String userId);

    /**
     * 给新添加的用户添加一个角色
     * @param userRole 用户角色
     * @return 1：成功 0：失败
     */
    @Insert("INSERT INTO user_role(user_role_id,user_role_uid,user_role_rid) " +
            "VALUES (#{userRoleId},#{userRoleUid},#{userRoleRid})")
    Integer insertUserRole(UserRole userRole);
}
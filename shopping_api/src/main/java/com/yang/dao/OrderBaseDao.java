package com.yang.dao;

import com.yang.entity.OrderBase;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 订单基础表(OrderBase)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface OrderBaseDao {

    /**
     * 根据编号获取基础订单
     * @param orderBaseId 订单编号
     * @return 订单类
     */
    @Select("SELECT * FROM order_base WHERE order_id = #{orderBaseId}")
    @Results(value = {
            @Result(property = "orderId",column = "order_id",id = true),
            @Result(property = "orderState",column = "order_state"),
            @Result(property = "orderBuyNum",column = "order_buy_num"),
            @Result(property = "orderNeedPay",column = "order_need_pay"),
            @Result(property = "orderPay",column = "order_pay"),
            @Result(property = "orderPayState",column = "order_pay_state"),
            @Result(property = "orderPayMethod",column = "order_pay_method"),
    },id = "baseOrderMap")
    OrderBase queryById(String orderBaseId);

    /**
     * 根据订单状态获取订单
     * @param orderState
     * @return
     */
    @Select("SELECT * FROM order_base WHERE order_state = #{orderState}")
    @ResultMap(value = "baseOrderMap")
    List<OrderBase> queryByOrderState(String orderState);


    /**
     * 添加一个新的订单编号
     * @param orderBase 订单类
     * @return 1： 添加成功 0：添加失败
     */
    @Insert("INSERT INTO " +
            "order_base(order_id,order_state,order_buy_num,order_need_pay,order_pay,order_pay_state,order_pay_method) " +
            "VALUES(#{orderId},#{orderState},#{orderBuyNum},#{orderNeedPay},#{orderPay},#{orderPayState},#{orderPayMethod})")
    Integer insertOrder(OrderBase orderBase);

    /**
     * 根据用户编号和订单编号 修改订单状态
     * @param orderBaseID 订单编号
     * @param orderState 订单状态
     * @return  1：修改成功 0：修改失败
     */
    @Update("UPDATE order_base SET " +
            "order_state = #{orderState} WHERE order_id = #{orderBaseID}")
    Integer updateOrderState(String orderBaseID,String orderState);
}
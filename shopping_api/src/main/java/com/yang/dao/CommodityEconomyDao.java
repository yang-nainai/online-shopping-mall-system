package com.yang.dao;

import com.yang.entity.CommodityEconomy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 商品优惠价格(CommodityEconomy)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommodityEconomyDao {

}
package com.yang.dao;

import com.yang.entity.Type;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 商品类型编号 用来存放一些商品类型(Type)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface TypeDao {

    /**
     * 通过ID查询单条数据
     *
     * @param typeId 主键
     * @return 实例对象
     */
    @Select("SELECT * FROM type WHERE type_id = #{typeId}")
    @Results(value = {
            @Result(property = "typeId", column = "type_id",id = true),
            @Result(property = "typeName", column = "type_name")
    },id = "typeMap")
    Type queryById(String typeId);

    /**
     * 查询所有的商品类型
     * @return
     */
    @Select("SELECT * FROM type")
    @ResultMap("typeMap")
    List<Type> queryAllType();
}
package com.yang.dao;

import com.yang.entity.Address;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 收货人&地址表(Address)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:56
 */
@Mapper
public interface  AddressDao {

    /**
     * 根据地址的编号获取地址信息
     * @param addressId 地址编号
     * @return 地址对象
     */
    @Select("SELECT * FROM address WHERE address_id = #{addressId}")
    @Results({
            @Result(property = "addressId",column = "address_id",id = true),
            @Result(property = "addressProvince",column = "address_province"),
            @Result(property = "addressCity",column = "address_city"),
            @Result(property = "addressCounty",column = "address_county"),
            @Result(property = "addressDetailed",column = "address_detailed"),
            @Result(property = "addressPerson",column = "address_person"),
            @Result(property = "addressTelephone",column = "address_telephone")
    })
    Address queryById(String addressId);

    /**
     * 添加一个新的地址
     * @param address 地址对象
     * @return 如果为1添加成功
     */
    @Insert("INSERT INTO " +
            "address(address_id,address_province,address_city,address_county,address_detailed," +
            "address_person,address_telephone) " +
            "VALUES (#{addressId},#{addressProvince},#{addressCity},#{addressCounty},#{addressDetailed}," +
            "#{addressPerson},#{addressTelephone})")
    Integer insertAddress(Address address);

    /**
     * 修改地址信息
     * @param address 地址类
     * @return 1:修改成功 0修改失败
     */
    @Update("UPDATE address SET " +
            "address_province = #{addressProvince} ,address_city = #{addressCity} ," +
            "address_county = #{addressCounty} ,address_detailed = #{addressDetailed} , " +
            "address_person= #{addressPerson} , address_telephone = #{addressTelephone} " +
            "WHERE address_id = #{addressId}")
    Integer updateAddress(Address address);

    /**
     * 删除地址信息
     * @param addressId 地址编号
     * @return 1：删除成功 0：删除失败
     */
    @Delete("DELETE FROM address WHERE address_id = #{addressId}")
    Integer deleteAddress(String addressId);
}
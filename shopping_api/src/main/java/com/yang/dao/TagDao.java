package com.yang.dao;

import com.yang.entity.Tag;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 标签表
(Tag)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface TagDao {

    /**
     * 根据标签的编号 获取标签内容
     * @param tagId 标签
     * @return 标签属性
     */
    @Select("SELECT * FROM tag WHERE tag_id = #{tagId}")
    @Results({
            @Result(property = "tagId", column = "tag_id", id = true),
            @Result(property = "tagName", column = "tag_name"),
            @Result(property = "tagBackground", column = "tag_background"),
            @Result(property = "tagFontColor", column = "tag_font_color"),
    })
    Tag queryById(String tagId);

}
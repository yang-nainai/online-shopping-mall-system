package com.yang.controller;

import com.yang.entity.Commodity;
import com.yang.entity.utils.PageInfo;
import com.yang.service.CommodityService;
import com.yang.utils.ElasticSearchUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * @author yangdacheng
 * @title: AATest
 * @projectName shopping_api
 * @description: TODO
 * @date 2022/4/14 3:02 下午
 */
@RestController
@RequestMapping("/es")
@Slf4j
public class ElasticSearchController {

    @Resource
    private ElasticSearchUtil util;
    @Resource
    private CommodityService commodityService;
    @Value("${myElasticSearch.index_name}")
    private String index_name;

    @RequestMapping(value = "/addAll",method = RequestMethod.GET)
    private Boolean addList(){
        log.info("index_name:"+index_name);
        Boolean insertResult = false; //添加的结果
        List<Commodity> commodities = commodityService.queryAllCommodity();
        try {
            if (!util.existIndex(index_name)){
                util.createEsIndex(index_name); //如果索引不存在就添加索引
            }
            insertResult = util.insertListDocument(index_name, commodities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return insertResult;
    }
}

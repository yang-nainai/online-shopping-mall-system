package com.yang.controller;

import com.yang.entity.CommoditySkuSize;
import com.yang.service.CommoditySkuSizeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品的SKU属性--大小(CommoditySkuSize)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("commoditySkuSize")
public class CommoditySkuSizeController {
    /**
     * 服务对象
     */
    @Resource
    private CommoditySkuSizeService commoditySkuSizeService;

}
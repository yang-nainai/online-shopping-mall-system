package com.yang.controller;

import com.yang.entity.CommodityDetails;
import com.yang.service.CommodityDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品图片详情表(CommodityDetails)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("commodityDetails")
public class CommodityDetailsController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityDetailsService commodityDetailsService;

}
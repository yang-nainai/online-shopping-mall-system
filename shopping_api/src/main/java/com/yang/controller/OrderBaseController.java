package com.yang.controller;

import com.yang.entity.OrderBase;
import com.yang.service.OrderBaseService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 订单基础表(OrderBase)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@RestController
@RequestMapping("orderBase")
public class OrderBaseController {
    /**
     * 服务对象
     */
    @Resource
    private OrderBaseService orderBaseService;

}
package com.yang.controller;

import com.yang.entity.CommoditySkuList;
import com.yang.service.CommoditySkuListService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品sku的集合
(CommoditySkuList)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("commoditySkuList")
public class CommoditySkuListController {
    /**
     * 服务对象
     */
    @Resource
    private CommoditySkuListService commoditySkuListService;

}
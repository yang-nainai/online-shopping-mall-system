package com.yang.controller;

import com.yang.entity.CommodityTag;
import com.yang.service.CommodityTagService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品标签表(CommodityTag)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("commodityTag")
public class CommodityTagController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityTagService commodityTagService;

}
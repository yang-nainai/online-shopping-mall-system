package com.yang.controller.admin;

import com.yang.entity.CommodityType;
import com.yang.entity.Type;
import com.yang.service.CommodityTypeService;
import com.yang.service.TagService;
import com.yang.service.TypeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品类型表用来存放不同商品的类型(CommodityType)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController("ManagementCommodityTypeController")
@RequestMapping("/management/commodityType")
public class CommodityTypeController {
    /**
     * 服务对象
     */
    @Resource
    private TypeService typeService;

    @GetMapping("/queryAllType")
    private List<Type> queryAllType(){
        return typeService.queryAllType();
    }
}
package com.yang.controller.admin;

import com.yang.entity.Merchant;
import com.yang.entity.User;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.MerchantService;
import com.yang.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 商家表
(Merchant)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController("ManagementMerchantController")
@RequestMapping("/management/merchant")
@Slf4j
public class MerchantController {
    /**
     * 服务对象
     */
    @Resource
    private MerchantService merchantService;
    @Resource
    private UserService userService;

    /**
     * 管理账号登陆
     * @param username
     * @param password
     * @return
     */
    @PostMapping(value = "merchantLogin")
    public ResponseParamInfo merchantLogin(@RequestParam("username") String username,
                                           @RequestParam("password") String password){
        ResponseParamInfo responseParamInfo = new ResponseParamInfo();
        log.info("管理员账号 === 》 username:{},password:{}",username,password);
        User user = userService.userLogin(username, password);  //判断是否为正常的用户
        if(Objects.isNull(user)){
            log.info("管理员账号 === 》 用户名或密码错误");
            responseParamInfo.setResponseCode(201)
                    .setResponseText("账号或密码错误～")
                    .setResponseData(null);
            return responseParamInfo;
        }
        Merchant merchant = merchantService.queryMerchantByUserID(user.getUserId());
        if (Objects.isNull(merchant)){
            responseParamInfo.setResponseCode(201)
                    .setResponseText("您不是对应的商家，无法进入～")
                    .setResponseData(null);
            return responseParamInfo;
        }
        log.info("管理员账号 === 》 商家信息：{}",merchant);
        responseParamInfo.setResponseCode(200)
                .setResponseText("登陆成功")
                .setResponseData(merchant);
        return responseParamInfo;
    }


    /**
     * 根据用户编号获取用户信息
     * @param userId
     * @return
     */
    @GetMapping("queryUserByID/{userID}")
    public User queryUserByID(@PathVariable("userID") String userId){
        return userService.queryById(userId);
    }

    @PutMapping("merchantUpdate")
    public Map<String,Object> merchantUpdate(Merchant merchant,User user){
        log.info("商家信息 === 》 {}",merchant);
        log.info("用户信息 === 》 {}",user);
        Map<String,Object> resultMap = new HashMap<>();
        Integer merchantResult = merchantService.updateMerchant(merchant);
        if (merchantResult <= 0){
            resultMap.put("code",201);
            resultMap.put("message","修改店铺信息失败!");
            return resultMap;
        }
        Integer userResult = userService.updateUser(user);
        if (userResult <= 0){
            resultMap.put("code",201);
            resultMap.put("message","修改用户信息失败!");
            return resultMap;
        }
        resultMap.put("code",200);
        resultMap.put("message","修改成功!");
        return resultMap;
    }
}
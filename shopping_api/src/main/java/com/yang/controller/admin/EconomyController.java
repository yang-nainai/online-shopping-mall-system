package com.yang.controller.admin;

import com.yang.service.EconomyService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 优惠表(Economy)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController("merchantEconomyController")
@RequestMapping("/management/economy")
public class EconomyController {
    /**
     * 服务对象
     */
    @Resource
    private EconomyService economyService;

}
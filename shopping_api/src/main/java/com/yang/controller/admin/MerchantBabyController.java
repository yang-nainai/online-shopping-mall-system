package com.yang.controller.admin;

import com.yang.entity.Commodity;
import com.yang.entity.MerchantBaby;
import com.yang.service.CommodityService;
import com.yang.service.MerchantBabyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商家 商品表(MerchantBaby)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController("ManagementMerchantBabyController")
@RequestMapping("/management/merchantBaby")
@Slf4j
public class MerchantBabyController {
    /**
     * 服务对象
     */
    @Resource
    private MerchantBabyService merchantBabyService;
    @Resource
    private CommodityService commodityService;

    /**
     * 根据商家编号获取商家所有商品信息
     *
     * @param merchantID 商家编号
     * @return 商品宝贝集合
     */
    @GetMapping("queryAllBaby/{merchantID}")
    public List<MerchantBaby> getMerchantBabyByMerchantID(@PathVariable("merchantID") String merchantID) {
        List<MerchantBaby> merchantBabies = merchantBabyService.queryMerchantBaby(merchantID);
        if (merchantBabies.size() == 0) {
            //如果没有商品信息
            MerchantBaby merchantBaby = new MerchantBaby();
            merchantBaby.setMerchantBabyId("没有对应的商品宝贝编号");
            merchantBabies.add(merchantBaby);
        }
        return merchantBabies;
    }

    /**
     * 根据商家编号获取商家所有商品数量
     * @param merchantID
     * @return
     */
    @GetMapping("/queryCount/{merchantID}")
    public Map<String,Object> queryMerchantBabyCount(@PathVariable("merchantID") String merchantID) {
        Map<String, Object> resultMap = new HashMap<>();
        Integer integer = merchantBabyService.queryMerchantBabyNumber(merchantID);
        resultMap.put("count",integer);
        resultMap.put("message","查询成功");
        resultMap.put("code",200);
        return resultMap;
    }

    /**
     * 删除商品信息
     * @param merchantBabyID
     * @return
     */
    @DeleteMapping("/delete/{merchantBabyID}")
    public Map<String,Object> deleteMerchantBaby(@PathVariable("merchantBabyID") String merchantBabyID){
        MerchantBaby merchantBaby = new MerchantBaby();
        merchantBaby.setMerchantBabyId(merchantBabyID)
                .setMerchantId("");
        Integer deleteResult = merchantBabyService.updateMerchantBaby(merchantBaby);
        Map<String,Object> resultMap = new HashMap<>();
        if (deleteResult == 1){
            resultMap.put("message","删除成功");
            resultMap.put("code",200);
        }else{
            resultMap.put("message","删除失败");
            resultMap.put("code",500);
        }
        return resultMap;
    }

    /**
     * 修改商品信息
     * @param commodity
     * @return
     */
    @PutMapping("/update/merchantBaby")
    public Map<String,Object> updateMerchantBaby(Commodity commodity,
                                                 @RequestParam("createTime") String createTime){
        Map<String,Object> resultMap = new HashMap<>();
        try {
            log.info("createTime:{}",createTime);
            log.info("commodityInfo:{}",commodity);
            SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss");
            commodity.setCommodityBirthTime(dateFormat.parse(createTime));  //设置商品生成时间
            Integer updateResult = commodityService.updateCommodity(commodity);
            if (updateResult == 1){
                resultMap.put("message","修改成功");
                resultMap.put("code",200);
            }else{
                resultMap.put("message","修改失败");
                resultMap.put("code",500);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return resultMap;
    }
}
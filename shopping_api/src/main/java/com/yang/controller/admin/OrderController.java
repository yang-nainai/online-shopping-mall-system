package com.yang.controller.admin;

import com.yang.entity.OrderUser;
import com.yang.service.OrderBaseService;
import com.yang.service.OrderUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author yangdacheng
 * @title: OrderController
 * @projectName shopping_api
 * @description: TODO
 * @date 2022/5/23 11:11 下午
 */
@RestController("merchantOrderController")
@RequestMapping("/management/order")
@Slf4j
public class OrderController {

    @Autowired
    private OrderUserService orderUserService;
    @Autowired
    private OrderBaseService orderBaseService;

    /**
     * 根据商品编号&订单状态获取订单
     * @param merchantID
     * @param state
     * @return
     */
    @GetMapping("/getOrderList/{merchantID}/{state}")
    public Map<String,Object> queryOrderByIDAndState(@PathVariable("merchantID") String merchantID,
                                      @PathVariable("state") String state){
        log.info("{}===={}",merchantID,state);
        Map<String,Object> resultMap = new HashMap();
        List<OrderUser> orderUsers = orderUserService.queryOrderByMerchantID(merchantID);
        List<OrderUser> orderState = new ArrayList<>();
        if (orderUsers.isEmpty()){
            resultMap.put("code",0);
            resultMap.put("message","没有订单");
            return resultMap;
        }
        //没有对应的状态
        if ("null".equals(state)){
            resultMap.put("code",200);
            resultMap.put("message","查询订单成功");
            resultMap.put("data",orderUsers);
            return resultMap;
        }
        //有对应的状态
        for (OrderUser orderUser : orderUsers) {
            if (orderUser.getOrderBase().getOrderState().equals(state)){
                orderState.add(orderUser);
            }
        }
        resultMap.put("code",200);
        resultMap.put("message","查询"+state+"订单成功");
        resultMap.put("data",orderState);
        return resultMap;
    }

    /**
     * 修改订单配送状态
     * @param orderID
     * @param state
     * @return
     */
    @PutMapping(value = "/updateOrderState",headers = "Accept=application/json")
    public Map<String,Object> updateOrder(@RequestParam("orderID") String orderID,
                                          @RequestParam("state") String state){
        Map<String,Object> resultMap = new HashMap();
        Integer updateResult = orderBaseService.updateOrderState(orderID, state);
        if (updateResult<=0){
            resultMap.put("code",0);
            resultMap.put("message","更新订单状态失败");
            return resultMap;
        }
        resultMap.put("code",200);
        resultMap.put("message","更新订单状态成功");
        return resultMap;
    }
}

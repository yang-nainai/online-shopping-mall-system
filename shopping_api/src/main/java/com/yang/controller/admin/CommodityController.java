package com.yang.controller.admin;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.yang.entity.*;
import com.yang.entity.admin.CommodityNumbers;
import com.yang.entity.utils.FileUploadBean;
import com.yang.entity.utils.HomeAdvertisement;
import com.yang.entity.utils.PageInfo;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.*;
import com.yang.utils.UploadFileToTenXunYun;
import com.yang.utils.admin.InputNumberInfo;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

/**
 * 商品表 用来记入一些商品的基本信息(Commodity)表控制层
 * 后端控制台 API
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController("ManagementCommodityController")
@RequestMapping("/management/commodity")
@Api(tags = "商品信息接口")
@Slf4j
public class CommodityController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityService commodityService;
    @Resource
    private MerchantService merchantService;
    @Resource
    private FileUploadBean fileUploadBean;
    @Resource
    private CommodityDetailsService detailsService;
    @Resource
    private CommodityShowService showService;
    @Resource
    private CommoditySkuColorService commoditySkuColorService;
    @Resource
    private CommoditySkuSizeService commoditySkuSizeService;
    @Resource
    private CommoditySkuListService commoditySkuListService;
    @Resource
    private MerchantBabyService merchantBabyService;
    @Resource
    private CommodityTypeService commodityTypeService;
    @Value("${fileUpload.baseUrl}")
    private String basePath;
    @Resource
    private RedisTemplate<String,String> redisTemplate;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ApiImplicitParam(paramType = "path",name = "id",value = "订单编号",required = true)
    @ApiOperation(value = "通过主键查询单条数据",notes = "通过主键查询单条数据")
    @ApiResponse(code = 200,message = "商品类")
    public Commodity selectOne(String id) {
        return this.commodityService.queryById(id);
    }

    /**
     * 获取 商品总数量
     * @return
     */
    @GetMapping("getCommoditySum")
    @ApiOperation(value = "商品总数量",notes = "商品总数量")
    @ApiResponses({
            @ApiResponse(code = 200,message = "商品总数量")
    })
    public Integer getCommoditySum(){
        return commodityService.find_commoditySum();
    }

    /**
     * 分页获取商品信息
     * @param nowPage
     * @param commoditySum
     * @return
     */
    @PostMapping("limit")
    @ApiOperation(value = "分页获取商品信息",notes = "分页获取商品信息")
    @ApiResponses({
            @ApiResponse(code = 200,message = "商品信息集合")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "nowPage",name = "当前页面",required = true),
            @ApiImplicitParam(paramType = "query",value = "commodityTotal",name = "获取数量",required = true)
    })
    public List<Commodity> limitCommodity(@RequestParam("nowPage") Integer nowPage,
                                          @RequestParam("commodityTotal") Integer commoditySum) {
        // 用户显示条数 后端可以修改 可以从数据库导入
        Integer pageSize = 10; // 默认为10条
        int allPages = commoditySum / pageSize;
        if (commoditySum % pageSize != 0) {
            allPages = commoditySum / pageSize;  //不是10 的倍数就将页面大小加1 为了取出所有的数据
            allPages = allPages + 1;
        }
        PageInfo pageInfo = new PageInfo((nowPage * pageSize), allPages, pageSize);
        List<Commodity> commodities = commodityService.limitCommodity(pageInfo);
        // 格式化数据样式
        // 获取商品类
        for (Commodity commodityContext : commodities) {
            CommodityShow commodityShow = commodityContext.getCommodityShow(); //进入商品类中的商品图片类
            // 设置图片
            String[] images = new String[5];
            images[0] = commodityShow.getShowImgOne();
            images[1] = commodityShow.getShowImgTwo();
            images[2] = commodityShow.getShowImgThree();
            images[3] = commodityShow.getShowImgFour();
            images[4] = commodityShow.getShowImgFive();
            commodityShow.setShowImages(images);
            // 添加商家信息
            MerchantBaby merchantBaby = commodityContext.getMerchantBaby();
            if (merchantBaby != null){
                String merchantId = merchantBaby.getMerchantId();
                Merchant merchant = merchantService.queryMerchantById(merchantId);
                commodityContext.setMerchant(merchant);
            }
        }
        return commodities;
    }


    /**
     * 文件上传
     * @param multipartFile 文件上传
     * @return
     */
    @PostMapping("commodityUpload")
    public ResponseParamInfo userImgUpLoad(@RequestParam("file") MultipartFile multipartFile){
        ResponseParamInfo responseParamInfo = new ResponseParamInfo();
        String fileBase = basePath+"littleMessageFile/files/";
        if (multipartFile.isEmpty()){
            responseParamInfo = new ResponseParamInfo(500,"文件上传失败",null);
        }
        //生成 uuid编号变成文件地址 避免重名文件覆盖
        String httpURL = UploadFileToTenXunYun.uploadFile(multipartFile, fileBase, "聊天文件");
        log.info("httpURL:"+httpURL);
        if (Objects.isNull(httpURL)){
            responseParamInfo = new ResponseParamInfo(500,"文件上传失败",null);
        }else{
            responseParamInfo = new ResponseParamInfo(200,"文件上传成功",httpURL);
        }
        log.info("文件上传：{}",responseParamInfo);
        return responseParamInfo;
    }

    /**
     * 商品上架
     * @return
     */
    @PostMapping(value = "insertCommodity")
    public Map<String,Object> insertCommodity(@RequestBody Map<String, Object> jsonObject){
        //获取 json中的对象
        List<CommoditySkuColor> skuColors = (List<CommoditySkuColor>) jsonObject.get("commoditySkuColorList");
        List<CommoditySkuSize> skuSizes = (List<CommoditySkuSize>)  jsonObject.get("commoditySkuSizeList");
        List<InputNumberInfo> inputNumbers = (List<InputNumberInfo>) jsonObject.get("commodityNumbers");
        List<String> showImages = (List<String>) jsonObject.get("showImages");      //商品展示图片
        List<String> detailsImages = (List<String>) jsonObject.get("detailsImages");      //商品详情图
        String merchantID =  jsonObject.get("merchantID").toString();      //商家编号
        String typeID = jsonObject.get("typeID").toString(); //商品类型

        CommoditySkuColor commoditySkuColor = new CommoditySkuColor();      //颜色对象
        CommoditySkuSize commoditySkuSize = new CommoditySkuSize();     //大小对象
        MerchantBaby merchantBaby = new MerchantBaby();  //数据商品集合
        CommodityType commodityType = new CommodityType(); //商品类型

        //新的对象集合
        List<CommoditySkuColor> skuColorsCache = new ArrayList<>();
        List<CommoditySkuSize> skuSizesCache = new ArrayList<>();
        List<InputNumberInfo> numberAndMoneyCache = new ArrayList<>(); //用来存放sku购买数量&价格
        CommodityShow commodityShow = new CommodityShow();      //商品展示图片对象
        CommodityDetails commodityDetails = new CommodityDetails(); //商品详情图片
        List<CommoditySkuList> commoditySkuListList = new ArrayList<>();

        //添加基本商品
        Commodity commodity = new Commodity();
        String commodityId = UUID.randomUUID().toString().replace("-","");
        commodity.setCommodityId(commodityId)
                .setCommodityName(jsonObject.get("commodityName").toString())   //设置商品名称
                .setCommoditySales(0) //设置商品销量
                .setCommodityBirthTime(new Date());
        //设置商家商品集合
        merchantBaby.setMerchantId(merchantID)
                .setCommodityId(commodityId)
                .setMerchantBabyId(UUID.randomUUID().toString().replace("-",""));

        //设置商品类型
        commodityType.setCommodityId(commodityId)
                .setCommodityTypeId(UUID.randomUUID().toString().replace("-",""))
                .setTypeId(typeID);

        //生成SkuColor商品编号
        List<String> skuColorIDS = new ArrayList<>();
        for (int index=0;index<skuColors.size();index++){
            String id = UUID.randomUUID().toString().replace("-","");
            //先转换为字符串 在转换为json数据
            String colorSku = JSONObject.toJSONString(skuColors.get(index));
            CommoditySkuColor nowSkuColor = JSONObject.parseObject(colorSku,CommoditySkuColor.class);       //获取当前循环对象

            nowSkuColor.setSkuColorId(id)
                    .setCommodityId(commodityId)
                    .setSkuPreviewImgUrl(nowSkuColor.getSkuImgUrl());
            skuColorsCache.add(nowSkuColor);
            skuColorIDS.add(id);
        }

        //生成SkuSize
        List<String> skuSizeIDs = new ArrayList<>();
        for (int index=0;index<skuSizes.size();index++){
            String id = UUID.randomUUID().toString().replace("-","");
            //转换为字符串在转换为对象
            String sizeSku = JSONObject.toJSONString(skuSizes.get(index));
            CommoditySkuSize skuSize = JSONObject.parseObject(sizeSku,CommoditySkuSize.class);
            //设置对象属性
            skuSize.setCommodityId(commodityId)
                    .setSkuSizeId(id);
            skuSizesCache.add(skuSize);
            skuSizeIDs.add(id);
        }

        //设置商品展示图
        String commodityShowID = UUID.randomUUID().toString().replace("-","");
        commodityShow.setCommodityId(commodityId).setCommodityShowId(commodityShowID);
        for (int index=0;index<showImages.size();index++){
            switch (index){
                case 0:
                    commodityShow.setShowImgOne(showImages.get(index));
                    break;
                case 1:
                    commodityShow.setShowImgTwo(showImages.get(index));
                    break;
                case 2:
                    commodityShow.setShowImgThree(showImages.get(index));
                    break;
                case 3:
                    commodityShow.setShowImgFour(showImages.get(index));
                    break;
                case 4:
                    commodityShow.setShowImgFive(showImages.get(index));
                    break;
            }
        }

        //设置商品详情图片
        String commodityDetailsId = UUID.randomUUID().toString().replace("-","");
        commodityDetails.setCommodityId(commodityId).setCommodityDetailsId(commodityDetailsId);
        for (int index=0;index<detailsImages.size();index++){
            switch (index){
                case 0:
                    commodityDetails.setBabyDetailImageOne(detailsImages.get(index));
                    break;
                case 1:
                    commodityDetails.setBabyDetailImageTwo(detailsImages.get(index));
                    break;
                case 2:
                    commodityDetails.setBabyDetailImageThree(detailsImages.get(index));
                    break;
                case 3:
                    commodityDetails.setBabyDetailImageFour(detailsImages.get(index));
                    break;
                case 4:
                    commodityDetails.setBabyDetailImageFive(detailsImages.get(index));
                    break;
                case 5:
                    commodityDetails.setBabyDetailImageSix(detailsImages.get(index));
                    break;
                case 6:
                    commodityDetails.setBabyDetailImageSeven(detailsImages.get(index));
                    break;
                case 7:
                    commodityDetails.setBabyDetailImageEight(detailsImages.get(index));
                    break;
                case 8:
                    commodityDetails.setBabyDetailImageNine(detailsImages.get(index));
                    break;
            }
        }

        //设置输入数量集合
        for(int index=0;index<inputNumbers.size();index++){
            String inputStr = JSONObject.toJSONString(inputNumbers.get(index));
            InputNumberInfo inputNumberInfo = JSONObject.parseObject(inputStr,InputNumberInfo.class);
            numberAndMoneyCache.add(inputNumberInfo);
        }

        //商品skuList格式化
        for (int colorIndex=0;colorIndex<skuColorsCache.size();colorIndex++){
            for (int sizeIndex=0;sizeIndex<skuSizesCache.size();sizeIndex++){
                CommoditySkuList commoditySkuList = new CommoditySkuList();
                commoditySkuList.setCommodityId(commodityId)
                        .setCommodityPrice(numberAndMoneyCache.get(colorIndex).getInputMoney())
                        .setCommodityStockNum(numberAndMoneyCache.get(colorIndex).getInputValue())
                        .setSkuSizeId(skuSizesCache.get(sizeIndex).getSkuSizeId())
                        .setSkuColorId(skuColorsCache.get(colorIndex).getSkuColorId())
                        .setSkuListId(UUID.randomUUID().toString().replace("-",""));
                commoditySkuListList.add(commoditySkuList);
            }
        }

        skuColorsCache.forEach(System.out::println);
        skuSizesCache.forEach(System.out::println);

        log.info("格式化后的数据");
        log.info("颜色集合:{}",skuColorsCache);
        log.info("大小合集:{}",skuSizesCache);
        log.info("展示图片合集:{}",commodityShow);
        log.info("详情图片:{}",commodityDetails);
        log.info("购买数量:{}",numberAndMoneyCache);
        log.info("sku集合:{}",commoditySkuListList);
        log.info("开始添加到数据库");
        //添加商品 commodityDao
        Integer commodityResult = commodityService.insertCommodity(commodity);
        //添加展示图片
        Integer showResult = showService.insertCommodityShow(commodityShow);
        //添加详情图片
        Integer detailsResult = detailsService.insertCommodityDetails(commodityDetails);
        //添加颜色sku
        Integer colorResult = commoditySkuColorService.insertSkuColor(skuColorsCache);
        //添加大小sku
        Integer sizeResult = commoditySkuSizeService.insertCommoditySkuSize(skuSizesCache);
        //添加sku集合
        Integer listResult = commoditySkuListService.insertSkuList(commoditySkuListList);
        //添加商品类型
        Integer typeResult = commodityTypeService.insertCommodityType(commodityType);
        //添加商家商品宝贝
        Integer babyResult = merchantBabyService.insertBaby(merchantBaby);

        log.info("最终的添加结果为:{}--{}--{}--{}--{}--{}--{}--{}",commodityResult,showResult,detailsResult,
                colorResult,sizeResult,listResult,babyResult,typeResult);
        //设置返回结果
        Map<String,Object> resultMap = new HashMap<>();
        if (commodityResult>0 && showResult>0 && detailsResult>0 && colorResult>0
                && sizeResult>0 && listResult >0 && babyResult>0 && typeResult>0){
            resultMap.put("code",200);
            resultMap.put("message","添加商品成功～");
            return resultMap;
        }
        resultMap.put("code",500);
        resultMap.put("message","添加失败");
        return resultMap;
    }

}
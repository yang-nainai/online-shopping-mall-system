package com.yang.controller;

import com.yang.entity.MerchantBaby;
import com.yang.service.MerchantBabyService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商家 商品表(MerchantBaby)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("merchantBaby")
public class MerchantBabyController {
    /**
     * 服务对象
     */
    @Resource
    private MerchantBabyService merchantBabyService;
    
}
package com.yang.controller;

import com.yang.entity.Type;
import com.yang.service.TypeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品类型编号 用来存放一些商品类型(Type)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@RestController
@RequestMapping("type")
public class TypeController {
    /**
     * 服务对象
     */
    @Resource
    private TypeService typeService;
}
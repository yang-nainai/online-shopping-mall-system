package com.yang.controller;

import com.yang.entity.CommoditySkuColor;
import com.yang.service.CommoditySkuColorService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品的sku属性表（颜色）(CommoditySkuColor)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("commoditySkuColor")
public class CommoditySkuColorController {
    /**
     * 服务对象
     */
    @Resource
    private CommoditySkuColorService commoditySkuColorService;

}
package com.yang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yang.entity.Commodity;
import com.yang.entity.Merchant;
import com.yang.entity.UserSearchHistory;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.CommodityService;
import com.yang.service.MerchantService;
import com.yang.utils.ElasticSearchUtil;
import com.yang.utils.RedisUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author yangdacheng
 * @title: SearchHistoryController 搜索记录类用来存放用户的搜索记录
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/11 2:55 下午
 */
@RestController
@RequestMapping("searchHistory")
@Api(tags = "搜索记录接口")
public class SearchHistoryController {

    /**
     * redis 服务对象 去redis缓存中搜索用户的搜索记录
     */
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    /**
     * 商品服务对象
     */
    @Resource
    private CommodityService commodityService;
    @Resource
    private MerchantService merchantService;
    @Resource
    private ElasticSearchUtil elasticSearchUtil;
    @Resource
    private RedisUtils redisUtils;
    @Value("${myElasticSearch.index_name}")
    private String index_name;

    /**
     * 获取用户在 redis 缓存中的key --获取用户的历史搜索记录
     *
     * @param userID
     * @return
     */
    @RequestMapping("queryUserHistory")
    @ApiOperation(value = "获取用户的历史搜索记录",notes = "获取用户的历史搜索记录")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userID",value = "用户编号",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "ResponseParamInfo类")
    })
    public ResponseParamInfo queryUserHistory(@RequestParam("userID") String userID) {
        ResponseParamInfo responseParamInfo = null;
        List<String> searchList = new ArrayList<>();
        /**
         * 1、获取用户在 redis 中的搜索记录 key
         */
        //String queryGender = "*"+userID+"search*";
        String queryGender = userID + "search";
        //String strUserSearchHistory = redisTemplate.opsForValue().get(key);
        List<String> userSearchList = redisTemplate.opsForList().range(queryGender, 0, -1);//获取list集合中的所有元素
        for (String userSearch : userSearchList) {
            UserSearchHistory userSearchHistory = JSONObject.parseObject(userSearch, UserSearchHistory.class);
            searchList.add(userSearchHistory.getUserSearchHistory());
            responseParamInfo = new ResponseParamInfo(200, "获取成功", searchList);
        }
        return responseParamInfo;
    }

    /**
     * 向 redis 缓存添加用户搜索记录 返回用户搜索的结果
     *
     * @param userID     用户编号
     * @param searchName 搜索名称
     * @return 放回用户搜索结果 商品类
     */
    @RequestMapping("insertUserHistory")
    @ApiOperation(value = "向 redis 缓存添加用户搜索记录 返回用户搜索的结果",notes = "向 redis 缓存添加用户搜索记录 返回用户搜索的结果")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userID",value = "用户编号",required = true),
            @ApiImplicitParam(paramType = "query",name = "searchName",value = "搜索名称",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "ResponseParamInfo类")
    })
    public ResponseParamInfo insertUserHistory(@RequestParam("userID") String userID,
                                               @RequestParam("searchName") String searchName) {
        ResponseParamInfo responseParamInfo = null;
        /**
         * 1、将用户搜索的记录 添加到 redis 缓存中
         * 2、去 mysql 数据库中查询用户搜索的记录 模糊查询
         * 3、将获取的数据返回出去
         */
        redisUtils.insertNewSearchItem(userID,searchName);
//        UserSearchHistory userSearchHistory = new UserSearchHistory(userID, searchName);
//        String strUserSearchHistory = JSONObject.toJSONString(userSearchHistory); //将对象转换为字符串
//        String redisKey = userID + "search"; //redis key
//        //查看缓存中是否在这个搜索记录 存在就移除 并且在头部添加
//        List<String> userSearchList = redisTemplate.opsForList().range(redisKey, 0, -1);
//        List<UserSearchHistory> userSearchHistories = new ArrayList<>(); //用来存放在redis 读取到的集合
//        List<UserSearchHistory> insertRedis = new ArrayList<>(); //添加到redis集合中
//        for (int index = 0; index < userSearchList.size(); index++) {
//            UserSearchHistory listHistory = JSONObject.parseObject(userSearchList.get(index), UserSearchHistory.class);
//            if (!listHistory.getUserSearchHistory().equals(searchName)) {
//                userSearchHistories.add(listHistory);
//            }
//        }
//        //没有重复的元素
//        if (userSearchHistories.size() == userSearchList.size()){
//            //获取缓存中的key 如果长度大于10个就移除最后一个
//            Long searchSize = redisTemplate.opsForList().size(redisKey);  //当前搜索记录大小
//            //如果长度为10个就需要移除最右边的元素
//            if (searchSize == 10) {
//                //redisTemplate.opsForList().rightPop(redisKey);
//                redisTemplate.opsForList().rightPopAndLeftPush(redisKey, strUserSearchHistory);
//            } else {
//                redisTemplate.opsForList().leftPush(redisKey, strUserSearchHistory); //存放到缓存中去
//            }
//        }else{
//            //删除原有的redis值
//            redisTemplate.delete(redisKey);
//            //如果用户搜索的值存在在原有的搜索集合中
//            insertRedis.add(userSearchHistory); //将用户新收拾的添加到头部
//            //将原来集合中的内容添加到新的集合
//            insertRedis.addAll(userSearchHistories);
//            //将现在的所有值添加到搜索中
//            for (UserSearchHistory history : insertRedis) {
//                String userHistory = JSONObject.toJSONString(history);
//                redisTemplate.opsForList().rightPush(redisKey, userHistory);
//            }
//        }

        //去数据库中模糊查询用户想要的结果
        List<Commodity> searchCommodities = commodityService.searchCommodity("%" + searchName + "%");
        //设置商家信息
        if (searchCommodities!=null){
            searchCommodities.forEach((value)->{
                Merchant merchant = merchantService.queryMerchantById(value.getMerchantBaby().getMerchantId());
                value.setMerchant(merchant);
            });
        }
        System.out.println(searchCommodities.size());
        //判断集合是否为空集合
        if (!searchCommodities.isEmpty()) {
            responseParamInfo = new ResponseParamInfo(200, "获取成功", searchCommodities);
        } else {
            responseParamInfo = new ResponseParamInfo(2000, "暂时还没有该类商品", null);
        }
        return responseParamInfo;
    }

    /**
     * 根据用户编号清空历史记录
     *
     * @param userID
     * @return
     */
    @RequestMapping("/clearHistory")
    @ApiOperation(value = "根据用户编号清空历史记录",notes = "根据用户编号清空历史记录")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userID",value = "用户编号",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "ResponseParamInfo类")
    })
    public ResponseParamInfo clearHistory(@RequestParam("userID") String userID) {
        ResponseParamInfo responseParamInfo = null;
        String queryKey = userID + "search";
        redisTemplate.delete(queryKey); //删除集合中的内容
        responseParamInfo = new ResponseParamInfo(200, "清空成功", null);
        return responseParamInfo;
    }


    @RequestMapping(value = "searchInElastic")
    @ApiOperation(value = "向 redis 缓存添加用户搜索记录 返回用户搜索的结果(elasticsearch)",notes = "向 redis 缓存添加用户搜索记录 返回用户搜索的结果")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userID",value = "用户编号",required = true),
            @ApiImplicitParam(paramType = "query",name = "searchName",value = "搜索名称",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "ResponseParamInfo类")
    })
    public ResponseParamInfo searchInElastic(@RequestParam("userID") String userID,
                                             @RequestParam("searchName") String searchName) throws IOException {
        ResponseParamInfo responseParamInfo = null;
        List<Commodity> newCommodities = new ArrayList<>();
        redisUtils.insertNewSearchItem(userID,searchName);
        //去 es 数据库中模糊查询用户想要的结果
        List<Object> searchCommodities = elasticSearchUtil.searchObject(index_name, "commodityName", searchName, true);

        //判断集合是否为空集合
        //设置商家信息
        if (searchCommodities!=null){
            searchCommodities.forEach((value)->{
                Commodity commodity = JSONObject.parseObject(JSONObject.toJSONString(value), Commodity.class); //类型转换
                Merchant merchant = merchantService.queryMerchantById(commodity.getMerchantBaby().getMerchantId());
                commodity.setMerchant(merchant);
                newCommodities.add(commodity);
            });
        }
        if (!newCommodities.isEmpty()) {
            responseParamInfo = new ResponseParamInfo(200, "获取成功", newCommodities);
        } else {
            responseParamInfo = new ResponseParamInfo(2000, "暂时还没有该类商品", null);
        }
        return responseParamInfo;
    }
}

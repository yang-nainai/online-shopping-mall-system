package com.yang.controller;

import com.yang.entity.Address;
import com.yang.entity.UserAddress;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.AddressService;
import com.yang.service.UserAddressService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 用户地址表(UserAddress)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@RestController
@RequestMapping("userAddress")
@Api(tags = "地址接口")
public class UserAddressController {

    private static ResponseParamInfo responseParamInfo = null;

    /**
     * 服务对象
     */
    @Resource
    private UserAddressService userAddressService;
    @Resource
    private AddressService addressService;

    /**
     * 根据用户编号获取用户地址信息
     * @param userID 用户编号
     * @return 用户地址集合
     */
    @GetMapping("getUserAddress")
    @ApiOperation(value = "根据用户编号获取用户地址信息",notes = "根据用户编号获取用户地址信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userID",value = "用户编号",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "ResponseParamInfo类")
    })
    public List<UserAddress> getUserAddress(@RequestParam("userId") String userID){
        List<UserAddress> allUserAddress = userAddressService.findAllUserAddress(userID);
        return allUserAddress;
    }

    /**
     * 添加一个新的地址
     * @param address 用户传递的地址信息
     * @param isDefault 是否为默认地址
     * @param userId 用户编号
     * @return ResponseParamInfo
     */
    @PostMapping("putUserAddress")
    @ApiOperation(value = "添加一个新的地址",notes = "添加一个新的地址")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userID",value = "用户编号",required = true),
            @ApiImplicitParam(paramType = "query",name = "isDefault",value = "是否为默认地址",required = true),
            @ApiImplicitParam(paramType = "query",name = "address",value = "用户传递的地址信息",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "ResponseParamInfo类")
    })
    public ResponseParamInfo putUserAddress(Address address,
                                            @RequestParam("isDefault")Integer isDefault,
                                            @RequestParam("userId")String userId){
        ResponseParamInfo responseParamInfo = null;
        // 生成地址编号 UUID 或者其他能生成唯一标示的编号
        UUID addressUUID = UUID.randomUUID(); //生成地址编号
        String addressId = addressUUID.toString();
        address.setAddressId(addressId);
        Integer putAddressResult = addressService.insertAddress(address);
        //判断地址是否添加成功
        if (putAddressResult>0){
            //如果用户将地址添加为默认地址 需要修改存在的默认地址
            if (isDefault == 1){
                // 查询用户是否有默认地址存在
                UserAddress defaultUserAddress = userAddressService.isDefaultUserAddress(userId,1);
                //存在默认地址 需要将原有的默认地址改变状态为0 添加新的地址为默认地址
                if (defaultUserAddress != null){
                    defaultUserAddress.setUserAddressIsdefault(0);
                    Integer default_state = userAddressService.updateDefaultAddress(defaultUserAddress);
                }
            }
            UUID userAddressUUID = UUID.randomUUID();
            String userAddressID = userAddressUUID.toString();
            UserAddress userAddress = new UserAddress(userAddressID,userId,address.getAddressId(),isDefault,null);
            Integer addressResult = userAddressService.insertUserAddress(userAddress);
            //用户地址添加成功
            if (addressResult>0){
                responseParamInfo = new ResponseParamInfo(200,"添加地址成功",addressResult);
            }
        }
        return responseParamInfo;
    }

    /**
     * 根据用户的编号来修改指定的地址信息
     * @param address
     * @param isDefault
     * @return
     */
    @PutMapping("updateAddress")
    @ApiOperation(value = "根据用户的编号来修改指定的地址信息",notes = "根据用户的编号来修改指定的地址信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userID",value = "用户编号",required = true),
            @ApiImplicitParam(paramType = "query",name = "isDefault",value = "是否为默认地址",required = true),
            @ApiImplicitParam(paramType = "query",name = "address",value = "用户传递的地址信息",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "ResponseParamInfo类")
    })
    public ResponseParamInfo updateAddress(Address address,Integer isDefault,String userId){
        //修改地址信息
        Integer updateResult = addressService.updateAddress(address);
        //设置默认地址
        if (isDefault == 1){
            //查询是否存在默认地址
            List<UserAddress> userAddressList = userAddressService.defaultAddress(userId, 1);
            //将查询出来的地址修改默认信息 关闭
            for (UserAddress userAddress: userAddressList){
                //关闭之前存在的默认地址
                userAddressService.updateDefaultState(userId, userAddress.getUserAddressAid(), 0);
            }
            //修改默认状态
            userAddressService.updateDefaultState(userId, address.getAddressId(), 1);
        }else{
            // 关闭默认选项地址
            userAddressService.updateDefaultState(userId, address.getAddressId(), 0);
        }
        if(updateResult>0){
            responseParamInfo = new ResponseParamInfo(200,"修改成功",null);
        }else {
            responseParamInfo = new ResponseParamInfo(500,"修改失败",null);
        }
        return responseParamInfo;
    }

    /**
     * 删除用户地址
     * @param userId
     * @param addressId
     * @return
     */
    @PostMapping("deleteAddress")
    @ApiOperation(value = "删除用户地址",notes = "删除用户地址")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userID",value = "用户编号",required = true),
            @ApiImplicitParam(paramType = "query",name = "addressId",value = "地址编号",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "ResponseParamInfo类")
    })
    public ResponseParamInfo deleteAddress(@RequestParam("userId") String userId,
                                           @RequestParam("addressId") String addressId){
        System.out.println("userId:"+userId+"\taddressID:"+addressId);
        if(userId != null && addressId !=null){
            Integer userAddressResult = userAddressService.deleteUserAddress(userId, addressId);
            // 先删除用户地址表里面信息 在删除地址里面内容
            if (userAddressResult > 0){
                Integer addressResult = addressService.deleteAddress(addressId);
                if(addressResult>0){
                    responseParamInfo = new ResponseParamInfo(200,"删除成功!",null);
                }else{
                    responseParamInfo = new ResponseParamInfo(500,"删除失败",null);
                }
            }else{
                responseParamInfo = new ResponseParamInfo(500,"删除失败",null);
            }
        }else{
            responseParamInfo = new ResponseParamInfo(500,"删除失败",null);
        }
        return responseParamInfo;
    }
}
package com.yang.controller;

import com.yang.entity.Address;
import com.yang.service.AddressService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 收货人&地址表(Address)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:56
 */
@RestController
@RequestMapping("address")
public class AddressController {
    /**
     * 服务对象
     */
    @Resource
    private AddressService addressService;
}
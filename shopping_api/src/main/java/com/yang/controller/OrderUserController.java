package com.yang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.yang.entity.*;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.*;
import io.swagger.annotations.*;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 用户订单表(OrderUser)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@RestController
@RequestMapping("orderUser")
@Api(tags = "用户订单接口")
public class OrderUserController {
    /**
     * 服务对象
     */
    @Resource
    private OrderUserService orderUserService;
    /**
     * 基础订单 服务对象
     */
    @Resource
    private OrderBaseService orderBaseService;
    /**
     * 购物车服务对象
     */
    @Resource
    private UserCartService userCartService;
    /**
     * 商品宝贝服务对象
     */
    @Resource
    private MerchantBabyService merchantBabyService;
    /**
     * 商品sku对象
     */
    @Resource
    private CommoditySkuListService commoditySkuListService;
    /**
     * 用户地址类
     */
    @Resource
    private UserAddressService userAddressService;
    /**
     * redis数据缓存
     */
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 添加新的订单
     *
     * @param orderState      订单当前状态
     * @param orderBuyNum     购买商品数量
     * @param orderNeedPay    购买商品所需要支付的钱
     * @param orderPay        购买商品实际支付的钱
     * @param orderPayState   商品的支付状态
     * @param merchantBabyId  对应的商家商品编号
     * @param userAddressId   获取用户地址信息
     * @param commodityId     商品编号
     * @param commodityListId 商品sku ist编号
     * @param cartID          购物车编号
     * @return
     */
    @PostMapping("insertOrder")
    @ApiOperation(value = "添加用户订单",notes = "添加用户订单")
    @ApiResponses({
            @ApiResponse(code = 200,message = "添加成功")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "订单状态",value = "orderState",required = true),
            @ApiImplicitParam(paramType = "query",name = "购买数量",value = "orderBuyNum",required = true),
            @ApiImplicitParam(paramType = "query",name = "需要支付金额",value = "orderNeedPay",required = true),
            @ApiImplicitParam(paramType = "query",name = "支付金额",value = "orderPay",required = true),
            @ApiImplicitParam(paramType = "query",name = "订单支付状态",value = "orderPayState",required = true),
            @ApiImplicitParam(paramType = "query",name = "商品宝贝编号",value = "merchantBabyId",required = true),
            @ApiImplicitParam(paramType = "query",name = "用户地址编号",value = "userAddressId",required = true),
            @ApiImplicitParam(paramType = "query",name = "商品编号",value = "commodityId",required = true),
            @ApiImplicitParam(paramType = "query",name = "商品skulist编号",value = "commodityListId",required = true),
            @ApiImplicitParam(paramType = "query",name = "用户编号",value = "userID",required = true),
            @ApiImplicitParam(paramType = "query",name = "购物车编号",value = "cartID",required = true),
    })
    public ResponseParamInfo insertNewOrder(@RequestParam("orderState") String orderState,
                                            @RequestParam("orderBuyNum") Integer orderBuyNum,
                                            @RequestParam("orderNeedPay") Float orderNeedPay,
                                            @RequestParam("orderPay") Float orderPay,
                                            @RequestParam("orderPayState") String orderPayState,
                                            @RequestParam("merchantBabyId") String merchantBabyId,
                                            @RequestParam("userAddressId") String userAddressId,
                                            @RequestParam("commodityId") String commodityId,
                                            @RequestParam("commodityListId") String commodityListId,
                                            @RequestParam("userID") String userID,
                                            @RequestParam("cartID") String cartID,
                                            @RequestParam("merchantID")String merchantID) {
        //地址有问题！！！！
        Map<String,String> redisKeys = new HashMap<>();
        String orderId = UUID.randomUUID().toString(); //生成基本订单编号
        orderId = orderId.replace("-","");
        OrderBase orderBase = new OrderBase(orderId, orderState, orderBuyNum, orderNeedPay, orderPay, orderPayState, "支付宝支付");
        String orderUserId = UUID.randomUUID().toString().replace("-","");  //生成订单
        MerchantBaby merchantBaby = merchantBabyService.queryByCommodityId(commodityId); //获取商家信息
        merchantBabyId = merchantBaby.getMerchantBabyId(); //设置商品宝贝编号
        UserAddress userAddress = userAddressService.queryByAddressId(userAddressId); //获取地址
        userAddressId = userAddress.getUserAddressId();    //..... null
        OrderUser orderUser = new OrderUser(orderUserId, merchantBabyId, userAddressId, orderId, commodityId, commodityListId, userID,merchantID);
        //基础类添加成功后添加用户订单 将订单添加到数据库
        ResponseParamInfo responseParamInfo = null;
        //将用户提交了 未支付的订单存放在 redis 缓存中
        //缓存基础订单
        String baseKey = orderId+ userID;
        String orderBaseStr = JSONObject.toJSONString(orderBase); //将对象转换为 json字符串
        redisTemplate.opsForValue().set(baseKey, orderBaseStr, 30, TimeUnit.MINUTES);
        //缓存用户订单
        String userOrderKey = orderUserId + userID; //redis总的key名称 通过用户编号+购物车编号组成
        String userOrderStr = JSON.toJSONString(orderUser); //将对象转换为json字符串
        redisTemplate.opsForValue().set(userOrderKey, userOrderStr, 30, TimeUnit.MINUTES);
        //将订单添加到集合中
        redisKeys.put("baseKey",baseKey);
        redisKeys.put("userOrderKey",userOrderKey);
        responseParamInfo = new ResponseParamInfo(200, "添加成功!", redisKeys);
        //删除用户购物车中的商品
        userCartService.deleteByCartId(cartID);

        /**
         * 1、删除购物车内容 -- 根据购物车编号
         * 2、创建新的订单 -- 根据传递进来的值
         * 3、用户点击提交订单 跳转到支付界面
         * 4、支付成功后就修改订单的状态
         */
        return responseParamInfo;
    }

    /**
     * 根据用户编号获取用户订单
     *
     * @param userId 用户编号
     * @return
     */
    @PostMapping("queryAllOrder")
    @ApiOperation(value = "根据用户编号获取用户订单",notes = "根据用户编号获取用户订单")
    @ApiImplicitParam(paramType = "query",name = "用户编号",value = "userID")
    @ApiResponse(code = 200,message = "查询成功")
    public ResponseParamInfo queryAllOrder(@RequestParam("userID") String userId) {
        ResponseParamInfo responseParamInfo = null;
        List<OrderUser> orderUserList = new ArrayList<>();
        List<OrderBase> orderBaseList = new ArrayList<>();
        HashMap<String, Object> resultMap = new HashMap<>();
        /**
         * 1、去 redis 缓存中获取未支付订单
         * 2、去 mysql 中获取其他订单
         * 3、将两者订单组合返回出去
         */
        //1、去 redis 缓存中获取未支付订单
        String selectKey = "*" + userId; //设置查询样式
        //查询未支付的订单
        Set<String> keys = redisTemplate.keys(selectKey);
        for (String key : keys) {
            String redisCacheOrder = redisTemplate.opsForValue().get(key);
            //获取过期时间
            Long expire = redisTemplate.opsForValue().getOperations().getExpire(key);
            //格式化从redis中取出来的数据并且强制转换
            if (redisCacheOrder.indexOf("orderState") != -1) {
                OrderBase orderBase = JSON.parseObject(redisCacheOrder, OrderBase.class);
                orderBaseList.add(orderBase);
            } else {
                OrderUser orderUser = JSON.parseObject(redisCacheOrder, OrderUser.class);
                orderUser.setPastTime(expire);  //设置过期时间
                orderUserList.add(orderUser);
            }
        }

        //获取订单 商品信息 地址信息 等
        for (OrderUser order : orderUserList) {
            //获取宝贝信息 设置宝贝信息
            MerchantBaby merchantBaby = merchantBabyService.queryByCommodityId(order.getCommodityId());
            MerchantBaby result_merchantBaby = merchantBabyService.queryByMerchantBabyId(merchantBaby.getMerchantBabyId());
            order.setMerchantBaby(result_merchantBaby);
            //设置商品 sku 属性
            CommoditySkuList commoditySkuList = commoditySkuListService.queryBySkuListId(order.getCommodityListId());
            order.setCommoditySkuList(commoditySkuList);
            //设置用户地址
            UserAddress userAddress = userAddressService.queryByUserAddressId(order.getUserAddressId());
            order.setUserAddress(userAddress);
            //设置用户基础订单
            String orderId = order.getOrderId();
            for (OrderBase orderBase : orderBaseList) {
                if (orderBase.getOrderId().equals(orderId)) {
                    order.setOrderBase(orderBase);
                    break;
                }
            }

        }
        //2、去 mysql 中获取其他订单
        List<OrderUser> orderUserListMysql = orderUserService.queryOrderByUserId(userId);
        //查询每个订单中的商品信息 重新赋值 并且添加到集合中
        for (OrderUser orderUser : orderUserListMysql) {
            orderUserList.add(orderUser);
        }
        resultMap.put("orderUserList", orderUserList);
        responseParamInfo = new ResponseParamInfo(200, "查询成功", orderUserList);
        return responseParamInfo;
    }

    /**
     * 根据用户编号 获取用户指定状态订单个数
     *
     * @param userID     用户编号
     * @param orderState 订单状态
     * @return
     */
    @PostMapping("queryOrderState")
    @ApiOperation(value = "获取用户指定状态订单个数",notes = "获取用户指定状态订单个数")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "用户编号",value = "userID"),
            @ApiImplicitParam(paramType = "query",name = "订单状态",value = "orderState"),
    })
    @ApiResponse(code = 200,message = "查询成功")
    public ResponseParamInfo queryOrderState(String userID, String orderState) {
        ResponseParamInfo responseParamInfo = null;
        Integer resultNumber = 0;
        /**
         * 1、获取用户的订单 根据用户订单 获取基础订单
         * 2、通过基础订单中的状态判断指定来计算指定订单个数
         */
        List<OrderUser> orderUserList = orderUserService.queryOrderByUserId(userID);
        for (OrderUser orderUser : orderUserList) {
            OrderBase orderBase = orderUser.getOrderBase();
            if (orderBase.getOrderState().equals(orderState)) {
                resultNumber += 1;
            }
        }
        // 如果是代付款去缓存中查找
        if (orderState.equals("待付款")) {
            //1、去 redis 缓存中获取未支付订单
            String selectKey = "*" + userID; //设置查询样式
            //查询未支付的订单
            Set<String> keys = redisTemplate.keys(selectKey);
            for (String key : keys) {
                String redisCacheOrder = redisTemplate.opsForValue().get(key);
                //格式化从redis中取出来的数据并且强制转换
                if (redisCacheOrder.indexOf("待付款") > 0) {
                    resultNumber += 1;
                }
            }
        }
        responseParamInfo = new ResponseParamInfo(200, "查询成功", resultNumber);
        return responseParamInfo;
    }

    /**
     * 修改用户购买商品数量
     * @param userID 用户编号
     * @param baseKey key名称
     * @param buyNumber 修改数量
     * @return
     */
    @PostMapping("updateByNumber")
    @ApiOperation(value = "修改用户购买商品数量",notes = "修改用户购买商品数量")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "用户编号",value = "userID"),
            @ApiImplicitParam(paramType = "query",name = "订单编号",value = "baseKey"),
            @ApiImplicitParam(paramType = "query",name = "修改数量",value = "buyNumber"),
    })
    @ApiResponse(code = 200,message = "修改成功")
    public ResponseParamInfo updateByNumber(@RequestParam("userID") String userID,
                                            @RequestParam("baseKey") String baseKey,
                                            @RequestParam("buyNumber") String buyNumber){
        ResponseParamInfo responseParamInfo = null;
        System.out.println("userID:"+userID+"\tbaseKey:"+baseKey+"\t\tbuyNumber:"+buyNumber);
        //从缓存中获取数据 并且重新赋值
        String baseOrderStr = redisTemplate.opsForValue().get(baseKey);
        OrderBase orderBase = JSON.parseObject(baseOrderStr,OrderBase.class);
        orderBase.setOrderBuyNum(Integer.parseInt(buyNumber));
        String newOrderStr  = JSON.toJSONString(orderBase);
        redisTemplate.opsForValue().set(baseKey,newOrderStr);
        responseParamInfo = new ResponseParamInfo(200,"PIG_SUCCESS","修改成功");
        return responseParamInfo;
    }


    /**
     * 修改订单状态   确定收货
     * @return
     */
    @PostMapping("updateOrderState")
    @ApiOperation(value = "修改订单状态",notes = "修改订单状态")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "用户编号",value = "userID"),
            @ApiImplicitParam(paramType = "query",name = "订单编号",value = "baseOrderId"),
            @ApiImplicitParam(paramType = "query",name = "修改状态",value = "orderState"),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "收货成功"),
            @ApiResponse(code = 500,message = "收货失败"),
    })
    public ResponseParamInfo updateOrderState(HttpServletRequest request){
        String userID = request.getParameter("userId");
        String baseOrderID = request.getParameter("baseOrderId").split(userID)[0];
        String orderState = request.getParameter("orderState");
        ResponseParamInfo responseParamInfo = null;
        Integer updateResult = orderBaseService.updateOrderState(baseOrderID, orderState);
        if (updateResult>0){
            responseParamInfo = new ResponseParamInfo(200,"PIG_SUCCESS","收货成功");
        }else{
            responseParamInfo = new ResponseParamInfo(500,"PIG_ERROR","收货失败");
        }
        return responseParamInfo;
    }

    /**
     * 删除订单
     * @return
     */
    @DeleteMapping("deleteOrder")
    @ApiOperation(value = "删除订单",notes = "删除订单")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "基础订单编号",value = "baseOrderId"),
            @ApiImplicitParam(paramType = "query",name = "用户订单编号",value = "userOrderId"),
    })
    @ApiResponse(code = 200,message = "SUCCESS")
    public ResponseParamInfo deleteOrder(HttpServletRequest request){
        String baseOrderId = request.getParameter("baseOrderId");
        String userOrderId = request.getParameter("userOrderId");
        redisTemplate.delete(baseOrderId);
        redisTemplate.delete(userOrderId);
        ResponseParamInfo responseParamInfo = new ResponseParamInfo(200,"PIG_SUCCESS","SUCCESS");
        return responseParamInfo;
    }
}
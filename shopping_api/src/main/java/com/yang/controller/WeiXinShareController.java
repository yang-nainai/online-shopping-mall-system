package com.yang.controller;

import com.yang.entity.WinXinEntity;
import com.yang.utils.WeiXinUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * @author yangdacheng
 * @title: WeiXinShareController 微信分享
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/22 7:15 下午
 */
@RestController
@RequestMapping("/weiXin")
@Api(tags = "微信分享接口")
public class WeiXinShareController {

    @Resource
    private RedisTemplate<String,String> redisTemplate;


    @PostMapping("/share")
    public WinXinEntity share(HttpServletRequest request) {
        String access_token = redisTemplate.opsForValue().get("weiXin_accessToken");
        String jsapiToken = redisTemplate.opsForValue().get("weiXin_jsapiTicket");
        WinXinEntity wx = null;
        String strUrl = "http://www.littlepig.xyz/";       //换成安全域名
        if (access_token != null && jsapiToken != null){
//            System.out.println("=======================直接获取签名=======================");
            wx = WeiXinUtils.getWinXinEntity(strUrl,access_token,jsapiToken);
//            System.out.println("wx:"+wx);
//            System.out.println("======================================================");
        }else{
//            System.out.println("=======================获取token=======================");
            wx = WeiXinUtils.getWinXinEntity(strUrl);
//            System.out.println("wx:"+wx);
//            System.out.println("======================================================");
        }
        redisTemplate.opsForValue().set("weiXin_accessToken",wx.getAccess_token(),7100,TimeUnit.SECONDS);
        redisTemplate.opsForValue().set("weiXin_jsapiTicket",wx.getTicket(),7100, TimeUnit.SECONDS);
        return wx;
    }
}

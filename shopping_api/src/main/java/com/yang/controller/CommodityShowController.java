package com.yang.controller;

import com.yang.entity.CommodityShow;
import com.yang.service.CommodityShowService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品图片展示表(CommodityShow)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("commodityShow")
public class CommodityShowController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityShowService commodityShowService;

}
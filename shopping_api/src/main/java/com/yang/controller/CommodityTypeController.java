package com.yang.controller;

import com.yang.entity.CommodityType;
import com.yang.service.CommodityTypeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品类型表用来存放不同商品的类型(CommodityType)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("commodityType")
public class CommodityTypeController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityTypeService commodityTypeService;

}
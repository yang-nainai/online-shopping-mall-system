package com.yang.controller;

import com.yang.entity.CommodityEconomy;
import com.yang.service.CommodityEconomyService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品优惠价格(CommodityEconomy)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("commodityEconomy")
public class CommodityEconomyController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityEconomyService commodityEconomyService;

}
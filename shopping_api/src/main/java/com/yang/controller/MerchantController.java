package com.yang.controller;

import com.yang.entity.Merchant;
import com.yang.service.MerchantService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商家表
(Merchant)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("merchant")
public class MerchantController {
    /**
     * 服务对象
     */
    @Resource
    private MerchantService merchantService;

}
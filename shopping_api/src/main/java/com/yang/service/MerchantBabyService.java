package com.yang.service;

import com.yang.entity.MerchantBaby;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 商家 商品表(MerchantBaby)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface MerchantBabyService {

    /**
     * 根据商品编号查询 商家信息
     * @param commodityId
     * @return
     */
    MerchantBaby queryByCommodityId(String commodityId);

    /**
     * 根据商家宝贝编号获取宝贝信息
     * @param merchantBabyId 商品宝贝编号
     * @return
     */

    MerchantBaby queryByMerchantBabyId(String merchantBabyId);

    /**
     * 添加商品 归宿
     * @param merchantBaby
     * @return
     */
    Integer insertBaby(MerchantBaby merchantBaby);

    /**
     * 根据商家编号获取商家所有商品谢谢
     * @param merchant_id 商家编号
     * @return
     */
    List<MerchantBaby> queryMerchantBaby(String merchant_id);

    /**
     * 根据商家编号获取商家的商品数量
     * @param merchantID
     * @return
     */
    Integer queryMerchantBabyNumber(String merchantID);

    /**
     * 修改商品宝贝
     * @param merchantBaby
     * @return
     */
    Integer updateMerchantBaby(MerchantBaby merchantBaby);

    /**
     * 添加商家商品
     * @param merchantBaby
     * @return
     */
    Integer insertMerchantBaBy(MerchantBaby merchantBaby);
}
package com.yang.service.impl;

import com.yang.entity.CommodityShow;
import com.yang.dao.CommodityShowDao;
import com.yang.service.CommodityShowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品图片展示表(CommodityShow)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commodityShowService")
public class CommodityShowServiceImpl implements CommodityShowService {
    @Resource
    private CommodityShowDao commodityShowDao;

    @Override
    public Integer insertCommodityShow(CommodityShow commodityShow) {
        return commodityShowDao.insertCommodityShow(commodityShow);
    }
}
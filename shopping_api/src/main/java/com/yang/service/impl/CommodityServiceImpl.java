package com.yang.service.impl;

import com.yang.entity.Commodity;
import com.yang.dao.CommodityDao;
import com.yang.entity.utils.PageInfo;
import com.yang.service.CommodityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品表 用来记入一些商品的基本信息(Commodity)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commodityService")
public class CommodityServiceImpl implements CommodityService {
    @Resource
    private CommodityDao commodityDao;

    /**
     * 通过ID查询单条数据
     *
     * @param commodityId 主键
     * @return 实例对象
     */
    @Override
    public Commodity queryById(String commodityId) {
        return this.commodityDao.queryById(commodityId);
    }

    /**
     * 分页查询商品数据
     *
     * @param pageInfo 页面工具类
     * @return 商品集合
     */
    @Override
    public List<Commodity> limitCommodity(PageInfo pageInfo) {
        return this.commodityDao.limitCommodity(pageInfo);
    }

    /**
     * 查询商品总数据条数
     * @return
     */
    public Integer find_commoditySum() {
        return this.commodityDao.find_commoditySum();
    }

    /**
     * 根据用户输入的搜索 获取要查询的信息   --- 模糊查询
     * @param searchName
     * @return
     */
    public List<Commodity> searchCommodity(String searchName){
        return commodityDao.searchCommodity(searchName);
    }

    @Override
    public List<Commodity> queryAllCommodity() {
        return commodityDao.queryAllCommodity();
    }

    @Override
    public Integer updateCommodity(Commodity commodity) {
        return commodityDao.updateCommodity(commodity);
    }

    @Override
    public Integer insertCommodity(Commodity commodity) {
        return commodityDao.insertCommodity(commodity);
    }

}
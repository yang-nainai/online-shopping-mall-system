package com.yang.service.impl;

import com.yang.entity.Economy;
import com.yang.dao.EconomyDao;
import com.yang.service.EconomyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 优惠表(Economy)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("economyService")
public class EconomyServiceImpl implements EconomyService {
    @Resource
    private EconomyDao economyDao;
}
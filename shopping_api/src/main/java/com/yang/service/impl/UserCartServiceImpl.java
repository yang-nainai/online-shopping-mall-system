package com.yang.service.impl;

import com.yang.entity.UserCart;
import com.yang.dao.UserCartDao;
import com.yang.service.UserCartService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户购物车表
(UserCart)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Service("userCartService")
public class UserCartServiceImpl implements UserCartService {
    @Resource
    private UserCartDao userCartDao;

    /**
     * 根据用户编号获取用户购物车信息
     * @param userId 用户编号
     * @return 一些购物车信息
     */
    public List<UserCart> findCartByUserId(String userId){
        return userCartDao.findCartByUserId(userId);
    }

    /**
     * 根据用户编号 获取用户名下购物车内商品数量
     * @param userId 用户编号
     * @return
     */
    public Integer findAllNumber(String userId){
        return userCartDao.findAllNumber(userId);
    }

    /**
     * 根据商品编号查询购物车 判断是否存在
     * @param merchantBabyId 宝贝编号
     * @return
     */
    public UserCart queryByMerchantBabyId(String merchantBabyId,String userId){
        return userCartDao.queryByMerchantBabyId(merchantBabyId,userId);
    }

    /**
     * 修改购物车内中 商品的属性
     * @param cartId 购物车编号
     * @param listId 商品 skulist 集合编号
     * @return 1：成功 0：失败
     */
    public Integer updateCart(String cartId,String listId,Integer buyNum){
        return userCartDao.updateCart(cartId, listId,buyNum);
    }

    /**
     * 将商品添加到用户购物车
     * @param userCart
     * @return
     */
    public Integer insertCart(UserCart userCart){
        return userCartDao.insertCart(userCart);
    }

    /**
     * 根据购物车编号 删除购物车
     * @param cartId 购物车编号
     * @return
     */
    public Integer deleteByCartId(String cartId){
        return userCartDao.deleteByCartId(cartId);
    }
}
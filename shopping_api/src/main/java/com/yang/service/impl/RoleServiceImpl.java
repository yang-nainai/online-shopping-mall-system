package com.yang.service.impl;

import com.yang.entity.Role;
import com.yang.dao.RoleDao;
import com.yang.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 角色表
(Role)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleDao roleDao;
}
package com.yang.service.impl;

import com.yang.entity.UserRole;
import com.yang.dao.UserRoleDao;
import com.yang.service.UserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户角色表
(UserRole)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {
    @Resource
    private UserRoleDao userRoleDao;

    /**
     * 给新添加的用户添加一个角色
     * @param userRole 用户角色
     * @return 1：成功 0：失败
     */
    public Integer insertUserRole(UserRole userRole){
        return userRoleDao.insertUserRole(userRole);
    }
}
package com.yang.service;

import com.yang.entity.CommoditySkuSize;
import java.util.List;

/**
 * 商品的SKU属性--大小(CommoditySkuSize)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface CommoditySkuSizeService {

    /**
     * 添加商品sku大小属性
     * @param commoditySkuSizes
     * @return
     */
    Integer insertCommoditySkuSize(List<CommoditySkuSize> commoditySkuSizes);
}
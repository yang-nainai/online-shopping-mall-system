package com.yang.service;

import com.yang.entity.CommodityDetails;
import java.util.List;

/**
 * 商品图片详情表(CommodityDetails)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface CommodityDetailsService {

    /**
     * 添加商品详情图片
     * @param commodityDetails
     * @return
     */
    Integer insertCommodityDetails(CommodityDetails commodityDetails);
}
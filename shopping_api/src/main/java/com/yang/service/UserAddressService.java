package com.yang.service;

import com.yang.entity.UserAddress;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户地址表(UserAddress)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
public interface UserAddressService {
    /**
     * 根据用户的编号获取 用户地址信息
     * @param userId
     * @return
     */
    List<UserAddress> findAllUserAddress(String userId);

    /**
     * 根据用户地址编号 获取用户地址
     * @param userAddressId 用户地址编号
     * @return 用户地址类
     */
    UserAddress queryByUserAddressId(String userAddressId);

    /**
     * 根据用户地址编号 获取用户地址
     * @param addressId 地址编号
     * @return 用户地址类
     */
    UserAddress queryByAddressId(String addressId);
    /**
     * 根据用户编号 获取用户指定状态地址信息
     * @param userId
     * @param defaultState
     * @return
     */
    List<UserAddress> defaultAddress(String userId,Integer defaultState);

    /**
     * 添加一个新的用户地址
     * @param userAddress
     * @return
     */
    Integer insertUserAddress(UserAddress userAddress);

    /**
     * 查询用户的默认地址
     * @param userId
     * @return
     */
    UserAddress isDefaultUserAddress(String userId,Integer defaultState);

    /**
     * 根据用户的编号 和地址的编号来修改地址的 默认状态 改为 0或者为1
     * @param userAddress 用户地址类
     * @return
     */
    Integer updateDefaultAddress(UserAddress userAddress);

    /**
     * 根据用户的编号、地址编号修改默认状态
     * @param userId 用户编号
     * @param addressId 地址编号
     * @param defaultState 默认信息 1：默认 0：不是
     * @return 1：修改成功 0：修改失败
     */
    Integer updateDefaultState(String userId,String addressId,Integer defaultState);

    /**
     * 更具用户地址编号 和用户编号 删除用户地址
     * @param userId 用户编号
     * @param addressId 地址编号
     * @return 1：删除成功 0：删除失败
     */
    Integer deleteUserAddress(String userId,String addressId);
}
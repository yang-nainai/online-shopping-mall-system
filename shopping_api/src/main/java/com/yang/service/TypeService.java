package com.yang.service;

import com.yang.entity.Type;
import java.util.List;

/**
 * 商品类型编号 用来存放一些商品类型(Type)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
public interface TypeService {

    /**
     * 查询所有的商品类型
     * @return
     */
    List<Type> queryAllType();
}
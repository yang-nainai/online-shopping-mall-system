package com.yang.service;

import com.yang.entity.CommodityType;
import java.util.List;

/**
 * 商品类型表用来存放不同商品的类型(CommodityType)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface CommodityTypeService {

    /**
     * 根据商品的编号来查询商品的类型 一个商品可以有多个类型
     * @param commodityID 商品编号
     * @return 商品类型集合
     */
    List<CommodityType> findCommodityTypeByCommodityId(String commodityID);


    /**
     * 查询所有的类型
     * @return
     */
    List<CommodityType> queryAllType();

    /**
     * 添加商品类型
     * @param commodityType
     * @return
     */
    Integer insertCommodityType(CommodityType commodityType);
}
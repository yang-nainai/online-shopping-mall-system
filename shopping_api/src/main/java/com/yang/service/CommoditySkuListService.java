package com.yang.service;

import com.yang.entity.CommoditySkuList;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商品sku的集合
(CommoditySkuList)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface CommoditySkuListService {
    /**
     * 根据商品sku的编号获取商品的信息
     * @param skuListId 商品sku编号
     * @return 商品信息
     */
    CommoditySkuList queryBySkuListId(String skuListId);

    /**
     * 添加商品sku集合
     * @param commoditySkuListList
     * @return
     */
    Integer insertSkuList(List<CommoditySkuList> commoditySkuListList);
}
package com.yang.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author yangdacheng
 * @title: TimingTask 定时任务
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/13 10:14 上午
 */
@Component
public class TimingTask {

    /**
     * 清空用户上传到服务器上面的文件 -- 清空数据缓存 每天早上5点开始执行
     */
    @Scheduled(cron = "0 0 5 * * ?")
    public void clearUserImg(){
        String userImgPath = "/Users/yangdacheng/Desktop/learn_springboot/SpringBoot-File-UpLoader";
        String messageImg = "/Users/yangdacheng/Desktop/learn_springboot/littleMessageCache";
        String messageFile = "/Users/yangdacheng/Desktop/learn_springboot/littleMessageFile";
        /**
         * 1、判断文件是否为文件夹
         * 2、是文件夹的话就读取下面的子文件
         * 3、删除下面的子文件
         */
        deleteFile(userImgPath);
        deleteFile(messageImg);
        deleteFile(messageFile);
    }

    public void deleteFile(String filepath){
        File file = new File(filepath);
        //如果文件是文件夹
        if (file.exists() && file.isDirectory()){
            File[] childFiles = file.listFiles(); //获取子文件夹
            for (File child: childFiles){
                deleteFile(child.getPath());
            }
        }else if (file.exists() && file.exists()){
            file.delete();
        }
    }

}

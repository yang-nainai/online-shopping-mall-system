package com.yang;

import com.yang.config.RedisConfig;
import com.yang.entity.Commodity;
import com.yang.entity.utils.FileUploadBean;
import com.yang.entity.utils.MessageCache;
import com.yang.entity.utils.MessageInfo;
import com.yang.entity.utils.PageInfo;
import com.yang.service.CommodityService;
import com.yang.utils.*;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@SpringBootTest
class ShoppingApiApplicationTests {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Autowired
    private EmailSendUtils emailSendUtils;
    @Autowired
    private ElasticSearchUtil elasticSearchUtil;
    @Autowired
    private CommodityService commodityService;
    @Resource
    private FileUploadBean fileUploadBean;

    /**
     * 添加到redis中
     */
    @Test
    void contextLoads() {
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.opsForValue().set("key2","阳奈");
        System.out.println(redisTemplate.opsForValue().get("key2"));
    }

    /**
     * 生成 验证码
     */
    @Test
    void buildCode(){
        EmailCodeFactory emailCodeFactory = new EmailCodeFactory();
        String s = emailCodeFactory.buildCode(4);
        System.out.println("S:"+s);
    }

    /**
     * 发送邮件
     */
    @Test
    void sendEmail(){
        emailSendUtils.sendCode("2465047559@qq.com",
                "2465047559@qq.com",
                "测试",
                "测试代码");
    }

    @Test
    void readCache() throws DocumentException {
        File file = new File("/Users/yangdacheng/Desktop/learn_springboot/littleMessageCache/阳奈TO杨楚楚.xml");
        SAXReader reader = new SAXReader();
        Document document = reader.read(file);
        Element rootElement = document.getRootElement();
        Iterator<Element> elementIterator = rootElement.elementIterator("messageCache");
        while (elementIterator.hasNext()){
            Element nextElement = elementIterator.next();
            String fromUserID = nextElement.elementText("fromUserID");
            String sendMessage = nextElement.elementText("sendMessage");
            String toUserID = nextElement.elementText("toUserID");
            String messageAreRead = nextElement.elementText("messageAreRead");
            String sendMessageTime = nextElement.elementText("sendMessageTime");
            System.out.println(fromUserID+"--->"+sendMessage+"--->"+toUserID+"--->"+messageAreRead+"--->"+sendMessageTime);
        }
    }

    @Test
    void fileTest() throws IOException {
//        MessageXml.createFile("阳奈","杨楚楚","/Users/yangdacheng/Desktop/learn_springboot/littleMessageCache/");
        File file = new File("/Users/yangdacheng/Desktop/learn_springboot/littleMessageCache/阳奈TO杨楚楚.xml");
        MessageCache messageCache1 = new MessageCache("张三","李四","张三你好",new Date(),true);
        MessageCache messageCache2 = new MessageCache("张三","李四","李四你好",new Date(),false);

        List<MessageCache> messageCaches = new ArrayList<>();
        messageCaches.add(messageCache1);
        messageCaches.add(messageCache2);
        MessageXml.writeMessage(file,messageCaches);
    }

    @Test
    void updateFile(){
        MessageXml.getMessageMenu("/Users/yangdacheng/Desktop/learn_springboot/littleMessageCache","3089210001");
    }

    @Test
    void spilitText(){
        String str1 = "dsada.jpg_dasda.jpg";
        String str2 = "dsada.jpg";
        System.out.println(str1.split(".jpg")[0].split(".jpg")[0]);
        System.out.println(str2.split(".jpg")[0].split(".jpg")[0]);
    }

    @Test
    void esTest() throws IOException {
        String indexName = "test_name";
        elasticSearchUtil.createEsIndex(indexName);
    }

    @Test
    void fileUploadTest(){
        System.out.println(fileUploadBean.imgFileUrl);
    }
}

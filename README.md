### 项目简介
本项目前端使用VUE+Vant开发作为前端界面的展示，后端使用SpringBoot/Spring Cloud做为API为前端提供数据。
本项目功能：支付功能、发送邮箱功能、搜索功能使用ElasticSearch实现、文件上传到云、聊天功能等。
### 文件夹简介
1. new_shopping存放VUE界面
2. shopping_api基于SpringBoot的API
3. shopping_cloud基于SpringCloud的API

### 项目运行
- VUE项目的运行
    1. 通过CMD进入到项目文件夹输入安装语句 npm install
    2. 安装完成后输入运行语句 npm run serve


### 项目部分运行图
![输入图片说明](picture%E9%A6%96%E9%A1%B5.png) ![输入图片说明](picture%E5%95%86%E5%93%81%E7%95%8C%E9%9D%A2.png) ![输入图片说明](picturesku%E9%80%89%E6%8B%A9%E7%95%8C%E9%9D%A2.png) ![输入图片说明](picture%E7%99%BB%E9%99%86%E9%A1%B5%E9%9D%A2.png)![输入图片说明](picture%E8%81%8A%E5%A4%A9%E7%95%8C%E9%9D%A2.png)![输入图片说明](picture%E8%AE%A2%E5%8D%95%E7%95%8C%E9%9D%A2.png)![输入图片说明](picture%E4%B8%AA%E4%BA%BA%E4%BF%A1%E6%81%AF%E7%95%8C%E9%9D%A2.png)![输入图片说明](picture%E6%94%AF%E4%BB%98%E5%A7%90main.png)
import wx from 'weixin-js-sdk'
import axios from "axios";

const wxApi = {
    /**
     * [wxRegister 微信Api初始化]
     * @param  {Function} callback [ready回调函数]
     */
    wxRegister() { //data是微信配置信息，option是分享的配置内容
        axios({
            url: '/weiXin/share',
            method: 'POST',
        }).then(response => {
            wx.config({
                debug: false, // 开启调试模式
                appId: "wxe92f93a9789b7d42", // 必填，公众号的唯一标识
                timestamp: parseInt(response.data.timestamp), // 必填，生成签名的时间戳
                nonceStr: response.data.noncestr, // 必填，生成签名的随机串
                signature: response.data.signature, // 必填，签名，见附录1
                jsApiList: [
                    'updateAppMessageShareData',  //自定义“分享给朋友”及“分享到QQ”
                    'updateTimelineShareData',    //自定义“分享到朋友圈”及“分享到QQ空间”
                    'onMenuShareWeibo',  //获取“分享到腾讯微博”按钮
                    'scanQRCode',    //调起微信扫一扫接口
                ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
            })
            wx.ready(() => {
                console.log("准备成功")
                wx.updateAppMessageShareData({
                    title: "阳奈", // 分享标题
                    link: location.href.split('#')[0], // 分享链接
                    imgUrl: "https://z3.ax1x.com/2021/11/25/oFsWTK.png", // 分享图标
                    desc: "这是阳奈编写的一个程序，快进来看看！！！", // 分享描述
                    success() {
                        // 用户成功分享后执行的回调函数
                        // option.success()
                        alert("分享成功")
                    },
                    cancel() {
                        // 用户取消分享后执行的回调函数
                        // option.error()
                        alert("取消分享")
                    }
                })
                wx.updateTimelineShareData({
                    title: "阳奈", // 分享标题
                    link: location.href.split('#')[0], // 分享链接
                    imgUrl: "https://z3.ax1x.com/2021/11/25/oFsWTK.png", // 分享图标
                    desc: "这是阳奈编写的一个程序，快进来看看！！！", // 分享描述
                    success() {
                        // 用户成功分享后执行的回调函数
                        // option.success()
                        alert("分享成功")
                    },
                    cancel() {
                        // 用户取消分享后执行的回调函数
                        // option.error()
                        alert("取消分享")
                    }
                })
                wx.onMenuShareWeibo({
                    title: "阳奈", // 分享标题
                    link: location.href.split('#')[0], // 分享链接
                    imgUrl: "https://z3.ax1x.com/2021/11/25/oFsWTK.png", // 分享图标
                    desc: "这是阳奈编写的一个程序，快进来看看！！！", // 分享描述
                    success() {
                        // 用户成功分享后执行的回调函数
                        // option.success()
                        alert("分享成功")
                    },
                    cancel() {
                        // 用户取消分享后执行的回调函数
                        // option.error()
                        alert("取消分享")
                    }
                })
            })
        }).catch(error => {
            console.log(error);
        })
    }
}
export default wxApi

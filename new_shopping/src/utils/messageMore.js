// 聊天界面跟多选项
import wx from 'weixin-js-sdk'
import axios from "axios";
const messageMore = {
    uploadFile(userChooseFile){
        //上传文件
        let params = new FormData();
        params.append("messageFile", userChooseFile.file);
        let imgConfig = {
            headers: {
                //添加请求头
                "Content-Type": "multipart/form-data"
            },
        };
        this.$axios({
            url: '/message/messageTextFile',
            method: 'POST',
            data: params,
            config: imgConfig
        }).then(response => {
            return response.data
            //上传成功后就发送消息
        }).catch(error => {
            console.log(error);
        })
    }
}

export default messageMore

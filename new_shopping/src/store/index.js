import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        oauth:{
            client_id: "yangnai",
            client_secret: "123456",
            grant_type: "password"
        }, //oauth模式
        choose_address: {
            id: ''
        },  //在开始的时候获取用户的默认地址 用户在确定订单后 更换地址 将地址存放到vuex中
        // 选中的商品信息
        choose_commodity: {},
        // 用户信息
        user_context:{
            userId: '',//3089210001
        },
        // 用来存放用户所有的地址
        user_address:[],
        user_choose:[], //用户选中的商品
        chooseSureOrderList:[], //用来存放用户提交的商品
        ordersID:[], //用来存放用户订单在redis中的商品
        userOrder:[], //用户订单
        toUserId: "", //设置聊天对象编号
        toUser:{
            userId: "",
            userInternetName: "",
            userPhotoImg: "",
        },//设置聊天对象
        messageNoReader: 0, //消息未读数量
    },
    mutations: {
        //设置选择后的地址
        set_choose_address(state, context) {
            this.state.choose_address = context;
        },
        // 设置选中的商品信息
        set_choose_commodity(state, context) {
            this.state.choose_commodity = context
        },
        // 设置用户信息
        set_user_context(state,context){
            state.user_context = context
        },
        // 设置用户地址
        set_user_address(state,context){
            state.user_address = context
        },
        //设置商品
        setChooseSureOrderList(state,context){
            state.chooseSureOrderList = context
        },
        //设置用户订单编号
        setRedisKeys(state,context){
            state.ordersID = context
        },
        //设置用户订单
        setUserOrder(state,context){
            state.userOrder = context
        },
        //设置聊天对象编号
        setToUserId(state,context){
            state.toUserId = context
        },
        //设置聊天对象
        setToUser(state,context){
            state.toUser = context
        },
        //设置消息未读数量
        setMessageNoReader(state,context){
            state.messageNoReader = context
        },

    },
    //异步操作
    actions: {
        //设置选择后的地址
        set_choose_address(context) {
            this.state.choose_address = context;
        },
        // 设置选中的商品信息
        set_choose_commodity(context) {
            this.state.choose_commodity = context
        },
        // 设置用户信息
        set_user_context(context){
            this.state.user_context = context
        },
        // 设置用户地址
        set_user_address(context){
            this.state.user_address = context
        },
        //设置商品
        setChooseSureOrderList(context){
            this.state.chooseSureOrderList = context
        },
        //设置用户订单编号
        setRedisKeys(state,context){
            this.state.ordersID = context
        },
        //设置用户订单
        setUserOrder(state,context){
            this.state.userOrder = context
        },
        //设置聊天对象编号
        setToUserId(state,context){
            this.state.toUserId = context
        },
        //设置聊天对象
        setToUser(state,context){
            this.state.toUser = context
        },
        //设置消息未读数量
        setMessageNoReader(state,context){
            this.state.messageNoReader = context
        },
    },
    modules: {}
})

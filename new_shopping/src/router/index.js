import Vue from 'vue'
import VueRouter from 'vue-router'
import Home_view from '../views/Home/Home_view'  //导入 首页 视图
import Message_view from "../views/Message/Message_view"; //导入 消息 视图
import Cart_view from "../views/Cart/Cart_view";  //导入 购物车 视图
import Profile_view from "../views/Profile/Profile_view";//导入 我的 视图
import Profile_login from "../views/Profile/Profile_login";//导入 登陆界面
import Profile_sing_in from "../views/Profile/Profile_sing_in";//导入注册界面
import Profile_getContext from "../views/Profile/Profile_getContext"; //获取用户信息变量
import UserProfile from "../views/Profile/UserProfile";  //用户个人信息
//导入 商品详情页面
import Commodity_profile_view from "../views/Commodity/Commodity_profile_view";
import Search_view from "../views/Search/Search_view";//导入 搜索 页面
import Show_all_address from "../views/Address/Show_all_address"; //导入 显示 所有地址
import Insert_address from "../views/Address/Insert_address";//导入 新增 地址
import Message_home from "../views/Message/Message_home"; //导入聊天室
import Sure_order from "../views/Order/Sure_order"; //导入确定订单页面
import Tip_message from "../views/Tip_view/Tip_message";// 注入购买成功界面
import OrderList from "../views/Order/OrderList"; //导入订单页面
//导入 支付
import Return_url from "../views/Alipay/Return_url";
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home_view,
  },
  {
    path: '/message_view',
    name: 'Message_view',
    component: Message_view
  },
  {
    path: '/cart_view',
    name: 'Cart_view',
    component: Cart_view
  },
  {
    path: '/profile_view',
    name: 'Profile_view',
    component: Profile_view
  },
  {
    path: '/profile_login',
    name: 'Profile_login',
    component: Profile_login
  },
  {
    path: '/profile_sing_in',
    name: 'Profile_sing_in',
    component: Profile_sing_in
  },
  {
    path: '/commodity_profile_view',
    name: 'Commodity_profile_view',
    component: Commodity_profile_view
  },
  {
    path: '/search_view',
    name: 'Search_view',
    component: Search_view
  },
  {
    path: '/show_all_address/:view_name',
    name: 'Show_all_address',
    component: Show_all_address
  },
  {
    path: '/insert_address',
    name: 'Insert_address',
    component: Insert_address
  },
  {
    path: '/message_home/:merchant_name',
    name: 'Message_home',
    component: Message_home,
  },
  {
    path: '/sure_order',
    name: 'Sure_order',
    component: Sure_order,
  },
  {
    path: '/tip_message/:tip',
    name: 'Tip_message',
    component: Tip_message,
  },
  {
    path: '/orderList/:active',
    name: 'OrderList',
    component: OrderList
  },
  {
    path: '/profile_getContext',
    name: 'Profile_getContext',
    component: Profile_getContext
  },
  {
    path: "/return_url",
    name: "Return_url",
    component: Return_url
  },
  {
    path: "/userProfile",
    name: "UserProfile",
    component: UserProfile
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})



const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (to) {
  return VueRouterPush.call(this, to).catch(err => err)
}



export default router

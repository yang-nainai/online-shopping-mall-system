import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'vant/lib/index.css'
import 'vant/lib/index.less'
import { Lazyload } from 'vant';
import axios from "axios";
import qs from 'qs'
import global from '../src/utils/global'

Vue.config.productionTip = false
Vue.use(Lazyload);  //开启图片懒加载模式


// 设置基础URL为后端服务api地址 192.168.207.218
// axios.defaults.baseURL = "http://littlepig.xyz:6035"   //# 服务器地址
axios.defaults.baseURL = "http://192.168.213.218:6035"   //# 服务器地址

//解决post没有参数的问题
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

let axiosService = axios.create({
  baseURL: "http://192.168.31.221:6035",
  // baseURL: "http://192.168.213.218:6035",
  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',
  headers: {
    // 'Content-Type': 'application/json;charset=utf-8',
    'Content-Type': 'application/x-www-form-urlencoded',
  },// 设置传输内容的类型和编码
  withCredentials: false,// 指定某个请求应该发送凭据。允许客户端携带跨域cookie，也需要此配置
});

// 使用拦截器，定义全局请求头
axiosService.interceptors.request.use(config => {
  // 在请求头中添加token
  config.headers.Authorization = "Bearer "+window.localStorage.getItem("access_token")
  return config
})

// 设置csrf请求头
axios.defaults.xsrfCookieName = 'XSRF-TOKEN'
axios.defaults.xsrfHeaderName = 'X-XSRF-TOKEN'

// 将API方法绑定到全局
Vue.prototype.$axios = axiosService
Vue.prototype.$qs = qs

//全局绑定
Vue.prototype.global = global
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

package com.yang.service;

import com.yang.entity.Commodity;
import com.yang.entity.utils.PageInfo;

import java.util.List;

/**
 * 商品表 用来记入一些商品的基本信息(Commodity)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface CommodityService {

    /**
     * 通过ID查询单条数据
     *
     * @param commodityId 主键
     * @return 实例对象
     */
    Commodity queryById(String commodityId);

    /**
     * 分页查询商品数据
     *
     * @param pageInfo 页面工具类
     * @return 商品集合
     */
    List<Commodity> limitCommodity(PageInfo pageInfo);

    /**
     * 查询商品总数据条数
     * @return
     */
    Integer find_commoditySum();

    /**
     * 根据用户输入的搜索 获取要查询的信息   --- 模糊查询
     * @param searchName
     * @return
     */
    List<Commodity> searchCommodity(String searchName);

    /**
     * 查询所有的商品
     * @return
     */
    List<Commodity> queryAllCommodity();

    /**
     * 修改商品信息
     * @param commodity
     * @return
     */
    Integer updateCommodity(Commodity commodity);

    /**
     * 添加商品信息
     * @param commodity
     * @return
     */
    Integer insertCommodity(Commodity commodity);
}
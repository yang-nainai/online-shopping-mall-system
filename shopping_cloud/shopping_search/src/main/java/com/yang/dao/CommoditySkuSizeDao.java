package com.yang.dao;

import com.yang.entity.CommoditySkuSize;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 商品的SKU属性--大小(CommoditySkuSize)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommoditySkuSizeDao {
    /**
     * 获取商品的 sku 第二个属性（大小/尺码等）
     * @param commodity_id 商品编号
     * @return
     */
    @Select("SELECT * FROM commodity_sku_size WHERE commodity_id = #{commodity_id}")
    @Results(value = {
            @Result(property = "skuSizeId", column = "sku_size_id", id = true),
            @Result(property = "skuKeyName", column = "sku_key_name"),
            @Result(property = "skuSizeName", column = "sku_size_name"),
            @Result(property = "commodityId", column = "commodity_id"),
    },id = "sku_size")
    List<CommoditySkuSize> findAllSizeByCommodityId(String commodity_id);

    /**
     * 根据 sku_size 编号 获取sku属性
     * @param sku_size_id sku_size编号
     * @return
     */
    @Select("SELECT * FROM commodity_sku_size WHERE sku_size_id = #{sku_size_id}")
    @ResultMap(value = "sku_size")
    CommoditySkuSize find_size_by_id(String sku_size_id);

    /**
     * 添加商品sku大小属性
     * @param commoditySkuSizes
     * @return
     */
    @Insert({
            "<script>",
            "INSERT INTO commodity_sku_size(sku_size_id, commodity_id,sku_key_name,sku_size_name) VALUES ",
            // collection 和 value对应   item代表循环中List的这一项IpMap
            "<foreach collection='commoditySkuSizes' item='item' index='index' separator=','>",
                "(#{item.skuSizeId}, #{item.commodityId},#{item.skuKeyName},#{item.skuSizeName})",
            "</foreach>",
            "</script>"
    })
    Integer insertCommoditySkuSize(List<CommoditySkuSize> commoditySkuSizes);
}
package com.yang.dao;

import com.yang.entity.CommoditySkuList;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 商品sku的集合
(CommoditySkuList)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommoditySkuListDao {
    /**
     * 获取商品 sku 列表集合（组合列表）
     * @param commodity_id
     * @return
     */
    @Select("SELECT * FROM commodity_sku_list WHERE commodity_id = #{commodity_id}")
    @Results(id = "skuList",value = {
            @Result(property = "skuListId", column = "sku_list_id"),
            @Result(property = "skuColorId", column = "sku_color_id"),
            @Result(property = "skuSizeId", column = "sku_size_id"),
            @Result(property = "commodityId", column = "commodity_id"),
            @Result(property = "commodityPrice", column = "commodity_price"),
            @Result(property = "commodityStockNum", column = "commodity_stock_num"),
            @Result(property = "commodityListName", column = "commodity_list_name"),
            @Result(property = "commoditySkuColor", column = "sku_color_id", one = @One(
                    select = "com.yang.dao.CommoditySkuColorDao.find_sku_color_by_id"
            )),
            @Result(property = "commoditySkuSize", column = "sku_size_id", one = @One(
                    select = "com.yang.dao.CommoditySkuSizeDao.find_size_by_id"
            )),
    })
    List<CommoditySkuList> findAllListByCommodityId(String commodity_id);

    /**
     * 根据商品sku的编号获取商品的信息
     * @param skuListId 商品sku编号
     * @return 商品信息
     */
    @Select("SELECT * FROM commodity_sku_list WHERE sku_list_id = #{skuListId}")
    @ResultMap("skuList")
    CommoditySkuList queryBySkuListId(String skuListId);

    /**
     * 添加商品sku集合
     * @param commoditySkuListList
     * @return
     */
    @Insert({
            "<script>",
            "INSERT INTO commodity_sku_list(sku_list_id, commodity_id,sku_color_id,sku_size_id,commodity_price," +
                    "commodity_stock_num,commodity_list_name) VALUES ",
            // collection 和 value对应   item代表循环中List的这一项IpMap
            "<foreach collection='commoditySkuListList' item='item' index='index' separator=','>",
                "(#{item.skuListId}, #{item.commodityId},#{item.skuColorId},#{item.skuSizeId},#{item.commodityPrice}," +
                        "#{item.commodityStockNum},#{item.commodityListName})",
            "</foreach>",
            "</script>"
    })
    Integer insertSkuList(List<CommoditySkuList> commoditySkuListList);
}
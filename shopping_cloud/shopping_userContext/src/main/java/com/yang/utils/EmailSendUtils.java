package com.yang.utils;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author yangdacheng
 * @title: EmailSendUtils 邮件发送工具类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/10/19 9:25 上午
 */
@Component
public class EmailSendUtils {

    @Resource
    private JavaMailSender mailSender;

    /**
     * 发送邮件
     * @param fromEmail  发送人
     * @param toEmail 接受者
     * @param sendText 发送消息
     * @param subject 发送主题
     */
    public void sendCode(String fromEmail,String toEmail,
                           String sendText,String subject){
        //发送邮件
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(fromEmail);
        mailMessage.setTo(toEmail);
        mailMessage.setText(sendText);
        mailMessage.setSubject(subject);
        mailSender.send(mailMessage);
    }
}

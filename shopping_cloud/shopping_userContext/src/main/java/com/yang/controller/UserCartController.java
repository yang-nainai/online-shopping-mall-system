package com.yang.controller;

import com.yang.entity.CommoditySkuList;
import com.yang.entity.MerchantBaby;
import com.yang.entity.UserCart;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.MerchantBabyService;
import com.yang.service.UserCartService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * 用户购物车表
(UserCart)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@RestController
@RequestMapping("/userCart/cloud")
@Api(tags = "购物车接口")
public class UserCartController {
    /**
     * 服务对象
     */
    @Resource
    private UserCartService userCartService;
    @Resource
    private MerchantBabyService merchantBabyService;

    private ResponseParamInfo<Object> responseParamInfo = null;

    /**
     * 根据用户编号获取购物车内容
     * @param userId
     * @return
     */
    @GetMapping("/getUserCart")
    @ApiOperation(value = "根据用户编号获取购物车内容",notes = "根据用户编号获取购物车内容")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "userId",name = "用户编号"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> getUserCart(@RequestParam("userId") String userId){
//        System.out.println("获取购物车："+userId);
        List<UserCart> cartByUserId = userCartService.findCartByUserId(userId);
        //设置用户选择的商品属性 购买数量
        /**
         * BUG ---- 同一件商品不同属性的时候会蹦
         */
        for (int index=0;index<cartByUserId.size();index++){
            CommoditySkuList choose_skuList = cartByUserId.get(index).getCommoditySkuList();
            cartByUserId.get(index).getMerchantBaby().get(0).setCommoditySkuList(choose_skuList);
            cartByUserId.get(index).setCommoditySkuList(null); //清空选择
            Integer merchantBabyNum = cartByUserId.get(index).getMerchantBabyNum(); //当前购买的数量
            cartByUserId.get(index).getMerchantBaby().get(0).setMerchantBabyNum(merchantBabyNum); //设置购买数量
            cartByUserId.get(index).setMerchantBabyNum(0);  //清空外层的购买数量
            String cartId = cartByUserId.get(index).getCartId();
            cartByUserId.get(index).getMerchantBaby().get(0).setCartId(cartId);
        }
        //格式化放回的数据格式 将同一家商品放在同一家
        /**
         * 遍历获取的集合 将第一个的商家编号获取出来
         * 将获取出来的商家编号 与 后面的商家编号进行对比 相同 就吧商品信息添加到这个商家中
         * 获取添加到商家中的下标 并从集合中移除出去
         * 外层循环：用户遍历所有的的集合 开始：0   条件：小于集合长度
         * 用一个变量存储 获取的第一个编号
         * 内存循环：用来比较 开始：外层循环的下标+1 条件：小于集合长度 - 1
         * 判断：相等就 加入到当前商品数组中 移除出集合
         * 加入到一个新的集合 返回出去
         */
        for (int cart_index=0;cart_index<cartByUserId.size();cart_index++){
            String cartMerchantId = cartByUserId.get(cart_index).getMerchantBaby().get(0).getMerchantId(); //获取当前的商家编号
            for (int index =cart_index+1;index<cartByUserId.size();index++){
                //判断前者和后者的商家编号是否相同
                if (cartMerchantId.equals(cartByUserId.get(index).getMerchantBaby().get(0).getMerchantId())){
                    cartByUserId.get(cart_index).getMerchantBaby().add(cartByUserId.get(index).getMerchantBaby().get(0));
//                    cartByUserId.get(index).getMerchantBaby().get(0).setMerchant(null); //清空当前商家信息
                    cartByUserId.remove(index);  //删除后 下标向前走了一个
                    index--; // 循环后退
                }
            }
        }
        if (cartByUserId != null){
            responseParamInfo = new ResponseParamInfo<Object>(200,"获取成功",cartByUserId);
        }else {
            responseParamInfo = new ResponseParamInfo<Object>(200,"暂无商品....",null);
        }
        return responseParamInfo;
    }

    /**
     * 获取用户购物车下商品数量
     * @param userId
     * @return
     */
    @GetMapping("/getCommodityNumInCart")
    @ApiOperation(value = "获取用户购物车下商品数量",notes = "获取用户购物车下商品数量")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "userId",name = "用户编号"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> getCommodityNumInCart(@RequestParam("userId") String userId){
        Integer allNumber = userCartService.findAllNumber(userId);
        return responseParamInfo = new ResponseParamInfo<Object>(200,"获取成功",allNumber);
    }


    /**
     * 修改用户的购物车属性 根据购物车编号修改集合属性
     * @param cartId 购物车编号
     * @param listId 选项编号
     * @param
     * @return
     */
    @PutMapping("/updateCommodityListInCart")
    @ApiOperation(value = "修改用户的购物车属性",notes = "修改用户的购物车属性")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "cartId",name = "购物车编号"),
            @ApiImplicitParam(paramType = "query",value = "listId",name = "商品skulist编号"),
            @ApiImplicitParam(paramType = "query",value = "buyNum",name = "购买数量"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> updateCommodityListInCart(@RequestParam("cartId") String cartId,
                                                               @RequestParam("listId") String listId,
                                                               @RequestParam("buyNum") Integer buyNum){
        System.out.println("购物车编号："+cartId+"\tlist编号："+listId);
        //修改选择属性
        Integer updateCartResult = userCartService.updateCart(cartId, listId,buyNum);
        if (updateCartResult>0){
            responseParamInfo = new ResponseParamInfo<Object>(200,"修改成功",null);
        }else{
            responseParamInfo = new ResponseParamInfo<Object>(500,"修改失败",null);
        }
        return responseParamInfo;
    }

    /**
     * 添加一个新的购物车
     * 1、先判断该用户购物车中是否存在该商品 存在就在原有的基础上加1
     * 2、不存在就将该商品添加到购物车中
     * @param userCart 购物车类
     * @return
     */
    @PostMapping("/insertCart")
    @ApiOperation(value = "添加一个新的购物车",notes = "添加一个新的购物车")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "userCart",name = "购物车类"),
            @ApiImplicitParam(paramType = "query",value = "goodsId",name = "商品编号"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> insertCart(@RequestBody UserCart userCart,
                                        @RequestParam("goodsId") String goodsId){
        //根据商品的编号获取商品宝贝消息 将宝贝编号添加到购物车类中
        MerchantBaby merchantBaby = merchantBabyService.queryByCommodityId(goodsId);
        String merchantBabyId = merchantBaby.getMerchantBabyId();
        userCart.setMerchantBabyId(merchantBabyId);
        //生成购物车编号
        String cartId = UUID.randomUUID().toString();
        userCart.setCartId(cartId);
        System.out.println("userCart:"+userCart+"\tgoodsId:"+goodsId);
        //查询出商品消息 判断商品在购物车中是否存在
        UserCart isNewCart = userCartService.queryByMerchantBabyId(merchantBabyId,userCart.getUserId());
        if (isNewCart == null){
            //不存在 添加新的商品
            Integer insertResult = userCartService.insertCart(userCart);
            if (insertResult>0){
                responseParamInfo = new ResponseParamInfo<Object>(200,"添加成功",null);
            }else{
                responseParamInfo = new ResponseParamInfo<Object>(500,"添加失败",null);
            }
        }else{
            //存在 修改商品数量
            Integer updateResult = userCartService.updateCart(isNewCart.getCartId(),
                    userCart.getMerchantBabyListId(), isNewCart.getMerchantBabyNum() + userCart.getMerchantBabyNum());
            if (updateResult>0){
                responseParamInfo = new ResponseParamInfo<Object>(200,"修改成功",null);
            }else {
                responseParamInfo = new ResponseParamInfo<Object>(500,"修改失败",null);
            }
        }

        return responseParamInfo;
    }

    /**
     * 根据购物车编号删除购物车
     * @param cartId 购物车编号
     * @return
     */
    @DeleteMapping("/deleteCart")
    @ApiOperation(value = "根据购物车编号删除购物车",notes = "根据购物车编号删除购物车")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "cartId",name = "购物车编号"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> deleteCart(@RequestParam("cartId") String cartId){
        Integer deleteByCartId = userCartService.deleteByCartId(cartId);
        if (deleteByCartId>0){
            responseParamInfo = new ResponseParamInfo<Object>(200,"删除成功",null);
        }else{
            responseParamInfo = new ResponseParamInfo<Object>(500,"删除失败",null);

        }
        System.out.println("cartId:"+cartId);
        return responseParamInfo;
    }
}
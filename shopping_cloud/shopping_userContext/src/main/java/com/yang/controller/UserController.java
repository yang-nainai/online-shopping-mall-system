package com.yang.controller;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import com.yang.entity.User;
import com.yang.entity.UserRole;
import com.yang.entity.utils.FileUploadBean;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.UserRoleService;
import com.yang.service.impl.UserServiceImpl;
import com.yang.utils.EmailCodeFactory;
import com.yang.utils.EmailSendUtils;
import com.yang.utils.UserNameFactory;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 用户表
 * (User)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@RestController
@RequestMapping("/user/cloud")
@Api(tags = "用户接口")
@Slf4j
public class UserController{
    /**
     * 服务对象
     */
    @Resource
    private UserServiceImpl userService;
    @Resource
    private UserRoleService userRoleService;
    /**
     * redis数据缓存
     */
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private EmailSendUtils emailSendUtils;
    @Resource
    private FileUploadBean fileUploadBean;
    private ResponseParamInfo<Object> responseParamInfo = null;

    /**
     * 用户登陆 api
     *
     * @param username 用户名称
     * @param password 用户密码
     * @return 返回信息封装类
     */
    @PostMapping("/userLogin")
    @ApiOperation(value = "用户登陆",notes = "用户登陆")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "username",name = "用户账号"),
            @ApiImplicitParam(paramType = "query",value = "password",name = "用户密码"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> userLogin(@RequestParam("username") String username,
                                       @RequestParam("password") String password){
        log.info("开始登陆:{},{}",username,password);
        User user = userService.queryByUsername(username);
//        User user = userService.userLogin(username, password);
        if (user != null) {
            if (passwordEncoder.matches(password,user.getLittlePigPassword())){
                responseParamInfo = new ResponseParamInfo<Object>(200, "登陆成功...", user);
            }else{
                responseParamInfo = new ResponseParamInfo<Object>(500, "密码错误...", null);
            }
        } else {
            responseParamInfo = new ResponseParamInfo<Object>(500, "账号不存在...", null);
        }
        return responseParamInfo;
    }

    /**
     * 跟用户发送邮件 获取验证码
     *
     * @param user_email
     * @return
     */
    @PostMapping("/sendEmail")
    @ApiOperation(value = "跟用户发送邮件 获取验证码",notes = "跟用户发送邮件 获取验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "user_email",name = "用户邮箱"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> sendEmail(@RequestParam("user_email") String user_email) {
        System.out.println("邮箱为：" + user_email);
        //生成验证码
        EmailCodeFactory emailCodeFactory = new EmailCodeFactory();
        String buildCode = emailCodeFactory.buildCode(4);
        try {
            //发送邮件消息
            emailSendUtils.sendCode("2465047559@qq.com",
                    user_email,
                    buildCode,
                    "小粉猪注册验证码");
            System.out.println("验证码为：" + buildCode);
            //将获取的验证码 存放到redis中 key为用户邮箱 value：验证码 设置过期时间为3分钟
            //问题 存放有序列化问题
            redisTemplate.opsForValue().set(user_email, buildCode, 60 * 3, TimeUnit.SECONDS);
        } catch (Exception e) {
            responseParamInfo = new ResponseParamInfo<Object>(500, "网络请求错误", null);
        }
        responseParamInfo = new ResponseParamInfo<Object>(200, "验证码发送成功", null);
        return responseParamInfo;
    }

    /**
     * 用户注册
     *
     * @param user_email 用户邮箱
     * @param inputCode  用户输入的验证码
     * @return 返回消息类
     */
    @PostMapping("/userSingIn")
    @ApiOperation(value = "用户注册",notes = "用户注册")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "user_email",name = "用户邮箱"),
            @ApiImplicitParam(paramType = "query",value = "input_code",name = "用户输入的验证码"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> userSingIn(@RequestParam("user_email") String user_email,
                                        @RequestParam("input_code") String inputCode) {
        inputCode = inputCode.toUpperCase();
        String get_code = redisTemplate.opsForValue().get(user_email); //获取缓存的验证码

        if (get_code != null) {
            //验证码输入错误
            if (inputCode.equals(get_code)) {
                responseParamInfo = new ResponseParamInfo<Object>(200, "验证码正确", null);
            } else {
                responseParamInfo = new ResponseParamInfo<Object>(500, "验证码输入错误", null);
            }
        } else {
            responseParamInfo = new ResponseParamInfo<Object>(500, "验证码过期，重新获取", null);
        }
        return responseParamInfo;
    }


    /**
     * 文件上传
     * @param multipartFile 文件上传
     * @return
     */
    @PostMapping("/userImgUpLoad")
    @ApiOperation(value = "文件上传",notes = "文件上传")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "fileUpload",name = "上传的文件"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> userImgUpLoad(@RequestParam("fileUpload") MultipartFile multipartFile){
        if (multipartFile.isEmpty()){
            responseParamInfo = new ResponseParamInfo<Object>(500,"文件上传失败",null);
        }
        //生成 uuid编号变成文件地址 避免重名文件覆盖
        UUID uuid = UUID.randomUUID();
        String fileName = uuid.toString().replace("-","");
        String oldName = multipartFile.getOriginalFilename();  //获取文件名称
//        String filePath = "/Users/yangdacheng/Desktop/learn_springboot/SpringBoot-File-UpLoader/"; //文件存放地址
//        String filePath = "/home/shopping/littleUserImageCache"; //文件存放地址
        String filePath = fileUploadBean.getImgFileUrl(); //文件存放地址
        File file = new File(filePath+fileName+oldName); //生成文件
        try{
            multipartFile.transferTo(file);  //文件上传
            responseParamInfo = new ResponseParamInfo<Object>(200,"文件上传成功",filePath+file.getName());
        }catch (Exception e){
            responseParamInfo = new ResponseParamInfo<Object>(500,"文件上传失败",null);
        }

        return responseParamInfo;
    }


    /**
     * 添加一个新的用户
     *
     * @param user 用户对象
     * @return
     */
    @PostMapping("/userInsert")
    @ApiOperation(value = "添加一个新的用户",notes = "添加一个新的用户")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "user",name = "用户对象"),
    })
    @ApiResponse(code = 200, message = "responseParamInfo")
    public ResponseParamInfo<Object> userInsert(@RequestBody User user) {
        System.out.printf("传递的用户信息为："+user);
        //生成 userId 用户角色
        String userId = UUID.randomUUID().toString().replace("-","");
        user.setUserId(userId);
        //生成一个用户账号 生成指定区间的随机数
        String userName = UserNameFactory.userNameBuild(100000000, 999999999);
        user.setLittlePigUsername(userName);
        //用户密码加密
        user.setLittlePigPassword(new BCryptPasswordEncoder().encode(user.getLittlePigPassword()));

        /**
         * 将用户的头像上传到对象存储中 -- 腾讯云
         */
        //1、初始化用户身份信息（secretId, secretKey）
        String secretId = "AKIDVnpa4IGxNWOzJM9gwvAAtQgfNoytXu0p";
        String secretKey = "ZtxkIZZIkwrVsynLccMo0wbz6wV5SaS3";
        COSCredentials credentials = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置 bucket 的地域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region("ap-guangzhou");  //连接到自己服务器 广州
        ClientConfig clientConfig = new ClientConfig(region);
        // 这里建议设置使用 https 协议
        // 从 5.6.54 版本开始，默认使用了 https  由于是本地请求 用户
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(credentials, clientConfig);
        // 5、将用户头像存放到本机服务器后 在存放到存储桶中
        String userPhotoImg = user.getUserPhotoImg();
        File file = new File(userPhotoImg); //生成文件
        System.out.println("fileName:"+file.getName());
        // 6、指定文件将要存放的存储桶
        String bucketName = "little-pig-1258768616";
        // 7、指定文件上传对象件
        String fileKey = "/用户头像/";
        // 8、将文件存放到存储桶中
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,fileKey+file.getName(),file);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        String returnFileURL = cosClient.getObjectUrl(bucketName,fileKey) + file.getName();
        user.setUserPhotoImg(returnFileURL);

        System.out.println(user.toString());

        // 添加用户到数据库
        Integer insertUserResult = userService.insertUser(user);
        if (insertUserResult > 0) {
            // 添加用户的角色信息到数据库 默认就是基础用户
            String userRoleId = UUID.randomUUID().toString();
            UserRole userRole = new UserRole(userRoleId, userId, "30890010001", null);
            Integer insertUserRoleResult = userRoleService.insertUserRole(userRole);
            if (insertUserRoleResult > 0) {
                responseParamInfo = new ResponseParamInfo<Object>(200, "注册成功", userName);
            } else {
                responseParamInfo = new ResponseParamInfo<Object>(500, "用户角色注册失败", null);
            }
        } else {
            responseParamInfo = new ResponseParamInfo<Object>(500, "用户添加失败", null);
        }
        return responseParamInfo;
    }

    /**
     * 根据用户编号获取用户名称
     * @param username
     * @return
     */
    @PostMapping("/queryUserByUserName")
    public User queryUserByUserName(@RequestParam("username") String username){
        System.out.println("开始获取用户根据名称。。。。");
        return userService.queryByUsername(username);
    }


}
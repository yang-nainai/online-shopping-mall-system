package com.yang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author yangdacheng
 * @title: ShoppingUserContext2047
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/14 9:35 下午
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
@EnableDiscoveryClient
public class ShoppingUserContext2047 {
    public static void main(String[] args) {
        SpringApplication.run(ShoppingUserContext2047.class,args);
    }
}

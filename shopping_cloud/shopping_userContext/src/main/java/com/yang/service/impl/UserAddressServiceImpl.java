package com.yang.service.impl;

import com.yang.dao.UserAddressDao;
import com.yang.entity.UserAddress;
import com.yang.service.UserAddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户地址表(UserAddress)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Service("userAddressService")
public class UserAddressServiceImpl implements UserAddressService {
    @Resource
    private UserAddressDao userAddressDao;

    /**
     * 根据用户编号 获取对应的地址编号
     * @param userId 用户编号
     * @return 用户地址集合
     */
    @Override
    public List<UserAddress> findAllUserAddress(String userId) {
        return userAddressDao.findAllUserAddress(userId);
    }

    /**
     * 根据用户地址编号 获取用户地址
     * @param userAddressId 用户地址编号
     * @return 用户地址类
     */
    public UserAddress queryByUserAddressId(String userAddressId){
        return userAddressDao.queryByUserAddressId(userAddressId);
    }

    /**
     * 根据用户地址编号 获取用户地址
     * @param addressId 地址编号
     * @return 用户地址类
     */
    public UserAddress queryByAddressId(String addressId){
        return userAddressDao.queryByAddressId(addressId);
    }

    /**
     * 根据用户编号 获取用户指定状态地址信息
     * @param userId
     * @param defaultState
     * @return
     */
    public List<UserAddress> defaultAddress(String userId,Integer defaultState){
        return userAddressDao.defaultAddress(userId, defaultState);
    }
    /**
     * 添加一个新的用户地址
     * @param userAddress
     * @return
     */
    public Integer insertUserAddress(UserAddress userAddress){
        return userAddressDao.insertUserAddress(userAddress);
    }

    /**
     * 查询用户的默认地址
     * @param userId
     * @return
     */
    public UserAddress isDefaultUserAddress(String userId,Integer defaultState){
        return userAddressDao.isDefaultUserAddress(userId,defaultState);
    }

    /**
     * 根据用户的编号 和地址的编号来修改地址的 默认状态 改为 0或者为1
     * @param userAddress 用户地址类
     * @return
     */
    public Integer updateDefaultAddress(UserAddress userAddress){
        return userAddressDao.updateDefaultAddress(userAddress);
    }

    /**
     * 根据用户的编号、地址编号修改默认状态
     * @param userId 用户编号
     * @param addressId 地址编号
     * @param defaultState 默认信息 1：默认 0：不是
     * @return 1：修改成功 0：修改失败
     */
    public Integer updateDefaultState(String userId,String addressId,Integer defaultState){
        return userAddressDao.updateDefaultState(userId, addressId, defaultState);
    }

    /**
     * 更具用户地址编号 和用户编号 删除用户地址
     * @param userId 用户编号
     * @param addressId 地址编号
     * @return 1：删除成功 0：删除失败
     */
    public Integer deleteUserAddress(String userId,String addressId){
        return userAddressDao.deleteUserAddress(userId, addressId);
    }
}
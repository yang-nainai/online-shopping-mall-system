package com.yang.service.impl;

import com.yang.dao.AddressDao;
import com.yang.entity.Address;
import com.yang.service.AddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 收货人&地址表(Address)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:56
 */
@Service("addressService")
public class AddressServiceImpl implements AddressService {
    @Resource
    private AddressDao addressDao;

    /**
     * 添加一个新的地址
     * @param address 地址对象
     * @return 如果为1添加成功
     */
    public Integer insertAddress(Address address){
        return addressDao.insertAddress(address);
    }

    /**
     * 修改地址信息
     * @param address 地址类
     * @return 1:修改成功 0修改失败
     */
    public Integer updateAddress(Address address){
        return addressDao.updateAddress(address);
    }

    /**
     * 删除地址信息
     * @param address 地址类
     * @return 1：删除成功 0：删除失败
     */
    public Integer deleteAddress(String addressId){
        return addressDao.deleteAddress(addressId);
    }
}
package com.yang.service;

import com.yang.entity.UserRole;

/**
 * 用户角色表
(UserRole)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
public interface UserRoleService {
    /**
     * 给新添加的用户添加一个角色
     * @param userRole 用户角色
     * @return 1：成功 0：失败
     */
    Integer insertUserRole(UserRole userRole);
}
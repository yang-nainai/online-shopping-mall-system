package com.yang.service;

import com.yang.entity.Address;

/**
 * 收货人&地址表(Address)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:56
 */
public interface AddressService {
    /**
     * 添加一个新的地址
     * @param address 地址对象
     * @return 如果为1添加成功
     */
    Integer insertAddress(Address address);

    /**
     * 修改地址信息
     * @param address 地址类
     * @return 1:修改成功 0修改失败
     */
    Integer updateAddress(Address address);

    /**
     * 删除地址信息
     * @param address 地址类
     * @return 1：删除成功 0：删除失败
     */
    Integer deleteAddress(String addressId);
}
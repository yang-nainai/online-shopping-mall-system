package com.yang.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yang.entity.*;
import com.yang.entity.utils.ResponseParamInfo;
import io.swagger.annotations.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author yangdacheng
 * @title: OrderUserAPIService
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/15 9:20 下午
 */
@FeignClient(value = "shopping-order-service")
public interface OrderUserAPIService {

    /**
     * 添加新的订单
     *
     * @param orderState      订单当前状态
     * @param orderBuyNum     购买商品数量
     * @param orderNeedPay    购买商品所需要支付的钱
     * @param orderPay        购买商品实际支付的钱
     * @param orderPayState   商品的支付状态
     * @param merchantBabyId  对应的商家商品编号
     * @param userAddressId   获取用户地址信息
     * @param commodityId     商品编号
     * @param commodityListId 商品sku ist编号
     * @param cartID          购物车编号
     * @return
     */
    @PostMapping("/orderUser/cloud/insertOrder")
    public ResponseParamInfo insertNewOrder(@RequestParam("orderState") String orderState,
                                            @RequestParam("orderBuyNum") Integer orderBuyNum,
                                            @RequestParam("orderNeedPay") Float orderNeedPay,
                                            @RequestParam("orderPay") Float orderPay,
                                            @RequestParam("orderPayState") String orderPayState,
                                            @RequestParam("merchantBabyId") String merchantBabyId,
                                            @RequestParam("userAddressId") String userAddressId,
                                            @RequestParam("commodityId") String commodityId,
                                            @RequestParam("commodityListId") String commodityListId,
                                            @RequestParam("userID") String userID,
                                            @RequestParam("cartID") String cartID,
                                            @RequestParam("merchantID")String merchantID);

    /**
     * 根据用户编号获取用户订单
     *
     * @param userId 用户编号
     * @return
     */
    @PostMapping("/orderUser/cloud/queryAllOrder")
    @ApiOperation(value = "根据用户编号获取用户订单",notes = "根据用户编号获取用户订单")
    @ApiImplicitParam(paramType = "query",name = "用户编号",value = "userID")
    @ApiResponse(code = 200,message = "查询成功")
    public ResponseParamInfo<Object> queryAllOrder(@RequestParam("userID") String userId);

    /**
     * 根据用户编号 获取用户指定状态订单个数
     *
     * @param userID     用户编号
     * @param orderState 订单状态
     * @return
     */
    @PostMapping("/orderUser/cloud/queryOrderState")
    public ResponseParamInfo<Object> queryOrderState(@RequestParam("userID") String userID,
                                             @RequestParam("orderState") String orderState);

    /**
     * 修改用户购买商品数量
     * @param userID 用户编号
     * @param baseKey key名称
     * @param buyNumber 修改数量
     * @return
     */
    @PostMapping("/orderUser/cloud/updateByNumber")
    public ResponseParamInfo<Object> updateByNumber(@RequestParam("userID") String userID,
                                            @RequestParam("baseKey") String baseKey,
                                            @RequestParam("buyNumber") String buyNumber);


    /**
     * 修改订单状态   确定收货
     * @return
     */
    @PostMapping("/orderUser/cloud/updateOrderState")
    public ResponseParamInfo<Object> updateOrderState(@RequestParam("userId") String userId,
                                              @RequestParam("baseOrderId") String baseOrderId,
                                              @RequestParam("orderState") String orderState);

    /**
     * 删除订单
     * @return
     */
    @DeleteMapping("/orderUser/cloud/deleteOrder")
    public ResponseParamInfo<Object> deleteOrder(@RequestParam("baseOrderId") String baseOrderId,
                                         @RequestParam("userOrderId") String userOrderId);
}

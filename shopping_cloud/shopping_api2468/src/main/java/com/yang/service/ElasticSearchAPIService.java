package com.yang.service;

import com.yang.entity.utils.ResponseParamInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author yangdacheng
 * @title: ElasticSearchAPIService
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/16 8:54 上午
 */
@FeignClient(value = "shopping-search-service")
public interface ElasticSearchAPIService {

    /**
     * 添加数据库的数据到ES
     * @return
     */
    @GetMapping(value = "/es/cloud/addAll")
    public Boolean addList();

    /**
     * 获取用户在 redis 缓存中的key --获取用户的历史搜索记录
     *
     * @param userID
     * @return
     */
    @PostMapping("/searchHistory/cloud/queryUserHistory")
    public ResponseParamInfo<Object> queryUserHistory(@RequestParam("userID") String userID);

    /**
     * 向 redis 缓存添加用户搜索记录 返回用户搜索的结果
     *
     * @param userID     用户编号
     * @param searchName 搜索名称
     * @return 放回用户搜索结果 商品类
     */
    @PostMapping("/searchHistory/cloud/insertUserHistory")
    public ResponseParamInfo<Object> insertUserHistory(@RequestParam("userID") String userID,
                                               @RequestParam("searchName") String searchName);

    /**
     * 根据用户编号清空历史记录
     *
     * @param userID
     * @return
     */
    @PostMapping("/searchHistory/cloud/clearHistory")
    public ResponseParamInfo<Object> clearHistory(@RequestParam("userID") String userID);


    @PostMapping(value = "/searchHistory/cloud/searchInElastic")
    public ResponseParamInfo<Object> searchInElastic(@RequestParam("userID") String userID,
                                             @RequestParam("searchName") String searchName) throws IOException;
}

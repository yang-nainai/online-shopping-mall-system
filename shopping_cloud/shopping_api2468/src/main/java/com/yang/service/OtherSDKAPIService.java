package com.yang.service;

import com.alibaba.fastjson.JSON;
import com.yang.entity.*;
import com.yang.entity.utils.ResponseParamInfo;
import io.swagger.annotations.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangdacheng
 * @title: OtherSDKAPIService
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/16 9:27 上午
 */
@FeignClient(value = "shopping-sdk-service")
public interface OtherSDKAPIService {


    /**
     * 多件商品支付 & 商品支付
     * @return
     */
    @PostMapping(value = "/alipay/cloud/morePay")
    public String morePay(@RequestParam("baseOrderId")String baseOrderId,
                          @RequestParam("userOrderId")String  userOrderId,
                          @RequestParam("userId")String userId);

    /**
     * 回调函数
//     * @param response
     * @throws IOException
     */
    @PostMapping(value = "/alipay/cloud/pay_notify")
    public ResponseParamInfo<Object> call(@RequestBody AlibabaPayInfo alibabaPayInfo) throws IOException;

    @PostMapping(value = "/weiXin/cloud/share")
    public WinXinEntity share();
}


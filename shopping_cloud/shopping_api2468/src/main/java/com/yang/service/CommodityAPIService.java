package com.yang.service;

import com.alibaba.fastjson.JSON;
import com.yang.entity.Commodity;
import com.yang.entity.CommodityShow;
import com.yang.entity.Merchant;
import com.yang.entity.MerchantBaby;
import com.yang.entity.utils.HomeAdvertisement;
import com.yang.entity.utils.PageInfo;
import io.swagger.annotations.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdacheng
 * @title: CommodityService
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/15 3:53 下午
 */
@FeignClient(value = "shopping-commodity-service")
public interface CommodityAPIService {
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/commodity/cloud/selectOne")
    public Commodity selectOne(@RequestParam("id") String id);

    /**
     * 获取 商品总数量
     * @return
     */
    @GetMapping("/commodity/cloud/getCommoditySum")
    public Integer getCommoditySum();

    /**
     * 分页获取商品信息
     * @param nowPage
     * @param commoditySum
     * @return
     */
    @PostMapping("/commodity/cloud/limit")
    public List<Commodity> limitCommodity(@RequestParam("nowPage") Integer nowPage,
                                          @RequestParam("commodityTotal") Integer commoditySum);

    //获取首页广告栏的商品消息
    @GetMapping("/commodity/cloud/homeAdvertisement")
    public List<HomeAdvertisement> homeAdvertisement();

    //设置轮播图
    @GetMapping("/commodity/cloud/setHomeAdvertisement")
    public Integer setHomeAdvertisement(@RequestParam("showImg") String showImg,
                                        @RequestParam("commodityID") String commodityID);

    /**
     * 根据用户输入的搜索 获取要查询的信息   --- 模糊查询
     * @param searchName
     * @return
     */
    @GetMapping("/commodity/cloud/searchCommodity")
    public List<Commodity> searchCommodity(@PathParam("searchName") String searchName);

    /**
     * 查询所有的商品
     * @return
     */
    @GetMapping("/commodity/cloud/queryAllCommodity")
    public List<Commodity> queryAllCommodity();

    /**
     * 修改商品信息
     * @param commodity
     * @return
     */
    @PostMapping("/commodity/cloud/updateCommodity")
    public Integer updateCommodity(@RequestParam("commodity") Commodity commodity);

    /**
     * 添加商品信息
     * @param commodity
     * @return
     */
    @PostMapping("/commodity/cloud/insertCommodity")
    public Integer insertCommodity(@RequestParam("commodity") Commodity commodity);
}

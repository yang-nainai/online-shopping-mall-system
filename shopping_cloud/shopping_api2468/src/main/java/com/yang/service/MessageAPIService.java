package com.yang.service;

import com.yang.entity.User;
import com.yang.entity.utils.MessageCache;
import com.yang.entity.utils.MessageMenuInfo;
import io.swagger.annotations.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.io.File;
import java.util.List;

/**
 * @author yangdacheng
 * @title: MessageAPIService
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/15 7:43 下午
 */
@FeignClient(value = "shopping-message-service")
public interface MessageAPIService {

    /**
     * 修改消息是否以读
     *
     * @return
     */
    @PostMapping("/message/cloud/setMessageRead")
    public Integer setMessageRead(@RequestParam("fromUserID") String toUserID,
                                  @RequestParam("toUserID") String fromUserID);

    /**
     * 获取所有的消息内容
     *
     * @return
     */
    @PostMapping("/message/cloud/getAllMessage")
    public List<MessageCache> getAllMessage(@RequestParam("fromUserID") String toUserID,
                                            @RequestParam("toUserID") String fromUserID);

    /**
     * 根据用户编号获取用户聊天对象有哪些
     *
     * @param userID 用户编号
     * @return
     */
    @PostMapping("/message/cloud/getAllMenu")
    public List<MessageMenuInfo> getAllMenu(@RequestParam("userID") String userID);

    /**
     * 发送图片上传图片文件
     * @param multipartFile
     * @return
     */
    @PostMapping(value = "/message/cloud/messageImg",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public String messageFileUpload(@RequestPart("messageImg") MultipartFile multipartFile);

    /**
     * 上传聊天文件
     * @param multipartFile
     * @return
     */
    @PostMapping(value = "/message/cloud/messageTextFile",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public String messageTextFileUpload(@RequestPart("messageFile") MultipartFile multipartFile);


    /**
     * 删除聊天文件
     * @return
     */
    @PostMapping("/message/cloud/deleteMessageFile")
    public Integer deleteMessageFile(@RequestParam("userID") String userID,
                                     @RequestParam("toUserID") String toUserID);
}

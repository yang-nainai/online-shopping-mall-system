package com.yang.service;

import com.yang.entity.*;
import com.yang.entity.utils.ResponseParamInfo;
import io.swagger.annotations.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author yangdacheng
 * @title: UserContextAPIService
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/16 9:29 上午
 */
@FeignClient(value = "shopping-user-service")
public interface UserContextAPIService {


    /**
     * 用户登陆 api
     *
     * @param username 用户名称
     * @param password 用户密码
     * @return 返回信息封装类
     */
    @PostMapping("/user/cloud/userLogin")
    public ResponseParamInfo<Object> userLogin(@RequestParam("username") String username,
                                       @RequestParam("password") String password);

    /**
     * 跟用户发送邮件 获取验证码
     *
     * @param user_email
     * @return
     */
    @PostMapping("/user/cloud/sendEmail")
    public ResponseParamInfo<Object> sendEmail(@RequestParam("user_email") String user_email);

    /**
     * 用户注册
     *
     * @param user_email 用户邮箱
     * @param inputCode  用户输入的验证码
     * @return 返回消息类
     */
    @PostMapping("/user/cloud/userSingIn")
    public ResponseParamInfo<Object> userSingIn(@RequestParam("user_email") String user_email,
                                        @RequestParam("input_code") String inputCode);

    /**
     * 文件上传
     * @param multipartFile 文件上传
     * @return
     */
    @PostMapping(value = "/user/cloud/userImgUpLoad",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseParamInfo<Object> userImgUpLoad(@RequestPart("fileUpload") MultipartFile multipartFile);

    /**
     * 添加一个新的用户
     *
     * @param user 用户对象
     * @return
     */
    @PostMapping("/user/cloud/userInsert")
    public ResponseParamInfo<Object> userInsert(User user);

    /**
     * 根据用户编号获取购物车内容
     * @param userId
     * @return
     */
    @GetMapping("/userCart/cloud/getUserCart")
    public ResponseParamInfo<Object> getUserCart(@RequestParam("userId") String userId);

    /**
     * 获取用户购物车下商品数量
     * @param userId
     * @return
     */
    @GetMapping("/userCart/cloud/getCommodityNumInCart")
    public ResponseParamInfo<Object> getCommodityNumInCart(@RequestParam("userId") String userId);

    /**
     * 修改用户的购物车属性 根据购物车编号修改集合属性
     * @param cartId 购物车编号
     * @param listId 选项编号
     * @param
     * @return
     */
    @PutMapping("/userCart/cloud/updateCommodityListInCart")
    public ResponseParamInfo<Object> updateCommodityListInCart(@RequestParam("cartId") String cartId,
                                                       @RequestParam("listId") String listId,
                                                       @RequestParam("buyNum") Integer buyNum);

    /**
     * 添加一个新的购物车
     * 1、先判断该用户购物车中是否存在该商品 存在就在原有的基础上加1
     * 2、不存在就将该商品添加到购物车中
     * @param userCart 购物车类
     * @return
     */
    @PostMapping("/userCart/cloud/insertCart")
    public ResponseParamInfo<Object> insertCart(@RequestBody UserCart userCart,
                                        @RequestParam("goodsId") String goodsId);

    /**
     * 根据购物车编号删除购物车
     * @param cartId 购物车编号
     * @return
     */
    @DeleteMapping("/userCart/cloud/deleteCart")
    public ResponseParamInfo<Object> deleteCart(@RequestParam("cartId") String cartId);

    /**
     * 根据用户编号获取用户地址信息
     * @param userID 用户编号
     * @return 用户地址集合
     */
    @GetMapping("/userAddress/cloud/getUserAddress")
    public List<UserAddress> getUserAddress(@RequestParam("userId") String userID);

    /**
     * 添加一个新的地址
     * @param address 用户传递的地址信息
     * @param isDefault 是否为默认地址
     * @param userId 用户编号
     * @return ResponseParamInfo
     */
    @PostMapping("/userAddress/cloud/putUserAddress")
    public ResponseParamInfo<Object> putUserAddress(@RequestBody Address address,
                                            @RequestParam("isDefault")Integer isDefault,
                                            @RequestParam("userId")String userId);

    /**
     * 根据用户的编号来修改指定的地址信息
     * @param address
     * @param isDefault
     * @return
     */
    @PutMapping("/userAddress/cloud/updateAddress")
    public ResponseParamInfo<Object> updateAddress(@RequestBody Address address,
                                           @RequestParam("isDefault") Integer isDefault,
                                           @RequestParam("userId") String userId);

    /**
     * 删除用户地址
     * @param userId
     * @param addressId
     * @return
     */
    @PostMapping("/userAddress/cloud/deleteAddress")
    public ResponseParamInfo<Object> deleteAddress(@RequestParam("userId") String userId,
                                           @RequestParam("addressId") String addressId);
}

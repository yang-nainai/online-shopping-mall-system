package com.yang.controller;

import com.yang.entity.Commodity;
import com.yang.entity.utils.HomeAdvertisement;
import com.yang.service.CommodityAPIService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.websocket.server.PathParam;
import java.util.List;

/**
 * 商品表 用来记入一些商品的基本信息(Commodity)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Slf4j
@RestController
@RequestMapping("/commodity")
public class CommodityAPIController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityAPIService commodityAPIService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne")
    public Commodity selectOne(@RequestParam("id") String id){
        return commodityAPIService.selectOne(id);
    }

    /**
     * 获取 商品总数量
     * @return
     */
    @GetMapping("/getCommoditySum")
    public Integer getCommoditySum(){
        return commodityAPIService.getCommoditySum();
    }

    /**
     * 分页获取商品信息
     * @param nowPage
     * @param commoditySum
     * @return
     */
    @PostMapping("/limit")
    public List<Commodity> limitCommodity(@RequestParam("nowPage") Integer nowPage,
                                          @RequestParam("commodityTotal") Integer commoditySum){
        return commodityAPIService.limitCommodity(nowPage, commoditySum);
    }

    //获取首页广告栏的商品消息
    @GetMapping("/homeAdvertisement")
    public List<HomeAdvertisement> homeAdvertisement(){
        return commodityAPIService.homeAdvertisement();
    }

    //设置轮播图
    //
    @GetMapping("/setHomeAdvertisement")
    public Integer setHomeAdvertisement(@RequestParam("showImg") String showImg,
                                        @RequestParam("commodityID") String commodityID){
        return commodityAPIService.getCommoditySum();
    }

    /**
     * 根据用户输入的搜索 获取要查询的信息   --- 模糊查询
     * @param searchName
     * @return
     */
    @GetMapping("/searchCommodity")
    public List<Commodity> searchCommodity(@RequestParam("searchName") String searchName){
        return commodityAPIService.searchCommodity(searchName);
    }

    /**
     * 查询所有的商品
     * @return
     */
    @GetMapping("/queryAllCommodity")
    public List<Commodity> queryAllCommodity(){
        return commodityAPIService.queryAllCommodity();
    }

    /**
     * 修改商品信息
     * @param commodity
     * @return
     */
    @PostMapping("/updateCommodity")
    public Integer updateCommodity(@RequestParam("commodity") Commodity commodity){
        return commodityAPIService.updateCommodity(commodity);
    }

    /**
     * 添加商品信息
     * @param commodity
     * @return
     */
    @PostMapping("/insertCommodity")
    public Integer insertCommodity(@RequestParam("commodity") Commodity commodity){
        return commodityAPIService.insertCommodity(commodity);
    }
}
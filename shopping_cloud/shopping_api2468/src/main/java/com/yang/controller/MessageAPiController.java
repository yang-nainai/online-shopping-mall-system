package com.yang.controller;

import com.yang.entity.User;
import com.yang.entity.utils.MessageCache;
import com.yang.entity.utils.MessageMenuInfo;
import com.yang.service.MessageAPIService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.websocket.server.PathParam;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author yangdacheng
 * @title: MessageAPiController
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/15 7:48 下午
 */
@RestController
@RequestMapping("/message")
public class MessageAPiController {

    @Autowired
    private MessageAPIService messageAPIService;

    /**
     * 修改消息是否以读
     *
     * @return
     */
    @PostMapping("/setMessageRead")
    public Integer setMessageRead(@RequestParam("fromUserID") String toUserID,
                                  @RequestParam("toUserID") String fromUserID) {
        return messageAPIService.setMessageRead(toUserID, fromUserID);
    }

    /**
     * 获取所有的消息内容
     *
     * @return
     */
    @PostMapping("/getAllMessage")
    public List<MessageCache> getAllMessage(@RequestParam("fromUserID") String toUserID,
                                            @RequestParam("toUserID") String fromUserID) {
        return messageAPIService.getAllMessage(toUserID, fromUserID);
    }

    /**
     * 根据用户编号获取用户聊天对象有哪些
     *
     * @param userID 用户编号
     * @return
     */
    @PostMapping("/getAllMenu")
    public List<MessageMenuInfo> getAllMenu(@RequestParam("userID") String userID) {
        return messageAPIService.getAllMenu(userID);
    }

    /**
     * 发送图片上传图片文件
     * @param multipartFile
     * @return
     */
    @PostMapping("/messageImg")
    public String messageFileUpload(@RequestPart("messageImg") MultipartFile multipartFile){
        return messageAPIService.messageFileUpload(multipartFile);
    }

    /**
     * 上传聊天文件
     * @param multipartFile
     * @return
     */
    @PostMapping("/messageTextFile")
    public String messageTextFileUpload(@RequestPart("messageFile") MultipartFile multipartFile){
        return  messageAPIService.messageTextFileUpload(multipartFile);
    }


    /**
     * 删除聊天文件
     * @return
     */
    @PostMapping("/deleteMessageFile")
    public Integer deleteMessageFile(@RequestParam("userID") String userID,
                                     @RequestParam("toUserID") String toUserID){
        return messageAPIService.deleteMessageFile(userID, toUserID);
    }
}

package com.yang.controller;

import com.yang.entity.AlibabaPayInfo;
import com.yang.entity.WinXinEntity;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.OtherSDKAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @author yangdacheng
 * @title: OtherSDKAPIController
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/16 9:26 上午
 */
@RestController
@RequestMapping(value = {"/alipay","/weiXin"})
public class OtherSDKAPIController {

    @Autowired
    private OtherSDKAPIService otherSDKAPIService;

    /**
     * 多件商品支付 & 商品支付
     * @return
     */
    @PostMapping("/morePay")
    public String morePay(String baseOrderId,String  userOrderId,String userId){
        return otherSDKAPIService.morePay(baseOrderId, userOrderId, userId);
    }

    /**
     * 回调函数
     * @throws IOException
     */
    @PostMapping(value = "/pay_notify")
    public ResponseParamInfo<Object> call(AlibabaPayInfo alibabaPayInfo) throws IOException{
        System.out.println(alibabaPayInfo.toString());
        return otherSDKAPIService.call(alibabaPayInfo);
    }

    @PostMapping(value = "/share")
    public WinXinEntity share(){
        return otherSDKAPIService.share();
    }
}

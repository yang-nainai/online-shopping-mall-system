package com.yang.controller;

import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.OrderUserAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author yangdacheng
 * @title: OrderUserAPIController
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/15 9:27 下午
 */
@RestController
@RequestMapping("/orderUser")
public class OrderUserAPIController {

    @Autowired
    private OrderUserAPIService orderUserAPIService;

    /**
     * 添加新的订单
     *
     * @param orderState      订单当前状态
     * @param orderBuyNum     购买商品数量
     * @param orderNeedPay    购买商品所需要支付的钱
     * @param orderPay        购买商品实际支付的钱
     * @param orderPayState   商品的支付状态
     * @param merchantBabyId  对应的商家商品编号
     * @param userAddressId   获取用户地址信息
     * @param commodityId     商品编号
     * @param commodityListId 商品sku ist编号
     * @param cartID          购物车编号
     * @return
     */
    @PostMapping("/insertOrder")
    public ResponseParamInfo<Object> insertNewOrder(@RequestParam("orderState") String orderState,
                                            @RequestParam("orderBuyNum") Integer orderBuyNum,
                                            @RequestParam("orderNeedPay") Float orderNeedPay,
                                            @RequestParam("orderPay") Float orderPay,
                                            @RequestParam("orderPayState") String orderPayState,
                                            @RequestParam("merchantBabyId") String merchantBabyId,
                                            @RequestParam("userAddressId") String userAddressId,
                                            @RequestParam("commodityId") String commodityId,
                                            @RequestParam("commodityListId") String commodityListId,
                                            @RequestParam("userID") String userID,
                                            @RequestParam("cartID") String cartID,
                                            @RequestParam("merchantID")String merchantID){
        return orderUserAPIService.insertNewOrder(orderState, orderBuyNum, orderNeedPay, orderPay,
                orderPayState, merchantBabyId, userAddressId,
                commodityId, commodityListId, userID, cartID, merchantID);
    }

    /**
     * 根据用户编号获取用户订单
     *
     * @param userId 用户编号
     * @return
     */
    @PostMapping("/queryAllOrder")
    public ResponseParamInfo<Object> queryAllOrder(@RequestParam("userID") String userId){
        return orderUserAPIService.queryAllOrder(userId);
    }

    /**
     * 根据用户编号 获取用户指定状态订单个数
     *
     * @param userID     用户编号
     * @param orderState 订单状态
     * @return
     */
    @PostMapping("/queryOrderState")
    public ResponseParamInfo<Object> queryOrderState(@RequestParam("userID") String userID,
                                             @RequestParam("orderState") String orderState){
        return orderUserAPIService.queryOrderState(userID, orderState);
    }

    /**
     * 修改用户购买商品数量
     * @param userID 用户编号
     * @param baseKey key名称
     * @param buyNumber 修改数量
     * @return
     */
    @PostMapping("/updateByNumber")
    public ResponseParamInfo<Object> updateByNumber(@RequestParam("userID") String userID,
                                            @RequestParam("baseKey") String baseKey,
                                            @RequestParam("buyNumber") String buyNumber){
        return orderUserAPIService.updateByNumber(userID, baseKey, buyNumber);
    }


    /**
     * 修改订单状态   确定收货
     * @return
     */
    @PostMapping("/updateOrderState")
    public ResponseParamInfo<Object> updateOrderState(@RequestParam("userId") String userId,
                                              @RequestParam("baseOrderId") String baseOrderId,
                                              @RequestParam("orderState") String orderState){
        return orderUserAPIService.updateOrderState(userId,baseOrderId,orderState);
    }

    /**
     * 删除订单
     * @return
     */
    @DeleteMapping("/deleteOrder")
    public ResponseParamInfo<Object> deleteOrder(@RequestParam("baseOrderId") String baseOrderId,
                                         @RequestParam("userOrderId") String userOrderId){
        return orderUserAPIService.deleteOrder(baseOrderId, userOrderId);
    }
}

package com.yang.controller;

import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.ElasticSearchAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author yangdacheng
 * @title: ElasticSearchAPIController
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/16 9:00 上午
 */
@RestController
@RequestMapping(value = {"/es","/searchHistory"})
public class ElasticSearchAPIController {

    @Autowired
    private ElasticSearchAPIService elasticSearchAPIService;

    /**
     * 添加数据库的数据到ES
     * @return
     */
    @GetMapping(value = "/addAll")
    public Boolean addList(){
        return elasticSearchAPIService.addList();
    }

    /**
     * 获取用户在 redis 缓存中的key --获取用户的历史搜索记录
     *
     * @param userID
     * @return
     */
    @PostMapping("/queryUserHistory")
    public ResponseParamInfo<Object> queryUserHistory(@RequestParam("userID") String userID){
        return elasticSearchAPIService.queryUserHistory(userID);
    }

    /**
     * 向 redis 缓存添加用户搜索记录 返回用户搜索的结果
     *
     * @param userID     用户编号
     * @param searchName 搜索名称
     * @return 放回用户搜索结果 商品类
     */
    @PostMapping("/insertUserHistory")
    public ResponseParamInfo<Object> insertUserHistory(@RequestParam("userID") String userID,
                                               @RequestParam("searchName") String searchName){
        return elasticSearchAPIService.insertUserHistory(userID, searchName);
    }

    /**
     * 根据用户编号清空历史记录
     *
     * @param userID
     * @return
     */
    @PostMapping("/clearHistory")
    public ResponseParamInfo<Object> clearHistory(@RequestParam("userID") String userID){
        return elasticSearchAPIService.clearHistory(userID);
    }


    @PostMapping(value = "/searchInElastic")
    public ResponseParamInfo<Object> searchInElastic(@RequestParam("userID") String userID,
                                             @RequestParam("searchName") String searchName) throws IOException{
        return elasticSearchAPIService.searchInElastic(userID, searchName);
    }
}

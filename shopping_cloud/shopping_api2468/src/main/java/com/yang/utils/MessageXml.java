package com.yang.utils;

import com.yang.entity.utils.MessageCache;
import com.yang.entity.utils.MessageInfo;
import com.yang.entity.utils.MessageMenuInfo;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.stereotype.Component;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author yangdacheng
 * @title: MessageXml 用来生成消息缓存的xml文件
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/25 2:44 下午
 */
@Slf4j
@Component
public class MessageXml {

    private static FileWriter fileWriter = null; //文件写入流
    private static BufferedWriter bufferedWriter = null; //文件写入流
    /**
     * 创建消息缓存文件
     * @param fromUserID 发送消息的一放
     * @param toUserID 接收消息的一方
     * @return 放回文件路径
     */
    public static String createFile(String fromUserID,String toUserID,String filePath) throws IOException {
        String fileName = filePath+fromUserID+"TO"+toUserID+".xml";
        File file = new File(fileName);
        if (!file.exists()){
            log.info("===消息文件不存在生成文件中...===");
            boolean createResult = file.createNewFile();
            if (createResult){
                log.info("===文件生成成功===");
                writeBaseMessage(file);  //生成xml文件的基本内容
            }else{
                log.info("文件生成失败");
                return "PIG_ERROR";
            }
        }else{
            writeBaseMessage(file);
        }
        return file.getPath();
    }


    /**
     * 生成xml文件的基本内容
     * @param file
     */
    public static void writeBaseMessage(File file){
        //xml文件的基本内容
        String writeStr = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
                "<messageCaches>\n" +
                "</messageCaches>";
        try {
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(writeStr);
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (fileWriter!=null){
                    fileWriter.close();
                }
                if (bufferedWriter!=null){
                    bufferedWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 将消息内容写入文件
     * @param file 文件
     * @param messageCaches 消息内容
     */
    public static void writeMessage(File file, List<MessageCache> messageCaches){
        SAXReader reader = new SAXReader();
        XMLWriter xmlWriter = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Document read = reader.read(file);  //读取文件
            Element rootElement = read.getRootElement(); //获取根结点
            for (MessageCache messageCache: messageCaches){
                //在根结点下面添加一个新的节点
                Element messageCacheElement = rootElement.addElement("messageCache");  //添加一个根结点
                Element fromUserID = messageCacheElement.addElement("fromUserID"); //添加一个子节点 用户编号
                fromUserID.setText(messageCache.getFromUserID());
                Element toUserID = messageCacheElement.addElement("toUserID"); //添加一个子节点 用户编号
                toUserID.setText(messageCache.getToUserID());
                Element sendMessage = messageCacheElement.addElement("sendMessage"); //添加一个子节点 发送的消息
                sendMessage.setText(messageCache.getSendMessage());
                Element sendMessageTime = messageCacheElement.addElement("sendMessageTime");//添加一个子节点 发送的时间
                sendMessageTime.setText(simpleDateFormat.format(messageCache.getSendMessageTime()));
                Element messageAreRead = messageCacheElement.addElement("messageAreRead"); //添加一个子节点 消息是否已读
                messageAreRead.setText(messageCache.getMessageAreRead().toString());
                Element messageType = messageCacheElement.addElement("messageType"); //添加一个子节点 消息类型
                messageType.setText(messageCache.getMessageType());
                Element fileName = messageCacheElement.addElement("fileName"); //添加一个子节点 文件名称
                fileName.setText(messageCache.getFileName());
            }
            //创建一种输出格式    每个节点元素可自动换行
            OutputFormat outputFormat=OutputFormat.createPrettyPrint();
            outputFormat.setEncoding("UTF-8");
            xmlWriter =new XMLWriter(new FileWriter(file),outputFormat);//写入XML文件的位置 以及指定的格式
            xmlWriter.write(read);//开始写入XML文件   写入Document对象
            System.out.println("写入成功！");
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                xmlWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 读取文件中消息内容
     * @param file 文件内容
     * @return 消息缓存类
     */
    public static List<MessageCache> readMessage(File file){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<MessageCache> messageCaches = new ArrayList<>();
        try {
            SAXReader saxReader = new SAXReader();
            if (!file.exists()){
                return null;
            }
            Document document = saxReader.read(file);
            //获取根目录
            Element rootElement = document.getRootElement();
            Iterator<Element> messageCacheIterator = rootElement.elementIterator("messageCache");
            while (messageCacheIterator.hasNext()){
                Element element = messageCacheIterator.next();
                String fromUserID = element.elementText("fromUserID");
                String sendMessage = element.elementText("sendMessage");
                String toUserID = element.elementText("toUserID");
                String messageAreRead = element.elementText("messageAreRead");
                String sendMessageTime = element.elementText("sendMessageTime");
                String messageType = element.elementText("messageType");
                String fileName = element.elementText("fileName");
                MessageCache messageCache = new MessageCache(fromUserID,toUserID,sendMessage
                        ,simpleDateFormat.parse(sendMessageTime),Boolean.parseBoolean(messageAreRead),messageType,fileName);
                messageCaches.add(messageCache);
            }
        } catch (DocumentException | ParseException e) {
            e.printStackTrace();
        }
        return messageCaches;
    }

    /**
     * 修改消息状态
     * @param file 文件类
     * @return
     */
    public static Integer updateMessage(File file){
        Integer updateResult = 0;
        try {
            SAXReader saxReader = new SAXReader();
            if (!file.exists()){
                return 0;
            }
            Document document = saxReader.read(file);
            //获取根目录
            Element rootElement = document.getRootElement();
            Iterator<Element> messageCacheIterator = rootElement.elementIterator("messageCache");
            while (messageCacheIterator.hasNext()){
                Element element = messageCacheIterator.next();
                element.element("messageAreRead").setText("true");  //设置消息已读
            }
            //保存文件
            FileOutputStream out = new FileOutputStream(file);
            OutputFormat format = OutputFormat.createPrettyPrint();
            format.setEncoding("utf-8");
            XMLWriter writer = new XMLWriter(out,format);
            writer.write(document);
            writer.close();
            updateResult = 1;
        } catch (DocumentException | FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return updateResult;
    }

    /**
     * 获取用户菜单
     * @param basePath 文件基础路径
     * @param userID 用户编号
     * @return
     */
    public static List<MessageMenuInfo> getMessageMenu(String basePath,String userID){
        File file = new File(basePath);
        List<MessageMenuInfo> messageMenuInfos = new ArrayList<>(); //用户聊天对象集合
        if (file.isDirectory()){
            File[] files = file.listFiles();  //获取目录下的所有文件
            for (File childFile:files){
                if (childFile.exists() && childFile.getName().startsWith(userID)){
                    List<MessageCache> messageCaches = readMessage(childFile);  //获取文件所有的消息
                    MessageMenuInfo messageMenuInfo = getMessageMenuInfo(messageCaches); //获取消息菜单对象
                    messageMenuInfo.setToUserId(childFile.getName().split("TO")[1].split(".xml")[0]);  //设置接受者名称
                    messageMenuInfos.add(messageMenuInfo);  //添加到集合中
                }
            }
        }
        return messageMenuInfos;
    }

    /**
     * 获取一个用户菜单对象
     * @param messageCaches
     * @return
     */
    public static MessageMenuInfo getMessageMenuInfo(List<MessageCache> messageCaches){
        MessageMenuInfo messageMenuInfo = new MessageMenuInfo();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Integer messageNoReader = 0;  //消息未读数量
        for (MessageCache messageCache: messageCaches){
            messageMenuInfo.setMessageLastContext(messageCache.getSendMessage());  //设置最后一条消息内容
            messageMenuInfo.setMessageTime(simpleDateFormat.format(messageCache.getSendMessageTime())); //设置最后发送时间
            if (!messageCache.getMessageAreRead()){
                messageNoReader++; //未读数量+1
            }
        }
        messageMenuInfo.setMessageBadge(messageNoReader);
        return messageMenuInfo;
    }

    /**
     * 生成两个聊天文件 第一个 发送者聊天文件 第二个 消息接收者聊天文件
     * @param fileBasePath 文件基础路径
     * @param messageInfo 消息对象
     */
    public static void writeTwoMessage(String fileBasePath, MessageInfo messageInfo){
        log.info("================生成聊天文件==================");
        log.info("文件路径:"+fileBasePath);
        String filePathFromUser = fileBasePath+messageInfo.getFromUserID()+"TO"+messageInfo.getToUserID()+".xml"; //发送者文件
        String filePathToUser = fileBasePath+messageInfo.getToUserID()+"TO"+messageInfo.getFromUserID()+".xml";//接收者文件
        File fileFromUser = new File(filePathFromUser);
        File fileToUser = new File(filePathToUser);
        //判断文件夹是否存在 不存在穿件一个
        File folder = fileFromUser.getParentFile();
        if (!folder.exists()){
            folder.mkdirs();
        }
        try {
            if (!fileFromUser.exists()){
                fileFromUser.createNewFile();
                MessageXml.writeBaseMessage(fileFromUser);
            }
            if (!fileToUser.exists()){
                fileToUser.createNewFile();
                MessageXml.writeBaseMessage(fileToUser);
            }
            //写入消息 发送者消息
            List<MessageCache> messageCachesFrom = new ArrayList<>();
            MessageCache messageCacheFromUser = new MessageCache(messageInfo.getFromUserID(),messageInfo.getToUserID(),
                    messageInfo.getSendMessage(),new Date(),true, messageInfo.getMessageType(),
                    messageInfo.getFileName());
            messageCachesFrom.add(messageCacheFromUser);

            List<MessageCache> messageCachesTo = new ArrayList<>();
            MessageCache messageCacheToUser = new MessageCache(messageInfo.getFromUserID(),messageInfo.getToUserID(),
                    messageInfo.getSendMessage(),new Date(),false, messageInfo.getMessageType(),messageInfo.getFileName());
            messageCachesTo.add(messageCacheToUser);
            MessageXml.writeMessage(fileFromUser,messageCachesFrom);
            //写入消息接收者消息
            MessageXml.writeMessage(fileToUser,messageCachesTo);
            log.info("================聊天文件生成==================");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除 谁 对 谁的聊天记录
     * @param fileBasePath 文件基本路径
     * @param userID 用户编号
     * @param toUserID 接收者编号
     */
    public static Integer deleteMessageFile(String fileBasePath,String userID,String toUserID){
        String filePath = fileBasePath+userID+"TO"+toUserID+".xml";
        File file = new File(filePath);
        boolean deleteResult = false;
        if (file.exists()){
            deleteResult = file.delete();
        }
        return deleteResult?1:0;
    }
}

package com.yang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author yangdacheng
 * @title: ShoppingApi6035 阳奈
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/15 3:51 下午
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableFeignClients
public class ShoppingApi2468 {
    public static void main(String[] args) {
        SpringApplication.run(ShoppingApi2468.class,args);
    }
}

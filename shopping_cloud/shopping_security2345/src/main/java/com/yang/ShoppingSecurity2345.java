package com.yang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author yangdacheng
 * @title: ShoppingSecurity2345
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/26 9:21 下午
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class ShoppingSecurity2345 {
    public static void main(String[] args) {
        SpringApplication.run(ShoppingSecurity2345.class,args);
    }
}

package com.yang.dao;

import com.yang.entity.User;
import org.apache.ibatis.annotations.*;

/**
 * 用户表
 * (User)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface UserDao {

    /**
     * 根据用户编号获取用户信息
     * @param userId 用户编号
     * @return
     */
    @Select("SELECT * FROM user WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "userEmail", column = "user_email"),
            @Result(property = "userGender", column = "user_gender"),
            @Result(property = "userPhotoImg", column = "user_photo_img"),
            @Result(property = "littlePigUsername", column = "user_name"),
            @Result(property = "littlePigPassword", column = "user_password"),
            @Result(property = "userInternetName", column = "user_internet_name"),
            @Result(property = "userRole",column = "user_id",one = @One(
                    select = "com.yang.dao.UserRoleDao.queryByUserId"
            ))
    },id = "userMap")
    User queryById(String userId);


    /**
     * 根据用户账号 查询用户
     * @param username
     * @return
     */
    @Select("SELECT * FROM user WHERE user_name = #{username}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "littlePigUsername",column = "user_name"),
            @Result(property = "littlePigPassword",column = "user_password"),
            @Result(property = "userEmail", column = "user_email"),
            @Result(property = "userGender", column = "user_gender"),
            @Result(property = "userPhotoImg", column = "user_photo_img"),
            @Result(property = "userInternetName", column = "user_internet_name"),
            @Result(property = "userRole",column = "user_id",one = @One(
                    select = "com.yang.dao.UserRoleDao.queryByUserId"
            ))
    })
    User queryByUsername(String username);

}
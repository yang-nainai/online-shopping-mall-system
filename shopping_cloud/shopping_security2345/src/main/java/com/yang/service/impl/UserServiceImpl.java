package com.yang.service.impl;

import com.yang.dao.UserDao;
import com.yang.entity.User;
import com.yang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author yangdacheng
 * @title: UserServiceImpl
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/26 9:33 下午
 */
@Service
public class UserServiceImpl implements UserDetailsService,UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userInfo = userDao.queryByUsername(username);
        if (Objects.isNull(userInfo)){
            throw new UsernameNotFoundException("用户不存在！！！");
        }
        return userInfo;
    }

    @Override
    public User queryById(String userId) {
        return userDao.queryById(userId);
    }

    @Override
    public User queryByUsername(String username) {
        return userDao.queryByUsername(username);
    }
}

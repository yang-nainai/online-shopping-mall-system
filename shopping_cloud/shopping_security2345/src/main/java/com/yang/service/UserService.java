package com.yang.service;

import com.yang.entity.User;
import com.yang.entity.utils.ResponseParamInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author yangdacheng
 * @title: UserService
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/26 9:34 下午
 */

public interface UserService {

    /**
     * 根据用户编号获取用户信息
     * @param userId 用户编号
     * @return
     */
    User queryById(String userId);


    /**
     * 根据用户账号 查询用户
     * @param username
     * @return
     */
    User queryByUsername(String username);
}

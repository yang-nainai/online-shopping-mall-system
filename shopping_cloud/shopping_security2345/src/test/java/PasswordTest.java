import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author yangdacheng
 * @title: PasswordTest
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/26 11:19 下午
 */

public class PasswordTest {
    public static void main(String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");
        System.out.println(encode);
        System.out.println(bCryptPasswordEncoder.matches("123456","$2a$10$5QCIdUp0YrdBW0MvuYtFw.03X0gyk8CNLjXpRZoeadDR8zPGwXRPW"));
    }
}

package com.yang.service.impl;

import com.yang.dao.MerchantDao;
import com.yang.entity.Merchant;
import com.yang.service.MerchantService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商家表
(Merchant)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("merchantService")
public class MerchantServiceImpl implements MerchantService {
    @Resource
    private MerchantDao merchantDao;

    /**
     * 根据商家编号获取商家信息
     * @param merchantId 商家编号
     * @return 商家信息
     */
    public Merchant queryMerchantById(String merchantId){
        return merchantDao.queryMerchantById(merchantId);
    }

    @Override
    public Merchant queryMerchantByUserID(String userID) {
        return merchantDao.queryMerchantByUserID(userID);
    }

    @Override
    public Integer updateMerchant(Merchant merchant) {
        return merchantDao.updateMerchant(merchant);
    }
}
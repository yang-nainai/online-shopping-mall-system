package com.yang.dao.controller;

import com.yang.entity.Merchant;
import com.yang.entity.MerchantBaby;
import com.yang.service.MerchantBabyService;
import com.yang.service.MerchantService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author yangdacheng
 * @title: MerchantCloudController
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/17 7:09 下午
 */
@RestController
@RequestMapping("/merchant/cloud/dao")
public class MerchantCloudController {

    @Resource
    private MerchantBabyService merchantBabyService;
    @Resource
    private MerchantService merchantService;


    /**
     * 根据商品编号查询 商家信息
     * @param commodityId
     * @return
     */
    @PostMapping("/queryByCommodityId")
    public MerchantBaby queryByCommodityId(@RequestParam("commodityId") String commodityId){
        return merchantBabyService.queryByCommodityId(commodityId);
    }

    /**
     * 根据商家宝贝编号获取宝贝信息
     * @param merchantBabyId 商品宝贝编号
     * @return
     */
    @PostMapping("/queryByMerchantBabyId")
    public MerchantBaby queryByMerchantBabyId(@RequestParam("merchantBabyId") String merchantBabyId){
        return merchantBabyService.queryByMerchantBabyId(merchantBabyId);
    }

    /**
     * 添加商品 归宿
     * @param merchantBaby
     * @return
     */
    @PostMapping("/insertBaby")
    public Integer insertBaby(@RequestBody MerchantBaby merchantBaby){
        return merchantBabyService.insertBaby(merchantBaby);
    }

    /**
     * 根据商家编号获取商家所有商品谢谢
     * @param merchant_id 商家编号
     * @return
     */
    @PostMapping("/queryMerchantBaby")
    public List<MerchantBaby> queryMerchantBaby(@RequestParam("merchant_id") String merchant_id){
        return merchantBabyService.queryMerchantBaby(merchant_id);
    }

    /**
     * 根据商家编号获取商家的商品数量
     * @param merchantID
     * @return
     */
    @PostMapping("/queryMerchantBabyNumber")
    public Integer queryMerchantBabyNumber(@RequestParam("merchantID") String merchantID){
        return merchantBabyService.queryMerchantBabyNumber(merchantID);
    }

    /**
     * 修改商品宝贝
     * @param merchantBaby
     * @return
     */
    @PostMapping("/updateMerchantBaby")
    public Integer updateMerchantBaby(@RequestBody MerchantBaby merchantBaby){
        return merchantBabyService.updateMerchantBaby(merchantBaby);
    }

    /**
     * 添加商家商品
     * @param merchantBaby
     * @return
     */
    @PostMapping("/insertMerchantBaBy")
    public Integer insertMerchantBaBy(@RequestBody MerchantBaby merchantBaby){
        return merchantBabyService.insertMerchantBaBy(merchantBaby);
    }

    /**
     * 根据商家编号获取商家信息
     * @param merchantId 商家编号
     * @return 商家信息
     */
    @PostMapping("/queryMerchantById")
    public Merchant queryMerchantById(@RequestParam("merchantId") String merchantId){
        return merchantService.queryMerchantById(merchantId);
    }

    /**
     * 根据用户编号获取商家信息
     * @param userID
     * @return
     */
    @PostMapping("/queryMerchantByUserID")
    public Merchant queryMerchantByUserID(@RequestParam("userID") String userID){
        return merchantService.queryMerchantByUserID(userID);
    }

    /**
     * 根据商家编号修改商家信息
     * @param merchant
     * @return
     */
    @PostMapping("/updateMerchant")
    public Integer updateMerchant(@RequestBody Merchant merchant){
        return merchantService.updateMerchant(merchant);
    }
}

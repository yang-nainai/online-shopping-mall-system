package com.yang.service.impl;

import com.yang.dao.MerchantBabyDao;
import com.yang.entity.MerchantBaby;
import com.yang.service.MerchantBabyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商家 商品表(MerchantBaby)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("merchantBabyService")
public class MerchantBabyServiceImpl implements MerchantBabyService {
    @Resource
    private MerchantBabyDao merchantBabyDao;

    /**
     * 根据商品编号查询 商家信息
     * @param commodityId
     * @return
     */
    public MerchantBaby queryByCommodityId(String commodityId){
        return merchantBabyDao.queryByCommodityId(commodityId);
    }

    /**
     * 根据商家宝贝编号获取宝贝信息
     * @param merchantBabyId 商品宝贝编号
     * @return
     */
    public MerchantBaby queryByMerchantBabyId(String merchantBabyId){
        return merchantBabyDao.queryByMerchantBabyId(merchantBabyId);
    }

    /**
     * 添加商品 归宿
     * @param merchantBaby
     * @return
     */
   public Integer insertBaby(MerchantBaby merchantBaby){
       return merchantBabyDao.insertBaby(merchantBaby);
   }

    /**
     * 根据商家编号获取商家所有商品信息
     * @param merchant_id 商家编号
     * @return
     */
    public List<MerchantBaby> queryMerchantBaby(String merchant_id){
        return merchantBabyDao.queryMerchantBaby(merchant_id);
    }

    @Override
    public Integer queryMerchantBabyNumber(String merchantID) {
        return merchantBabyDao.queryMerchantBabyNumber(merchantID);
    }

    @Override
    public Integer updateMerchantBaby(MerchantBaby merchantBaby) {
        return merchantBabyDao.updateMerchantBaby(merchantBaby);
    }

    @Override
    public Integer insertMerchantBaBy(MerchantBaby merchantBaby) {
        return merchantBabyDao.insertMerchantBaBy(merchantBaby);
    }

}
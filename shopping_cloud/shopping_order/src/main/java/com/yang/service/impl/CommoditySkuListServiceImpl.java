package com.yang.service.impl;

import com.yang.dao.CommoditySkuListDao;
import com.yang.entity.CommoditySkuList;
import com.yang.service.CommoditySkuListService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品sku的集合
(CommoditySkuList)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commoditySkuListService")
public class CommoditySkuListServiceImpl implements CommoditySkuListService {
    @Resource
    private CommoditySkuListDao commoditySkuListDao;

    /**
     * 根据商品sku的编号获取商品的信息
     * @param skuListId 商品sku编号
     * @return 商品信息
     */
    public CommoditySkuList queryBySkuListId(String skuListId){
        return commoditySkuListDao.queryBySkuListId(skuListId);
    }

    @Override
    public Integer insertSkuList(List<CommoditySkuList> commoditySkuListList) {
        return commoditySkuListDao.insertSkuList(commoditySkuListList);
    }
}
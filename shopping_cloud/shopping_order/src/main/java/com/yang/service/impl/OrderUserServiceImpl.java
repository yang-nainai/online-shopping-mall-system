package com.yang.service.impl;

import com.yang.dao.OrderUserDao;
import com.yang.entity.OrderUser;
import com.yang.service.OrderUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户订单表(OrderUser)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Service("orderUserService")
public class OrderUserServiceImpl implements OrderUserService {
    @Resource
    private OrderUserDao orderUserDao;

    /**
     * 根据用户编号获取订单
     * @param userID 用户编号
     * @return 订单集合
     */
    public List<OrderUser> queryOrderByUserId(String userID){
        return orderUserDao.queryOrderByUserId(userID);
    }


    /**
     * 添加一个新的用户订单
     * @param orderUser 用户订单类
     * @return 1：成功 0：失败
     */
    public Integer insertOrderUser(OrderUser orderUser){
        return orderUserDao.insertOrderUser(orderUser);
    }

    @Override
    public List<OrderUser> queryOrderByMerchantID(String merchantID) {
        return orderUserDao.queryOrderByMerchantID(merchantID);
    }

}
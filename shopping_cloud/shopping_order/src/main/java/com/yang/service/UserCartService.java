package com.yang.service;

import com.yang.entity.UserCart;

import java.util.List;

/**
 * 用户购物车表
(UserCart)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
public interface UserCartService {

    /**
     * 根据用户编号获取用户购物车信息
     * @param userId 用户编号
     * @return 一些购物车信息
     */
    List<UserCart> findCartByUserId(String userId);

    /**
     * 根据用户编号 获取用户名下购物车内商品数量
     * @param userId 用户编号
     * @return
     */
    Integer findAllNumber(String userId);

    /**
     * 根据商品编号查询购物车 判断是否存在
     * @param merchantBabyId 宝贝编号
     * @return
     */
    UserCart queryByMerchantBabyId(String merchantBabyId,String userId);

    /**
     * 修改购物车内中 商品的属性
     * @param cartId 购物车编号
     * @param listId 商品 skulist 集合编号
     * @return 1：成功 0：失败
     */
    Integer updateCart(String cartId,String listId,Integer buyNum);

    /**
     * 将商品添加到用户购物车
     * @param userCart
     * @return
     */
    Integer insertCart(UserCart userCart);

    /**
     * 根据购物车编号 删除购物车
     * @param cartId 购物车编号
     * @return
     */
    Integer deleteByCartId(String cartId);
}
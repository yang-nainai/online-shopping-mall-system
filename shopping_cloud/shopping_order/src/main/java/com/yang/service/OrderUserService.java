package com.yang.service;

import com.yang.entity.OrderUser;

import java.util.List;

/**
 * 用户订单表(OrderUser)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
public interface OrderUserService {

    /**
     * 根据用户编号获取订单
     * @param userID 用户编号
     * @return 订单集合
     */
    List<OrderUser> queryOrderByUserId(String userID);

    /**
     * 添加一个新的用户订单
     * @param orderUser 用户订单类
     * @return 1：成功 0：失败
     */
    Integer insertOrderUser(OrderUser orderUser);

    /**
     * 根据商家编号获取订单
     * @param merchantID
     * @return
     */
    List<OrderUser> queryOrderByMerchantID(String merchantID);

}
package com.yang.dao;

import com.yang.entity.OrderUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 用户订单表(OrderUser)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface OrderUserDao {

    /**
     * 根据用户编号获取订单
     * @param userID 用户编号
     * @return 订单集合
     */
    @Select("SELECT * FROM order_user WHERE user_id = #{userID}")
    @Results(value = {
            @Result(property = "userOrderId",column = "user_order_id",id = true),
            @Result(property = "merchantBabyId",column = "merchant_baby_id"),
            @Result(property = "userAddressId",column = "user_address_id"),
            @Result(property = "orderId",column = "order_id"),
            @Result(property = "commodityId",column = "commodity_id"),
            @Result(property = "commodityListId",column = "commodity_list_id"),
            @Result(property = "userId",column = "user_id"),
            @Result(property = "orderBase",column = "order_id",one = @One(
                    select = "com.yang.dao.OrderBaseDao.queryById"
            )),
            @Result(property = "merchantBaby",column = "merchant_baby_id",one = @One(
                    select = "com.yang.dao.MerchantBabyDao.queryByMerchantBabyId"
            )),
            @Result(property = "userAddress",column = "user_address_id",one = @One(
                    select = "com.yang.dao.UserAddressDao.queryByUserAddressId"
            )),
            @Result(property = "commoditySkuList",column = "commodity_list_id",one = @One(
                    select = "com.yang.dao.CommoditySkuListDao.queryBySkuListId"
            )),
    },id = "orderUserMap")
    List<OrderUser> queryOrderByUserId(String userID);


    /**
     * 添加一个新的用户订单
     * @param orderUser 用户订单类
     * @return 1：成功 0：失败
     */
    @Insert("INSERT INTO " +
            "order_user(user_order_id,merchant_baby_id,user_address_id,order_id,commodity_id,commodity_list_id,user_id,merchant_id) " +
            "VALUES(#{userOrderId},#{merchantBabyId},#{userAddressId},#{orderId},#{commodityId}," +
            "#{commodityListId},#{userId},#{merchantID})")
    Integer insertOrderUser(OrderUser orderUser);


    /**
     * 根据商家编号获取订单
     * @param merchantID
     * @return
     */
    @Select("SELECT * FROM order_user WHERE merchant_id = #{merchantID}")
    @ResultMap("orderUserMap")
    List<OrderUser> queryOrderByMerchantID(String merchantID);

}
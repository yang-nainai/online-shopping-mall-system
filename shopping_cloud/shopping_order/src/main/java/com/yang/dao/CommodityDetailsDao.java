package com.yang.dao;

import com.yang.entity.CommodityDetails;
import org.apache.ibatis.annotations.*;

/**
 * 商品图片详情表(CommodityDetails)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommodityDetailsDao {
    /**
     * 根据商品编号 获取商品详情图片
     * @param commodity_id 商品编号
     * @return
     */
    @Select("SELECT * FROM commodity_details WHERE commodity_id = #{commodity_id}")
    @Results({
            @Result(property = "commodityDetailsId", column = "commodity_details_id",id = true),
            @Result(property = "commodityId",column = "commodity_id"),
            @Result(property = "babyDetailImageOne",column = "baby_detail_image_one"),
            @Result(property = "babyDetailImageTwo",column = "baby_detail_image_two"),
            @Result(property = "babyDetailImageThree",column = "baby_detail_image_three"),
            @Result(property = "babyDetailImageFour",column = "baby_detail_image_four"),
            @Result(property = "babyDetailImageFive",column = "baby_detail_image_five"),
            @Result(property = "babyDetailImageSix",column = "baby_detail_image_six"),
            @Result(property = "babyDetailImageSeven",column = "baby_detail_image_seven"),
            @Result(property = "babyDetailImageEight",column = "baby_detail_image_eight"),
            @Result(property = "babyDetailImageNine",column = "baby_detail_image_nine")

    })
    CommodityDetails findDetailsByCommodityId(String commodity_id);

    /**
     * 添加商品详情图片
     * @param commodityDetails
     * @return
     */
    @Insert("INSERT INTO commodity_details(commodity_details_id,commodity_id,baby_detail_image_one,baby_detail_image_two," +
            "baby_detail_image_three,baby_detail_image_four,baby_detail_image_five,baby_detail_image_six,baby_detail_image_seven," +
            "baby_detail_image_eight,baby_detail_image_nine) " +
            "VALUES(#{commodityDetailsId},#{commodityId},#{babyDetailImageOne},#{babyDetailImageTwo}," +
            "#{babyDetailImageThree},#{babyDetailImageFour},#{babyDetailImageFive},#{babyDetailImageSix}," +
            "#{babyDetailImageSeven},#{babyDetailImageEight},#{babyDetailImageNine})")
    Integer insertCommodityDetails(CommodityDetails commodityDetails);
}
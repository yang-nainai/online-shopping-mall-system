package com.yang.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * 商品优惠价格(CommodityEconomy)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommodityEconomyDao {

}
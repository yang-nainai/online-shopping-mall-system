package com.yang.dao;

import com.yang.entity.Commodity;
import com.yang.entity.utils.PageInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 商品表 用来记入一些商品的基本信息(Commodity)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommodityDao {

    /**
     * 通过ID查询单条数据
     * @param commodityId 主键
     * @return 实例对象
     */
    @Select("select * from commodity where commodity_id = #{commodityId}")
    @Results(id = "commodity_dao",value = {
            @Result(property = "commodityId", column = "commodity_id",id = true),
            @Result(property = "commodityName", column = "commodity_name"),
            @Result(property = "commoditySales", column = "commodity_sales"),
            @Result(property = "commodityBirthTime", column = "commodity_birth_time"),
            @Result(property = "commodityTypeList", column = "commodity_id", many = @Many(
                    select = "com.yang.dao.CommodityTypeDao.findCommodityTypeByCommodityId")
            ),
            @Result(property = "commodityDetails",column = "commodity_id",one = @One(
                    select = "com.yang.dao.CommodityDetailsDao.findDetailsByCommodityId"
            )),
            @Result(property = "commodityShow",column = "commodity_id", one = @One(
                    select = "com.yang.dao.CommodityShowDao.findShowByCommodityId"
            )),
            @Result(property = "commoditySkuColorList", column = "commodity_id", many = @Many(
                    select = "com.yang.dao.CommoditySkuColorDao.findAllSkuColorByCommodityId"
            )),
            @Result(property = "commoditySkuSizeList", column = "commodity_id", many = @Many(
                    select = "com.yang.dao.CommoditySkuSizeDao.findAllSizeByCommodityId"
            )),
            @Result(property = "commoditySkuListList", column = "commodity_id", many = @Many(
                    select = "com.yang.dao.CommoditySkuListDao.findAllListByCommodityId"
            )),
            @Result(property = "commodityTag", column = "commodity_id", one = @One(
                    select = "com.yang.dao.CommodityTagDao.queryByCommodityId"
            )),

            @Result(property = "merchantBaby",column = "commodity_id",one = @One(
                    select = "com.yang.dao.MerchantBabyDao.queryByCommodityId"
            ))
    })
    Commodity queryById(String commodityId);

    /**
     * 分页查询商品数据
     *
     * @param pageInfo 页面工具类
     * @return 商品集合
     */
    @Select("SELECT * FROM commodity LIMIT #{now_page},#{page_size}")
    @ResultMap(value = "commodity_dao")
    List<Commodity> limitCommodity(PageInfo pageInfo);

    /**
     * 查询商品总数据条数
     * @return
     */
    @Select("SELECT COUNT(*) FROM commodity")
    Integer find_commoditySum();

    /**
     * 根据用户输入的搜索 获取要查询的信息   --- 模糊查询
     * @param searchName 搜索名称
     * @return
     */
    @Select("SELECT * FROM commodity WHERE commodity_name LIKE #{searchName}")
    @ResultMap(value = "commodity_dao")
    List<Commodity> searchCommodity(String searchName);

    /**
     * 查询所有的商品
     * @return
     */
    @Select("SELECT * FROM commodity")
    @ResultMap(value = "commodity_dao")
    List<Commodity> queryAllCommodity();

    /**
     * 修改商品信息
     * @param commodity
     * @return
     */
    @Update("UPDATE commodity SET " +
            "commodity_name = #{commodityName}," +
            "commodity_birth_time = #{commodityBirthTime} " +
            "WHERE commodity_id = #{commodityId}")
    Integer updateCommodity(Commodity commodity);

    /**
     * 添加商品信息
     * @param commodity
     * @return
     */
    @Insert("INSERT INTO commodity(commodity_id,commodity_name,commodity_sales,commodity_birth_time) " +
            "VALUES(#{commodityId},#{commodityName},#{commoditySales},#{commodityBirthTime})")
    Integer insertCommodity(Commodity commodity);
}
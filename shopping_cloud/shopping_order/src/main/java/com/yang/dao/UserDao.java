package com.yang.dao;

import com.yang.entity.User;
import org.apache.ibatis.annotations.*;

/**
 * 用户表
 * (User)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface UserDao {

    /**
     * 用户登陆
     *
     * @param userName 用户账号
     * @param password 用户密码
     * @return 用户信息
     */
    @Select("SELECT * FROM user WHERE user_name = #{userName} AND user_password = #{password}")
    @Results(id = "userMapper",value = {
            @Result(property = "userId", column = "user_id", id = true),
            @Result(property = "littlePigUsername", column = "user_name"),
            @Result(property = "littlePigPassword", column = "user_password"),
            @Result(property = "userEmail", column = "user_email"),
            @Result(property = "userGender", column = "user_gender"),
            @Result(property = "userPhotoImg", column = "user_photo_img"),
            @Result(property = "userInternetName", column = "user_internet_name"),
            @Result(property = "userRole",column = "user_id",one = @One(
                    select = "com.yang.dao.UserRoleDao.queryByUserId"
            ))
    })
    User userLogin(String userName, String password);

    /**
     * 根据用户编号获取用户信息
     * @param userId 用户编号
     * @return
     */
    @Select("SELECT * FROM user WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "userEmail", column = "user_email"),
            @Result(property = "userGender", column = "user_gender"),
            @Result(property = "userPhotoImg", column = "user_photo_img"),
            @Result(property = "littlePigUsername", column = "user_name"),
            @Result(property = "littlePigPassword", column = "user_password"),
            @Result(property = "userInternetName", column = "user_internet_name"),
            @Result(property = "userRole",column = "user_id",one = @One(
                    select = "com.yang.dao.UserRoleDao.queryByUserId"
            ))
    },id = "userMap")
    User queryById(String userId);

    /**
     * 添加一个新的用户
     * @param user 用户对象
     * @return 1：成功 0：失败
     */
    @Insert("INSERT INTO user" +
            "(user_id,user_name,user_email,user_gender,user_password,user_photo_img,user_internet_name) " +
            "VALUES (#{userId},#{userName},#{userEmail},#{userGender},#{userPassword},#{userPhotoImg},#{userInternetName})")
    Integer insertUser(User user);

    /**
     * 根据用户账号 查询用户
     * @param username
     * @return
     */
    @Select("SELECT * FROM user WHERE user_name = #{username}")
    @Results(value = {
            @Result(property = "userId", column = "user_id", id = true),
            @Result(property = "littlePigUsername",column = "user_name"),
            @Result(property = "littlePigPassword",column = "user_password"),
            @Result(property = "userEmail", column = "user_email"),
            @Result(property = "userGender", column = "user_gender"),
            @Result(property = "userPhotoImg", column = "user_photo_img"),
            @Result(property = "userInternetName", column = "user_internet_name"),
            @Result(property = "userRole",column = "user_id",one = @One(
                    select = "com.yang.dao.UserRoleDao.queryByUserId"
            ))
    })
    User queryByUsername(String username);

    /**
     * 根据用户编号修改用户信息
     * @param user
     * @return
     */
    @Update("UPDATE user SET " +
            "user_email=#{userEmail},user_password = #{littlePigPassword},user_internet_name = #{userInternetName} " +
            "WHERE user_id = #{userId}")
    Integer updateUser(User user);
}
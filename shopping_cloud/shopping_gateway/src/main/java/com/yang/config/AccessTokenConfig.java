package com.yang.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

/**
 * @author yangdacheng
 * @title: AccessTokenConfig
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/27 11:43 上午
 */
@Configuration
public class AccessTokenConfig {

    @Autowired
    private RedisConnectionFactory connectionFactory;

    @Bean
    public TokenStore tokenStore(){
        return new RedisTokenStore(connectionFactory);
    }
}

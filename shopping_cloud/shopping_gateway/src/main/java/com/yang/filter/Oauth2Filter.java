package com.yang.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Objects;

/**
 * @author yangdacheng
 * @title: Oauth2Filter
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/27 5:09 下午
 */
@Configuration
@Slf4j
public class Oauth2Filter implements GlobalFilter {

    @Autowired
    private TokenStore tokenStore;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpResponse response = exchange.getResponse();
        //获取请求路径
        URI uri = exchange.getRequest().getURI();
        String path = uri.getPath();
        if (path.startsWith("/oauth")||path.startsWith("/commodity")){
            //判断是否获取令牌 是放行
            return chain.filter(exchange).then(Mono.fromRunnable(()->{
                log.info("开始获取令牌....");
            }));
        }
        // 1.获取请求参数
        HttpHeaders headers = exchange.getRequest().getHeaders();
        // 2.获取authorization参数
        String authorization = headers.getFirst("Authorization");
        log.info("获取的令牌为:{}",authorization);
        if (Objects.isNull(authorization)){
            log.info("token为空");
            //设置返回消息
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            byte[] bytes = "{\"status\":\"-1\",\"msg\":\"无效token请重新登陆\"}".getBytes(StandardCharsets.UTF_8);
            DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
            return exchange.getResponse().writeWith(Flux.just(buffer));
        }
        String[] authorizationStr = authorization.split(" ");
        OAuth2AccessToken auth2AccessToken = this.tokenStore.readAccessToken(authorizationStr[1]);
        if (Objects.isNull(auth2AccessToken)){
            log.info("token为空！！！");
            byte[] bytes = "{\"status\":\"-1\",\"msg\":\"无效token请重新登陆\"}".getBytes(StandardCharsets.UTF_8);
            DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
            return exchange.getResponse().writeWith(Flux.just(buffer));
        }
        if (auth2AccessToken.isExpired()){
            log.info("token超时");
            byte[] bytes = "{\"status\":\"-1\",\"msg\":\"token过期请重新登陆\"}".getBytes(StandardCharsets.UTF_8);
            DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
            return exchange.getResponse().writeWith(Flux.just(buffer));
        }
        OAuth2Authentication oAuth2Authentication = this.tokenStore.readAuthentication(auth2AccessToken);
        if (oAuth2Authentication == null){
            log.info("无效token");
            byte[] bytes = "{\"status\":\"-1\",\"msg\":\"无效token请重新登陆\"}".getBytes(StandardCharsets.UTF_8);
            DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
            return exchange.getResponse().writeWith(Flux.just(buffer));
        }else{
            return chain.filter(exchange).then(Mono.fromRunnable(()->{
                log.info("token正确....开始跳转到"+exchange.getRequest().getURI()+"请求方式为:"+exchange.getRequest().getMethod().name());
            }));
        }
    }
}

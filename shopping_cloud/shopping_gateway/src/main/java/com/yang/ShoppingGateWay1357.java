package com.yang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author yangdacheng
 * @title: ShoppingGateWay1357
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/27 9:48 上午
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ShoppingGateWay1357 {
    public static void main(String[] args) {
        SpringApplication.run(ShoppingGateWay1357.class,args);
    }
}

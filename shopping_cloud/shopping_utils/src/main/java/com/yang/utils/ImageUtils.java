package com.yang.utils;

/**
 * @author yangdacheng
 * @title: ImageUtils 图片工具类
 * @projectName shopping_api
 * @description: TODO 将图片的后缀进行格式化为 指定格式
 * @date 2021/10/15 8:59 上午
 */
public class ImageUtils {

    /**
     * 格式化图片后缀名称
     * @param imageUrl
     * @return
     */
    public static String settingImageHref(String imageUrl){
        String[] splitName = {"jpg", "jpeg", "png", "gif"};
        for (String s : splitName) {
            int isSplitNameOne = imageUrl.indexOf(s);
            if (isSplitNameOne > 0) {
                String[] split = imageUrl.split(s);
                imageUrl = split[0] + s;
                if (imageUrl.endsWith(s)) {
                    break;
                }
            }
        }
        return imageUrl;
    }
}

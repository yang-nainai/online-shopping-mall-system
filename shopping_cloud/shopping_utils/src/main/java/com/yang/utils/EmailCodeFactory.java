package com.yang.utils;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @author yangdacheng
 * @title: EmailCodeFactory 邮件验证码生成工厂
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/10/19 9:03 上午
 */
@Component
public class EmailCodeFactory {

    /**
     * 生成4位数的验证码
     * @return
     */
    public String buildCode(Integer codeLength){
        String resultCode = "";
        char word_list[] = {'a','b','c','d','e','f','g','h','i','j','k','l',
        'm','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        Random random = new Random();
        for (int index=0;index<codeLength;index++){
            int random_index = random.nextInt(26);
            resultCode += word_list[random_index];
        }
        return resultCode.toUpperCase();
    }
}

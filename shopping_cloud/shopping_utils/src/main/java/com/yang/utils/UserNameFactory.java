package com.yang.utils;

import java.util.Random;

/**
 * @author yangdacheng
 * @title: UserNameFactory 用户账号生成工厂
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/10/20 9:11 上午
 */
public class UserNameFactory {

    public static String userNameBuild(Integer maxNumber,Integer minNumber){
        Random random = new Random();
        int s = random.nextInt(maxNumber)%(maxNumber-minNumber+1) + minNumber;
        return String.valueOf(s);
    }
}

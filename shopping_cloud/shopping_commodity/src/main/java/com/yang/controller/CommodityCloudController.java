package com.yang.controller;

import com.yang.entity.*;
import com.yang.entity.utils.PageInfo;
import com.yang.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author yangdacheng
 * @title: CommodityCloudController 用来操作数据库
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/17 6:16 下午
 */
@RestController
@RequestMapping("/commodity/cloud/dao")
public class CommodityCloudController {

    @Autowired
    private CommodityService commodityService;
    @Autowired
    private CommodityDetailsService commodityDetailsService;
    @Autowired
    private CommodityEconomyService commodityEconomyService;
    @Autowired
    private CommodityShowService commodityShowService;
    @Autowired
    private CommoditySkuColorService commoditySkuColorService;
    @Autowired
    private CommoditySkuListService commoditySkuListService;
    @Autowired
    private CommoditySkuSizeService commoditySkuSizeService;
    @Autowired
    private CommodityTagService commodityTagService;
    @Autowired
    private CommodityTypeService commodityTypeService;
    @Autowired
    private EconomyService economyService;
    @Autowired
    private TagService tagService;
    @Autowired
    private TypeService typeService;

    /**
     * 通过ID查询单条数据
     *
     * @param commodityId 主键
     * @return 实例对象
     */
    @PostMapping("/commodityQueryById")
    public Commodity commodityQueryById(@RequestParam("commodityId") String commodityId){
        return commodityService.queryById(commodityId);
    }

    /**
     * 分页查询商品数据
     *
     * @param pageInfo 页面工具类
     * @return 商品集合
     */
    @PostMapping("/limitCommodity")
    public List<Commodity> limitCommodity(@RequestBody PageInfo pageInfo){
        return commodityService.limitCommodity(pageInfo);
    }

    /**
     * 查询商品总数据条数
     * @return
     */
    @PostMapping("/find_commoditySum")
    public Integer find_commoditySum(){
        return commodityService.find_commoditySum();
    }

    /**
     * 根据用户输入的搜索 获取要查询的信息   --- 模糊查询
     * @param searchName
     * @return
     */
    @PostMapping("/searchCommodity")
    public List<Commodity> searchCommodity(@RequestParam("searchName") String searchName){
        return commodityService.searchCommodity(searchName);
    }

    /**
     * 查询所有的商品
     * @return
     */
    @PostMapping("/queryAllCommodity")
    public List<Commodity> queryAllCommodity(){
        return commodityService.queryAllCommodity();
    }

    /**
     * 修改商品信息
     * @param commodity
     * @return
     */
    @PostMapping("/updateCommodity")
    public Integer updateCommodity(@RequestBody Commodity commodity){
        return commodityService.updateCommodity(commodity);
    }

    /**
     * 添加商品信息
     * @param commodity
     * @return
     */
    @PostMapping("/insertCommodity")
    Integer insertCommodity(@RequestBody Commodity commodity){
        return commodityService.insertCommodity(commodity);
    }

    /**
     * 添加商品详情图片
     * @param commodityDetails
     * @return
     */
    @PostMapping("/insertCommodityDetails")
    public Integer insertCommodityDetails(@RequestBody CommodityDetails commodityDetails){
        return commodityDetailsService.insertCommodityDetails(commodityDetails);
    }


    /**
     * 添加商品展示图片
     * @param commodityShow
     * @return
     */
    @PostMapping("/insertCommodityShow")
    public Integer insertCommodityShow(@RequestBody CommodityShow commodityShow){
        return commodityShowService.insertCommodityShow(commodityShow);
    }


    /**
     * 批量添加商品skuColor
     * @param commoditySkuColorList
     * @return
     */
    @PostMapping("/insertSkuColor")
    public Integer insertSkuColor(@RequestParam("commoditySkuColorList") List<CommoditySkuColor> commoditySkuColorList){
        return commoditySkuColorService.insertSkuColor(commoditySkuColorList);
    }

    /**
     * 根据商品sku的编号获取商品的信息
     * @param skuListId 商品sku编号
     * @return 商品信息
     */
    @PostMapping("")
    public CommoditySkuList queryBySkuListId(@RequestParam("skuListId") String skuListId){
        return commoditySkuListService.queryBySkuListId(skuListId);
    }

    /**
     * 添加商品sku集合
     * @param commoditySkuListList
     * @return
     */
    @PostMapping("/insertSkuList")
    public Integer insertSkuList(@RequestParam("commoditySkuListList") List<CommoditySkuList> commoditySkuListList){
        return commoditySkuListService.insertSkuList(commoditySkuListList);
    }

    /**
     * 添加商品sku大小属性
     * @param commoditySkuSizes
     * @return
     */
    @PostMapping("/insertCommoditySkuSize")
    public Integer insertCommoditySkuSize(@RequestParam("commoditySkuSizes") List<CommoditySkuSize> commoditySkuSizes){
        return commoditySkuSizeService.insertCommoditySkuSize(commoditySkuSizes);
    }

    /**
     * 根据商品的编号来查询商品的类型 一个商品可以有多个类型
     * @param commodityID 商品编号
     * @return 商品类型集合
     */
    @PostMapping("/findCommodityTypeByCommodityId")
    public List<CommodityType> findCommodityTypeByCommodityId(@RequestParam("commodityID") String commodityID){
        return commodityTypeService.findCommodityTypeByCommodityId(commodityID);
    }


    /**
     * 查询所有的类型
     * @return
     */
    @PostMapping("/queryAllTypeCommodity")
    public List<CommodityType> queryAllTypeCommodity(){
        return commodityTypeService.queryAllType();
    }

    /**
     * 添加商品类型
     * @param commodityType
     * @return
     */
    @PostMapping("/insertCommodityType")
    public Integer insertCommodityType(@RequestBody CommodityType commodityType){
        return commodityTypeService.insertCommodityType(commodityType);
    }

    /**
     * 查询所有的商品类型
     * @return
     */
    @PostMapping("/queryAllType")
    public List<Type> queryAllType(){
        return typeService.queryAllType();
    }
}

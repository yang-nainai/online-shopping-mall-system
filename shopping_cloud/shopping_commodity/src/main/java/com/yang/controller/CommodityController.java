package com.yang.controller;

import com.alibaba.fastjson.JSON;
import com.yang.entity.Commodity;
import com.yang.entity.CommodityShow;
import com.yang.entity.Merchant;
import com.yang.entity.MerchantBaby;
import com.yang.entity.utils.HomeAdvertisement;
import com.yang.entity.utils.PageInfo;
import com.yang.service.CommodityService;
import com.yang.service.MerchantService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 商品表 用来记入一些商品的基本信息(Commodity)表控制层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@RestController
@RequestMapping("/commodity/cloud")
@Api(tags = "商品信息接口")
@Slf4j
public class CommodityController {
    /**
     * 服务对象
     */
    @Resource
    private CommodityService commodityService;
    @Resource
    private MerchantService merchantService;
    @Resource
    private RedisTemplate<String,String> redisTemplate;
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne")
    @ApiImplicitParam(paramType = "path",name = "id",value = "订单编号",required = true)
    @ApiOperation(value = "通过主键查询单条数据",notes = "通过主键查询单条数据")
    @ApiResponse(code = 200,message = "商品类")
    public Commodity selectOne(String id) {
        log.info("获取商品的编号为：{}",id);
        return this.commodityService.queryById(id);
    }

    /**
     * 获取 商品总数量
     * @return
     */
    @GetMapping("/getCommoditySum")
    @ApiOperation(value = "商品总数量",notes = "商品总数量")
    @ApiResponses({
            @ApiResponse(code = 200,message = "商品总数量")
    })
    public Integer getCommoditySum(){
        return commodityService.find_commoditySum();
    }

    /**
     * 分页获取商品信息
     * @param nowPage
     * @param commoditySum
     * @return
     */
    @PostMapping("/limit")
    @ApiOperation(value = "分页获取商品信息",notes = "分页获取商品信息")
    @ApiResponses({
            @ApiResponse(code = 200,message = "商品信息集合")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",value = "nowPage",name = "当前页面",required = true),
            @ApiImplicitParam(paramType = "query",value = "commodityTotal",name = "获取数量",required = true)
    })
    public List<Commodity> limitCommodity(@RequestParam("nowPage") Integer nowPage,
                                          @RequestParam("commodityTotal") Integer commoditySum) {
        // 用户显示条数 后端可以修改 可以从数据库导入
        Integer pageSize = 10; // 默认为10条
        int allPages = commoditySum / pageSize;
        if (commoditySum % pageSize != 0) {
            allPages = commoditySum / pageSize;  //不是10 的倍数就将页面大小加1 为了取出所有的数据
            allPages = allPages + 1;
        }
        PageInfo pageInfo = new PageInfo((nowPage * pageSize), allPages, pageSize);
        List<Commodity> commodities = commodityService.limitCommodity(pageInfo);
        // 格式化数据样式
        // 获取商品类
        for (Commodity commodityContext : commodities) {
            CommodityShow commodityShow = commodityContext.getCommodityShow(); //进入商品类中的商品图片类
            // 设置图片
            String[] images = new String[5];
            images[0] = commodityShow.getShowImgOne();
            images[1] = commodityShow.getShowImgTwo();
            images[2] = commodityShow.getShowImgThree();
            images[3] = commodityShow.getShowImgFour();
            images[4] = commodityShow.getShowImgFive();
            commodityShow.setShowImages(images);
            // 添加商家信息
            MerchantBaby merchantBaby = commodityContext.getMerchantBaby();
            if (merchantBaby != null){
                String merchantId = merchantBaby.getMerchantId();
                Merchant merchant = merchantService.queryMerchantById(merchantId);
                commodityContext.setMerchant(merchant);
            }
        }
        return commodities;
    }

    //获取首页广告栏的商品消息
    @GetMapping("/homeAdvertisement")
    public List<HomeAdvertisement> homeAdvertisement(){
        List<String> homeAdvertisementInRedis = redisTemplate.opsForList().range("homeAdvertisement", 0, -1);
        List<HomeAdvertisement> homeAdvertisements = new ArrayList<>();
        if (homeAdvertisementInRedis!=null){
            homeAdvertisementInRedis.forEach((value)->{
                HomeAdvertisement homeAdvertisement = JSON.parseObject(value,HomeAdvertisement.class);
                //获取商家消息
                Merchant merchant = merchantService.queryMerchantById(homeAdvertisement.getCommodity().getMerchantBaby().getMerchantId());
                homeAdvertisement.getCommodity().setMerchant(merchant);
                homeAdvertisements.add(homeAdvertisement);
            });
        }
        return homeAdvertisements;
    }

    //设置轮播图
    @GetMapping("/setHomeAdvertisement")
    public Integer setHomeAdvertisement(String showImg,String commodityID){
        Commodity commodity = commodityService.queryById(commodityID);
        HomeAdvertisement homeAdvertisement = new HomeAdvertisement(showImg,commodity);
        String homeAdvertisementStr = JSON.toJSONString(homeAdvertisement);
        redisTemplate.opsForList().leftPush("homeAdvertisement",homeAdvertisementStr);
        return 1;
    }


    /**
     * 根据用户输入的搜索 获取要查询的信息   --- 模糊查询
     * @param searchName
     * @return
     */
    @GetMapping("/searchCommodity")
    @ApiOperation(value = "根据用户输入的搜索 获取要查询的信息   --- 模糊查询",notes = "根据用户输入的搜索 获取要查询的信息   --- 模糊查询")
    @ApiResponses({
            @ApiResponse(code = 200,message = "商品信息集合")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "String",value = "searchName",name = "搜索名称",required = true)
    })
    public List<Commodity> searchCommodity(String searchName){
        return commodityService.searchCommodity(searchName);
    }

    /**
     * 查询所有的商品
     * @return
     */
    @GetMapping("/queryAllCommodity")
    @ApiOperation(value = "查询所有的商品",notes = "查询所有的商品")
    @ApiResponses({
            @ApiResponse(code = 200,message = "商品信息集合")
    })
    public List<Commodity> queryAllCommodity(){
        return commodityService.queryAllCommodity();
    }

    /**
     * 修改商品信息
     * @param commodity
     * @return
     */
    @GetMapping("/updateCommodity")
    @ApiOperation(value = "修改商品信息",notes = "修改商品信息")
    @ApiResponses({
            @ApiResponse(code = 200,message = "1:成功 0:失败")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "commodity",value = "commodity",name = "商品类型",required = true)
    })
    public Integer updateCommodity(Commodity commodity){
        return commodityService.updateCommodity(commodity);
    }

    /**
     * 添加商品信息
     * @param commodity
     * @return
     */
    @GetMapping("/insertCommodity")
    @ApiOperation(value = "添加商品信息",notes = "添加商品信息")
    @ApiResponses({
            @ApiResponse(code = 200,message = "1:成功 0:失败")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "commodity",value = "commodity",name = "商品类型",required = true)
    })
    public Integer insertCommodity(Commodity commodity){
        return commodityService.insertCommodity(commodity);
    }
}
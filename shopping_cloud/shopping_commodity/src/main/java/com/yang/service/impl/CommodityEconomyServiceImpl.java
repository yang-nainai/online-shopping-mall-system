package com.yang.service.impl;

import com.yang.dao.CommodityEconomyDao;
import com.yang.service.CommodityEconomyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商品优惠价格(CommodityEconomy)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commodityEconomyService")
public class CommodityEconomyServiceImpl implements CommodityEconomyService {
    @Resource
    private CommodityEconomyDao commodityEconomyDao;


}
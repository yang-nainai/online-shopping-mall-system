package com.yang.service;

import com.yang.entity.CommodityShow;

/**
 * 商品图片展示表(CommodityShow)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface CommodityShowService {

    /**
     * 添加商品展示图片
     * @param commodityShow
     * @return
     */
    Integer insertCommodityShow(CommodityShow commodityShow);

}
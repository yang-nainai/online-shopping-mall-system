package com.yang.service;


import com.yang.entity.Merchant;

/**
 * 商家表
(Merchant)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface MerchantService {

    /**
     * 根据商家编号获取商家信息
     * @param merchantId 商家编号
     * @return 商家信息
     */
    Merchant queryMerchantById(String merchantId);

    /**
     * 根据用户编号获取商家信息
     * @param userID
     * @return
     */
    Merchant queryMerchantByUserID(String userID);

    /**
     * 根据商家编号修改商家信息
     * @param merchant
     * @return
     */
    Integer updateMerchant(Merchant merchant);
}
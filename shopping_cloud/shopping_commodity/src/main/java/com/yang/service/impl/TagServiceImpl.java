package com.yang.service.impl;

import com.yang.dao.TagDao;
import com.yang.service.TagService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 标签表
(Tag)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Service("tagService")
public class TagServiceImpl implements TagService {
    @Resource
    private TagDao tagDao;
}
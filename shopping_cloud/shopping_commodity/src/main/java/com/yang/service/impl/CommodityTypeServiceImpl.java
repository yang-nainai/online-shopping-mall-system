package com.yang.service.impl;

import com.yang.dao.CommodityTypeDao;
import com.yang.entity.CommodityType;
import com.yang.service.CommodityTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品类型表用来存放不同商品的类型(CommodityType)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commodityTypeService")
public class CommodityTypeServiceImpl implements CommodityTypeService {
    @Resource
    private CommodityTypeDao commodityTypeDao;

    /**
     * 根据商品的编号来查询商品的类型 一个商品可以有多个类型
     * @param commodityID 商品编号
     * @return 商品类型集合
     */
    @Override
    public List<CommodityType> findCommodityTypeByCommodityId(String commodityID) {
        return this.commodityTypeDao.findCommodityTypeByCommodityId(commodityID);
    }

    @Override
    public List<CommodityType> queryAllType() {
        return this.commodityTypeDao.queryAllType();
    }

    @Override
    public Integer insertCommodityType(CommodityType commodityType) {
        return commodityTypeDao.insertCommodityType(commodityType);
    }
}
package com.yang.service;

import com.yang.entity.CommoditySkuColor;

import java.util.List;

/**
 * 商品的sku属性表（颜色）(CommoditySkuColor)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
public interface CommoditySkuColorService {
    /**
     * 批量添加商品skuColor
     * @param commoditySkuColorList
     * @return
     */
    Integer insertSkuColor(List<CommoditySkuColor> commoditySkuColorList);
}
package com.yang.service.impl;

import com.yang.dao.CommoditySkuColorDao;
import com.yang.entity.CommoditySkuColor;
import com.yang.service.CommoditySkuColorService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品的sku属性表（颜色）(CommoditySkuColor)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commoditySkuColorService")
public class CommoditySkuColorServiceImpl implements CommoditySkuColorService {
    @Resource
    private CommoditySkuColorDao commoditySkuColorDao;

    @Override
    public Integer insertSkuColor(List<CommoditySkuColor> commoditySkuColorList) {
        return commoditySkuColorDao.insertSkuColor(commoditySkuColorList);
    }
}
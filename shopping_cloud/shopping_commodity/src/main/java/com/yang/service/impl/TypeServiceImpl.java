package com.yang.service.impl;

import com.yang.dao.TypeDao;
import com.yang.entity.Type;
import com.yang.service.TypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品类型编号 用来存放一些商品类型(Type)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Service("typeService")
public class TypeServiceImpl implements TypeService {
    @Resource
    private TypeDao typeDao;


    @Override
    public List<Type> queryAllType() {
        return typeDao.queryAllType();
    }
}
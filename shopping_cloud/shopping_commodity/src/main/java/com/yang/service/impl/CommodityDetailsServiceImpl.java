package com.yang.service.impl;

import com.yang.dao.CommodityDetailsDao;
import com.yang.entity.CommodityDetails;
import com.yang.service.CommodityDetailsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商品图片详情表(CommodityDetails)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commodityDetailsService")
public class CommodityDetailsServiceImpl implements CommodityDetailsService {
    @Resource
    private CommodityDetailsDao commodityDetailsDao;

    @Override
    public Integer insertCommodityDetails(CommodityDetails commodityDetails) {
        return commodityDetailsDao.insertCommodityDetails(commodityDetails);
    }
}
package com.yang.service.impl;

import com.yang.dao.CommoditySkuSizeDao;
import com.yang.entity.CommoditySkuSize;
import com.yang.service.CommoditySkuSizeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品的SKU属性--大小(CommoditySkuSize)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commoditySkuSizeService")
public class CommoditySkuSizeServiceImpl implements CommoditySkuSizeService {
    @Resource
    private CommoditySkuSizeDao commoditySkuSizeDao;

    @Override
    public Integer insertCommoditySkuSize(List<CommoditySkuSize> commoditySkuSizes) {
        return commoditySkuSizeDao.insertCommoditySkuSize(commoditySkuSizes);
    }
}
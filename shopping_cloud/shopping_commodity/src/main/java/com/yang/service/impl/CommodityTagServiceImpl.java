package com.yang.service.impl;

import com.yang.dao.CommodityTagDao;
import com.yang.service.CommodityTagService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商品标签表(CommodityTag)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Service("commodityTagService")
public class CommodityTagServiceImpl implements CommodityTagService {
    @Resource
    private CommodityTagDao commodityTagDao;
}
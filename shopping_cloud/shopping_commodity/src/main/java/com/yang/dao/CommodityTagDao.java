package com.yang.dao;

import com.yang.entity.CommodityTag;
import org.apache.ibatis.annotations.*;
/**
 * 商品标签表(CommodityTag)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommodityTagDao {

    @Select("SELECT * FROM commodity_tag WHERE commodity_id = #{commodityId}")
    @Results({
            @Result(property = "commodityTagId", column = "commodity_tag_id", id = true),
            @Result(property = "commodityId", column = "commodity_id"),
            @Result(property = "tagId", column = "tag_id", one = @One(
                    select = "com.yang.dao.TagDao.queryById"
            )),
    })
    CommodityTag queryByCommodityId(String commodityId);
}
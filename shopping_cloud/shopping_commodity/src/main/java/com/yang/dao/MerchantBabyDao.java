package com.yang.dao;

import com.yang.entity.MerchantBaby;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 商家 商品表(MerchantBaby)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface MerchantBabyDao {

    /**
     * 根据商品编号查询 商家信息
     * @param commodityId
     * @return
     */
    @Select("SELECT * FROM merchant_baby WHERE commodity_id = #{commodityId}")
    @Results(id = "merchantBaby",value = {
            @Result(property = "merchantBabyId",column = "merchant_baby_id"),
            @Result(property = "merchantId",column = "merchant_id"),
            @Result(property = "commodityId",column = "commodity_id")
    })
    MerchantBaby queryByCommodityId(String commodityId);


    /**
     * 根据商家宝贝编号获取宝贝信息
     * @param merchantBabyId 商品宝贝编号
     * @return
     */
    @Select("SELECT * FROM merchant_baby WHERE merchant_baby_id =#{merchantBabyId}")
    @Results(value = {
            @Result(property = "merchantBabyId",column = "merchant_baby_id",id = true),
            @Result(property = "merchantId",column = "merchant_id"),
            @Result(property = "merchant",column = "merchant_id",one = @One(
                    select = "com.yang.dao.MerchantDao.queryMerchantById"
            )),
            @Result(property = "commodityId",column = "commodity_id"),
            @Result(property = "commodity",column = "commodity_id",one = @One(
                    select = "com.yang.dao.CommodityDao.queryById"
            )),

    },id = "merchantBabyMapper")
    MerchantBaby queryByMerchantBabyId(String merchantBabyId);


    /**
     * 添加商品 归宿
     * @param merchantBaby
     * @return
     */
    @Insert("INSERT INTO " +
            "merchant_baby(merchant_baby_id,merchant_id,commodity_id) " +
            "VALUES " +
            "(#{merchantBabyId},#{merchantId},#{commodityId})")
    Integer insertBaby(MerchantBaby merchantBaby);


    /**
     * 根据商家编号获取商家所有商品信息
     * @param merchantID 商家编号
     * @return
     */
    @Select("SELECT * FROM merchant_baby WHERE merchant_id = #{merchantID}")
    @ResultMap("merchantBabyMapper")
    List<MerchantBaby> queryMerchantBaby(String merchantID);

    /**
     * 根据商家编号获取商家的商品数量
     * @param merchantID
     * @return
     */
    @Select("SELECT count(*) FROM merchant_baby WHERE merchant_id = #{merchantID}")
    Integer queryMerchantBabyNumber(String merchantID);

    /**
     * 修改商品宝贝
     * @param merchantBaby
     * @return
     */
    @Update("UPDATE merchant_baby SET " +
            "merchant_id = #{merchantId} " +
            "WHERE merchant_baby_id = #{merchantBabyId}")
    Integer updateMerchantBaby(MerchantBaby merchantBaby);

    /**
     * 添加商家商品
     * @param merchantBaby
     * @return
     */
    @Insert("INSERT INTO merchant_baby " +
            "VALUES(#{merchantBabyId},#{merchantId},#{commodityId})")
    Integer insertMerchantBaBy(MerchantBaby merchantBaby);
}
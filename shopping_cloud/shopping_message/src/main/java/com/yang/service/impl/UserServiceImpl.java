package com.yang.service.impl;

import com.yang.dao.UserDao;
import com.yang.entity.User;
import com.yang.service.UserService;
import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 用户表
(User)表服务实现类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Service("userService")
@Slf4j
//UserDetailsService
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    /**
     * 用户登陆
     * @param userName 用户账号
     * @param password 用户密码
     * @return 用户信息
     */
    public User userLogin(String userName, String password){
        return userDao.userLogin(userName, password);
    }


    /**
     * 根据用户编号获取用户信息
     * @param userId 用户编号
     * @return
     */
    public User queryById(String userId){
        return userDao.queryById(userId);
    }
    /**
     * 添加一个新的用户
     * @param user 用户对象
     * @return 1：成功 0：失败
     */
    public Integer insertUser(User user){
        return userDao.insertUser(user);
    }

    /**
     * 根据用户账号 查询用户
     * @param username
     * @return
     */
    public User queryByUsername(String username){
        return userDao.queryByUsername(username);
    }

    @Override
    public Integer updateUser(User user) {
        return userDao.updateUser(user);
    }

//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        log.info("-----登陆------");
//        User user = queryByUsername(username);
//        if (user == null){
//            log.info("用户不存在");
//            throw new UsernameNotFoundException("用户不存在");
//        }
//        log.info("{} ===== {}",user.getUsername(),user.getPassword());
//        return user;
//    }


}
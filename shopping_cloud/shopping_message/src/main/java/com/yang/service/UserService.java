package com.yang.service;

import com.yang.entity.User;

/**
 * 用户表
(User)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
public interface UserService {

    /**
     * 用户登陆
     * @param userName 用户账号
     * @param password 用户密码
     * @return 用户信息
     */
    User userLogin(String userName, String password);


    /**
     * 根据用户编号获取用户信息
     * @param userId 用户编号
     * @return
     */
    User queryById(String userId);

    /**
     * 添加一个新的用户
     * @param user 用户对象
     * @return 1：成功 0：失败
     */
    Integer insertUser(User user);

    /**
     * 根据用户账号 查询用户
     * @param username
     * @return
     */
    User queryByUsername(String username);

    /**
     * 根据用户编号修改用户信息
     * @param user
     * @return
     */
    Integer updateUser(User user);
}
package com.yang.message;

import com.alibaba.fastjson.JSON;
import com.yang.entity.utils.MessageInfo;
import com.yang.utils.MessageXml;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author yangdacheng
 * @title: MyWebSocketServer
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/14 10:20 上午
 */
@ServerEndpoint("/weChart/{userId}")
@Component
@Slf4j
public class MyWebSocketServer {

    @Value("${fileUpload.messageFileUrl}")
    private String fileUrl;

    //用来存放当前在想用户人数
    private static AtomicInteger onlineCount = new AtomicInteger(0);
    //用来存放现在连接的用户
    private static Map<String, Session> clients = new ConcurrentHashMap<>();
    //用来存放一个session对象 通过他来给客户端发送消息
    private Session session;
    //请求的用户编号
    private String userId;
    //用户连接了
    @OnOpen
    public void onOpen(Session session,@PathParam("userId") String userId){
        this.userId = userId;
        this.session = session;
        log.info("有新的客户端连接了:{}",userId);
        clients.put(this.userId,this.session); //将用户连接存放到集合中
        onlineCount.incrementAndGet();  //人数 +1
    }

    @OnClose
    public void onClose(Session session){
        log.info("有用户断开了:{}",userId);
        clients.remove(userId); //将用户移出集合
        onlineCount.decrementAndGet();  //人数 -1
    }

    @OnError
    public void onError(Throwable throwable){
        throwable.printStackTrace();
    }

    @OnMessage
    public void onMessage(String message,Session session){
        log.info("服务端收到客户端消息:{}",message);
        try{
            MessageInfo messageInfo = JSON.parseObject(message,MessageInfo.class); //将发送过来的json文件转换为消息类型
            //将发送的消息写入文件
            //写入两份文件一份 接受者文件 一个发送者文件
//            String basePath = fileUploadBean.getMessageFileUrl();
//            String basePath = "/Users/yangdacheng/Desktop/learn_springboot/littleMessageCache/";
            String basePath = "/home/new_shopping/littleMessageCache/";
            MessageXml.writeTwoMessage(basePath,messageInfo);  //写入两个文件
            /**
             * 1、聊天消息缓存
             * 2、创建消息文件 文件名称： fromUserID TO toUserID.xml
             * 3、修改消息已读状态
             */
            if (messageInfo != null){
                //获取发送消息给谁
                Session toSession = clients.get(messageInfo.getToUserID());
                //发送的用户存在将
                if (toSession != null){
                    //发送消息
                    String returnMessage = JSON.toJSONString(message);
                    log.info(returnMessage);
                    this.sendMessage(returnMessage,toSession);
                }else{
                    //用户不再线将消息缓存
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("解析失败！");
        }
    }

    /**
     * 发送消息
     * @param message
     * @param toSession
     */
    public void sendMessage(String message,Session toSession){
        try {
            log.info("服务端给客户端[{}]发送消息[{}]", toSession.getId(), message);
            toSession.getBasicRemote().sendText(message);
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败：{}", e);
        }
    }
}

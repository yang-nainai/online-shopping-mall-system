package com.yang.utils;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.UUID;

/**
 * @author yangdacheng
 * @title: UploadFileToTenXunYun  上传文件到腾讯云
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/27 11:38 上午
 */
@Component
public class UploadFileToTenXunYun {

    /**
     * 上传文件到腾讯云
     * @param file 文件
     * @param uploadPath 文件上传路径
     * @return
     */
    public static String uploadFile(File file,String uploadPath){
        //1、初始化用户身份信息（secretId, secretKey）
        String secretId = "AKIDVnpa4IGxNWOzJM9gwvAAtQgfNoytXu0p";
        String secretKey = "ZtxkIZZIkwrVsynLccMo0wbz6wV5SaS3";
        COSCredentials credentials = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置 bucket 的地域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region("ap-guangzhou");  //连接到自己服务器 广州
        ClientConfig clientConfig = new ClientConfig(region);
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(credentials, clientConfig);
        // 5、将文件在存放到存储桶中
        // 6、指定文件将要存放的存储桶
        String bucketName = "little-pig-1258768616";
        // 7、指定文件上传对象件
        String fileKey = "/"+uploadPath+"/";
        // 8、将文件存放到存储桶中
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,fileKey+file.getName(),file);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        String returnFileURL = cosClient.getObjectUrl(bucketName,fileKey) + file.getName();
        return returnFileURL;
    }

    /**
     * 文件上传腾讯云
     * @param multipartFile 上传文件
     * @param basePath  基本路径
     * @param uploadPath 上传路径
     * @return 文件路径
     */
    public static String uploadFile(MultipartFile multipartFile, String basePath, String uploadPath){
        if (multipartFile.isEmpty()){
            return null;
        }
        //生成文件随机编号
        String fileUUID = UUID.randomUUID().toString().replace("-","");
        String fileName = multipartFile.getOriginalFilename(); //获取文件名称
        String uploadFilePath = basePath; //文件上传路径
        File file = new File(uploadFilePath+fileUUID+fileName);
        try{
            multipartFile.transferTo(file);
            //将文件存放到腾讯云 存储桶中
            String fileUrl = UploadFileToTenXunYun.uploadFile(file, uploadPath);
            return fileUrl; //文件上传成功
        }catch (Exception e){
            return null;
        }
    }
}

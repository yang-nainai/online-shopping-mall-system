package com.yang.controller;


import com.yang.entity.User;
import com.yang.entity.utils.MessageCache;
import com.yang.entity.utils.MessageMenuInfo;
import com.yang.service.UserService;
import com.yang.utils.MessageXml;
import com.yang.utils.UploadFileToTenXunYun;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author yangdacheng
 * @title: MessageController 消息服务类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/25 4:26 下午
 */
@Api(tags = "消息接口")
@Slf4j
@RestController
@RequestMapping("/message/cloud")
public class MessageController {

    @Resource
    private UserService userService;
    @Value("${fileUpload.baseUrl}")
    private String basePath;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 修改消息是否以读
     *
     * @return
     */
    @PostMapping("/setMessageRead")
    @ApiOperation(value = "修改消息是否以读", notes = "修改消息是否以读")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fromUserID", value = "发送者", required = true),
            @ApiImplicitParam(paramType = "query", name = "toUserID", value = "接收者", required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "是否查看消息成功")
    })
    public Integer setMessageRead(@PathParam("fromUserID") String toUserID,
                                  @PathParam("toUserID") String fromUserID) {
        File file = new File(basePath + "littleMessageCache/" + fromUserID + "TO" + toUserID + ".xml");
        Integer updateMessage = MessageXml.updateMessage(file);
        return updateMessage;
    }

    /**
     * 获取所有的消息内容
     *
     * @return
     */
    @PostMapping("/getAllMessage")
    @ApiOperation(value = "获取所有的消息内容", notes = "获取所有的消息内容")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fromUserID", value = "发送者", required = true),
            @ApiImplicitParam(paramType = "query", name = "toUserID", value = "接收者", required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "消息缓存中的内容")
    })
    public List<MessageCache> getAllMessage(@PathParam("fromUserID") String toUserID,
                                            @PathParam("toUserID") String fromUserID) {
        log.info("获取所有的消息内容");
        File file = new File(basePath + "littleMessageCache/" + fromUserID + "TO" + toUserID + ".xml");
        List<MessageCache> messageCaches = MessageXml.readMessage(file);
        //获取用户头像
        User toUser = userService.queryById(toUserID);
        User fromUser = userService.queryById(fromUserID);
        if (messageCaches!=null){
            Integer showTimeIndex = 0;
            //设置初始值
            for (int index=0;index<messageCaches.size();index++){
                long sendMessageTime = messageCaches.get(showTimeIndex).getSendMessageTime().getTime()/1000/60; //发送消息时间戳
                messageCaches.get(index).setToUserImg(toUser.getUserPhotoImg());
                messageCaches.get(index).setFromUserImg(fromUser.getUserPhotoImg());
                long nowMessageTime = messageCaches.get(index).getSendMessageTime().getTime()/1000/60; //当前消息时间戳
                if (nowMessageTime - sendMessageTime >3){
                    showTimeIndex = index;
                    messageCaches.get(index).setFormatTime(simpleDateFormat.format(messageCaches.get(index).getSendMessageTime()));
                }else if (index == 0){
                    messageCaches.get(showTimeIndex).setFormatTime(simpleDateFormat.format(messageCaches.get(showTimeIndex).getSendMessageTime()));
                }else{
                    messageCaches.get(index).setFormatTime("");
                }
            }
        }
        return messageCaches;
    }

    /**
     * 根据用户编号获取用户聊天对象有哪些
     *
     * @param userID 用户编号
     * @return
     */
    @PostMapping("/getAllMenu")
    @ApiOperation(value = "根据用户编号获取用户聊天对象有哪些", notes = "根据用户编号获取用户聊天对象有哪些")
    @ApiImplicitParam(paramType = "query", name = "userID", value = "用户编号", required = true)
    @ApiResponse(code = 200, message = "用户聊天对象集合")
    private List<MessageMenuInfo> getAllMenu(@RequestParam("userID") String userID) {
        log.info("根据用户编号获取用户聊天对象有哪些");
        log.info("{}",userID);
        List<MessageMenuInfo> messageMenu = MessageXml.getMessageMenu(basePath+"littleMessageCache/", userID);
        for (MessageMenuInfo messageMenuInfo : messageMenu) {
            User user = userService.queryById(messageMenuInfo.getToUserId());
            if (user!=null){
                messageMenuInfo.setMessageUserName(user.getUserInternetName()); //设置用户名称
                messageMenuInfo.setMessageImg(user.getUserPhotoImg()); //设置用户头像
            }
        }
        return messageMenu;
    }

    /**
     * 发送图片上传图片文件
     * @param multipartFile
     * @return
     */
    @PostMapping("/messageImg")
    public String messageFileUpload(@RequestParam("messageImg") MultipartFile multipartFile){
        String filePath = basePath+"littleMessageFile/images/";
        return UploadFileToTenXunYun.uploadFile(multipartFile, filePath, "聊天图片");
    }

    /**
     * 上传聊天文件
     * @param multipartFile
     * @return
     */
    @PostMapping("/messageTextFile")
    public String messageTextFileUpload(@RequestParam("messageFile") MultipartFile multipartFile){
        String filePath = basePath+"littleMessageFile/files/";
        return  UploadFileToTenXunYun.uploadFile(multipartFile, filePath, "聊天文件");
    }


    /**
     * 删除聊天文件
     * @return
     */
    @PostMapping("/deleteMessageFile")
    public Integer deleteMessageFile(@RequestParam("userID") String userID,
                                     @RequestParam("toUserID") String toUserID){
        String filePath = basePath+"littleMessageCache/";
        return MessageXml.deleteMessageFile(filePath, userID, toUserID);
    }
}

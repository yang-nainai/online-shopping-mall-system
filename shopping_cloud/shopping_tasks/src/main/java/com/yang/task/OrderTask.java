package com.yang.task;

import com.yang.entity.OrderBase;
import com.yang.service.OrderBaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author yangdacheng
 * @title: OrderTask  订单自动类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/24 6:00 下午
 */
@Component
@Slf4j
public class OrderTask {

    @Autowired
    private OrderBaseService orderBaseService;

    /**
     * 修改订单状态 模拟发货  没一个小时改变一次状态
     */
    @Scheduled(cron = "0 0 */6 * * ?")
    public void sendCommodity(){
        CopyOnWriteArrayList<OrderBase> orderBases = (CopyOnWriteArrayList<OrderBase>) orderBaseService.queryByOrderState("待发货");
        for (OrderBase orderBase : orderBases){
            orderBaseService.updateOrderState(orderBase.getOrderId(),"待收货");
        }
    }
}

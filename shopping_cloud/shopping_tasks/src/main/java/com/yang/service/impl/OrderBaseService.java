package com.yang.service.impl;

import com.yang.dao.OrderBaseDao;
import com.yang.entity.OrderBase;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author yangdacheng
 * @title: OrderBaseService
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/14 10:14 上午
 */
@Service("orderBaseService")
public class OrderBaseService implements com.yang.service.OrderBaseService {
    @Resource
    private OrderBaseDao orderBaseDao;

    /**
     * 根据订单状态获取订单
     * @param orderState
     * @return
     */
    public List<OrderBase> queryByOrderState(String orderState){
        return orderBaseDao.queryByOrderState(orderState);
    }

    /**
     * 添加一个新的订单编号
     * @param orderBase 订单类
     * @return 1： 添加成功 0：添加失败
     */
    public Integer insertOrder(OrderBase orderBase){
        return orderBaseDao.insertOrder(orderBase);
    }

    /**
     * 根据用户编号和订单编号 修改订单状态
     * @param orderBaseID 订单编号
     * @param orderState 订单状态
     * @return  1：修改成功 0：修改失败
     */
    public Integer updateOrderState(String orderBaseID,String orderState){
        return orderBaseDao.updateOrderState(orderBaseID, orderState);
    }
}

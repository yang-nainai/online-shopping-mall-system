package com.yang.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.yang.entity.AlibabaPayInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yangdacheng
 * @title: AlipayConfig 支付配置类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/17 8:35 上午
 */
@Configuration
public class AlipayConfig {
    // 商户appid
    public static String APPID = "*********";
    // 私钥 pkcs8格式的
    public static String RSA_PRIVATE_KEY = "**********";
    // 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8081/pay_notify";
    // 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
    public static String return_url = "http://localhost:8081/return_url";
    // 请求网关地址 沙盒网关
    public static String URL = "https://openapi.alipaydev.com/gateway.do";
    // 编码
    public static String CHARSET = "UTF-8";
    // 返回格式
    public static String FORMAT = "json";
    // 支付宝公钥
    public static String ALIPAY_PUBLIC_KEY = "**********";
    // 日志记录目录
    public static String log_path = "/log";
    // RSA2
    public static String SIGNTYPE = "RSA2";

    @Bean
    public AlipayClient alipayClient(){
        return new DefaultAlipayClient(URL,APPID,RSA_PRIVATE_KEY,FORMAT,CHARSET,ALIPAY_PUBLIC_KEY,SIGNTYPE);
    }

    /**
     * sdk签名验证
     * @param object
     * @return
     */
    public static boolean checkSign(AlibabaPayInfo alibabaPayInfo) throws IllegalAccessException {
        Map<String, String> paramMap = getParams(alibabaPayInfo);
        System.out.println("=================");
        paramMap.forEach((key,value)->{
            System.out.println(key+"---->"+value);
        });
        System.out.println(paramMap.size());
        //调用sdk签名验证
        try{
            return AlipaySignature.rsaCheckV1(paramMap,AlipayConfig.ALIPAY_PUBLIC_KEY,AlipayConfig.CHARSET,"RSA2");
        }catch (AlipayApiException e) {
            e.printStackTrace();
            System.out.println("===========验签失败===============");
            return false;
        }
    }

    /**
    /**
     * 获取alibabaPayInfo参数
     * @param alibabaPayInfo
     * @return
     */
    public static Map<String,String> getParams(AlibabaPayInfo alibabaPayInfo) throws IllegalAccessException {
//        Map<String,String[]> requestMap = request.getParameterMap();
        Map<String,String> paramMap = new HashMap<>();
        System.out.println("开始解析参数===========");
        Field[] declaredFields = alibabaPayInfo.getClass().getDeclaredFields();
        for (int index = 0; index < declaredFields.length; index++) {
            declaredFields[index].setAccessible(true);
            String name = declaredFields[index].getName();
            Object value = declaredFields[index].get(alibabaPayInfo);
            System.out.println(name+"---->"+value);
            paramMap.put(name,value.toString());
        }
        System.out.println("解析参数结束===========");
//        requestMap.forEach((key,values) -> {
//            String strs = "";
//            for (String value : values){
//                strs += value;
//            }
//            System.out.println(key+"=====>"+strs);
//            paramMap.put(key,strs);
//        });
//        System.out.println();
        return paramMap;
    }
}

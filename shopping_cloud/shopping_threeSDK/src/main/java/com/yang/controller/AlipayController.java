package com.yang.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.yang.config.AlipayConfig;
import com.yang.entity.AlibabaPayInfo;
import com.yang.entity.Commodity;
import com.yang.entity.OrderBase;
import com.yang.entity.OrderUser;
import com.yang.entity.utils.ResponseParamInfo;
import com.yang.service.CommodityService;
import com.yang.service.OrderBaseService;
import com.yang.service.OrderUserService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangdacheng
 * @title: AlipayController 支付宝支付层
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/17 9:04 上午
 */
@RequestMapping("/alipay/cloud")
@RestController
@Slf4j
@Api(tags = "支付宝支付接口")
public class AlipayController {

    /**
     * redis操作对象
     */
    @Resource
    private RedisTemplate<String,String> redisTemplate;
    /**
     * 支付宝接口对象
     */
    @Resource
    private AlipayClient alipayClient;
    /**
     * 商品服务层
     */
    @Resource
    private CommodityService commodityService;
    /**
     * 基础商品订单服务层
     */
    @Resource
    private OrderBaseService orderBaseService;
    /**
     * 用户订单服务层
     */
    @Resource
    private OrderUserService orderUserService;

//    @Resource
//    private UserController userController;

    /**
     * 多件商品支付 & 商品支付
     * @return
     */
    @PostMapping("/morePay")
    @ApiOperation(value = "唤醒移动端支付宝支付",notes = "唤醒移动端支付宝支付")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "baseOrderId",value = "用户基础订单",required = true),
            @ApiImplicitParam(paramType = "query",name = "userOrderId",value = "用户详细订单",required = true),
            @ApiImplicitParam(paramType = "query",name = "userId",value = "用户编号",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "from表单，该表单唤醒支付宝支付")
    })
    public String morePay(@RequestParam("baseOrderId")String baseOrderId,
                          @RequestParam("userOrderId")String  userOrderId,
                          @RequestParam("userId")String userId){
        Map<String,OrderBase> orderBaseMap = new ConcurrentHashMap<>();  //用来存放在redis的基础订单集合
        Map<String,OrderUser> orderUserMap = new ConcurrentHashMap<>();  //用来存放在redis的用户订单集合
        Map<String,Commodity> commodityMap = new ConcurrentHashMap<>();  //用来存放商品信息
        //将用户提交的订单编号变成数组
        String[] baseOrdersId = baseOrderId.split(",");
        String[] userOrdersId = userOrderId.split(",");
        for (int index=0;index<baseOrdersId.length;index++){
            String redisCacheOrder = redisTemplate.opsForValue().get(baseOrdersId[index]);
            //格式化从redis中取出来的数据并且强制转换
            OrderBase orderBase = JSON.parseObject(redisCacheOrder, OrderBase.class);
            log.info("基础订单{}",orderBase.toString());
            orderBaseMap.put(baseOrdersId[index],orderBase);
        }
        for (int index=0;index<userOrdersId.length;index++){
            String redisCacheOrder = redisTemplate.opsForValue().get(userOrdersId[index]);
            OrderUser orderUser = JSON.parseObject(redisCacheOrder,OrderUser.class);
            Commodity commodity = commodityService.queryById(orderUser.getCommodityId());
            commodityMap.put(orderUser.getOrderId(),commodity);
            orderUserMap.put(userOrdersId[index],orderUser);
//            log.info("用户订单{}",orderUser.toString());
        }

        //获取这些订单付款总金额
        float finallyNeedPay = 0;
        for (int index=0;index<baseOrdersId.length;index++){
            OrderBase orderBase = orderBaseMap.get(baseOrdersId[index]);
            finallyNeedPay += (orderBase.getOrderNeedPay() * orderBase.getOrderBuyNum());
        }
        //设置订单编号
        String out_trade_no = baseOrdersId[0];
        //付款金额，必填
        String total_amount = String.valueOf(finallyNeedPay);
        //订单名称，必填 添加字段
        String subject = commodityMap.get(baseOrdersId[0].split(userId)[0]).getCommodityName();
        //商品描述，可空  到时候使用商品的sku list
        String body = subject+"等"+baseOrdersId.length+"件商品";
        // 超时时间 可空
        String timeout_express="2m";
        // 封装请求支付信息
        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
        model.setOutTradeNo(out_trade_no); //设置订单编号
        model.setSubject(subject);//设置订单名称
        model.setTotalAmount(total_amount); //设置订单金额
        model.setBody(body); //设置商品描述
        model.setTimeoutExpress(timeout_express); //设置超时手机

        AlipayTradeWapPayRequest payRequest = new AlipayTradeWapPayRequest();
        payRequest.setBizModel(model);
        //添加异步访问后放回地址
        payRequest.setNotifyUrl(AlipayConfig.notify_url);
        //设置同步地址
        payRequest.setReturnUrl(AlipayConfig.return_url);

        //6、发送请求
        String form="";
        try {
            form = alipayClient.pageExecute(payRequest).getBody(); //调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        //设置订单集合
        for (int index=0;index<baseOrdersId.length;index++){
            redisTemplate.opsForList().leftPush(baseOrdersId[index]+"order",userOrderId);  //将用户基础订单添加到list表中 第一个
            redisTemplate.opsForList().rightPush(baseOrdersId[index]+"order",baseOrderId); //将用户订单添加到list表中 最后一个
        }
        return form;
    }

    /**
     * 回调函数
     * @param request
     * @param response
     * @throws IOException
     */
    @ApiOperation(value = "支付宝回调函数",notes = "支付宝回调函数")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "out_trade_no",value = "订单编号",required = true),
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "commodityName 商品名称"),
            @ApiResponse(code = 200,message = "payMoney 支付金额"),
            @ApiResponse(code = 200,message = "finishTime 完成时间"),
    })
    @PostMapping(value = "/pay_notify")
    public ResponseParamInfo<Object> call(@RequestBody AlibabaPayInfo alibabaPayInfo,
                                          HttpServletRequest request, HttpServletResponse response) throws IOException, IllegalAccessException {
        System.out.println(alibabaPayInfo.toString());

        ResponseParamInfo<Object> responseParamInfo = null;
        String finallyCommodityName = "";
        response.setContentType("type=text/html;charset=UTF-8");
        log.info("支付宝回掉函数被调用");
        if (!AlipayConfig.checkSign(alibabaPayInfo)){
            log.info("验签失败");
//            response.getWriter().write("failure");
            return new ResponseParamInfo<Object>(500,"验签失败",null);
        }
        log.info("success");
        /**
         * 验签成功 -- 证明存在该订单
         * 1、获取订单 输出在缓存中的订单
         * 2、将缓存中订单 添加到数据库
         * 3、放回前端  在放回前端的过程中 用户支付离开了本界面 最坏的方法
         * 在获取用户订单的时候 根据用户编号 获取用户信息 将获取的用户信息放回到前端
         * 前端在将放回的数据进行封装
         */
        Map<String, String> params = AlipayConfig.getParams(alibabaPayInfo);       //获取请求参数
        String out_trade_no = params.get("out_trade_no");  //获取订单号
        System.out.println("订单号："+out_trade_no);
        Map<String,OrderBase> orderBaseMap = new ConcurrentHashMap<>();
        Map<String,OrderUser> orderUserMap = new ConcurrentHashMap<>();
        List<String> orderKeys = redisTemplate.opsForList().range(out_trade_no+"order", 0, -1);  //获取所有订单集合

        for (String orderKey:orderKeys){
            String[] splitKey = orderKey.split(","); //切割后的订单编号
            for (int index=0;index<splitKey.length;index++){
                String cacheOrder = redisTemplate.opsForValue().get(splitKey[index]); //根据订单获取商品订单编号
//                System.out.println("cacheOrder"+cacheOrder);
                //格式化从redis中取出来的数据并且强制转换
                if (cacheOrder.indexOf("orderState") != -1) {
                    OrderBase orderBase = JSON.parseObject(cacheOrder, OrderBase.class);
                    orderBase.setOrderPay(orderBase.getOrderNeedPay()); //设置以支付金额
                    orderBase.setOrderPayState("已支付"); //设置支付状态
                    orderBase.setOrderState("待发货");  //设置商品状态
                    orderBaseMap.put(orderBase.getOrderId(),orderBase); //添加到集合中
                } else {
                    OrderUser orderUser = JSON.parseObject(cacheOrder, OrderUser.class);
                    orderUserMap.put(orderUser.getUserOrderId(),orderUser); //添加到集合中
                    finallyCommodityName = orderUser.getUserOrderId();
                }
                //删除缓存中数据
                redisTemplate.delete(splitKey[index]);
                redisTemplate.delete(splitKey[index]+"order");
            }
        }
        //将基础订单添加到数据库
        orderBaseMap.forEach((key,value) ->{
            orderBaseService.insertOrder(value);
        });
        //将用户订单添加到数据库
        orderUserMap.forEach((key,value)->{
            orderUserService.insertOrderUser(value);
        });
        Commodity commodity = commodityService.queryById(orderUserMap.get(finallyCommodityName).getCommodityId());
        //删除缓存中的值
        redisTemplate.delete(out_trade_no+"order");
        Map<String,String> returnMap = new HashMap<>();
        if (orderBaseMap.size() > 1) {
            returnMap.put("commodityName", commodity.getCommodityName() + "等" + orderBaseMap.size() + "件商品");
        } else {
            returnMap.put("commodityName", commodity.getCommodityName());
        }
        returnMap.put("payMoney",params.get("total_amount"));
        returnMap.put("finishTime",params.get("timestamp"));
        return new ResponseParamInfo(200,"PIG_SUCCESS",returnMap);
    }
}

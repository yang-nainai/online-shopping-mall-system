package com.yang.service;

import com.yang.entity.OrderBase;

import java.util.List;

/**
 * 订单基础表(OrderBase)表服务接口
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
public interface OrderBaseService {

    /**
     * 根据订单状态获取订单
     * @param orderState
     * @return
     */
    List<OrderBase> queryByOrderState(String orderState);

    /**
     * 添加一个新的订单编号
     * @param orderBase 订单类
     * @return 1： 添加成功 0：添加失败
     */
    Integer insertOrder(OrderBase orderBase);


    /**
     * 根据用户编号和订单编号 修改订单状态
     * @param orderBaseID 订单编号
     * @param orderState 订单状态
     * @return  1：修改成功 0：修改失败
     */
    Integer updateOrderState(String orderBaseID,String orderState);
}
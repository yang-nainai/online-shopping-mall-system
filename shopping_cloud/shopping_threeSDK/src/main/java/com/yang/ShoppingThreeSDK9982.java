package com.yang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author yangdacheng
 * @title: ShoppingThreeSDK9982
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/15 9:03 上午
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
@EnableDiscoveryClient
public class ShoppingThreeSDK9982 {
    public static void main(String[] args) {
        SpringApplication.run(ShoppingThreeSDK9982.class,args);
    }
}

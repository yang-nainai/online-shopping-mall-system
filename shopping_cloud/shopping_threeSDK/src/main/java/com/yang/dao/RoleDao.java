package com.yang.dao;

import com.yang.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.*;

/**
 * 角色表
(Role)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface RoleDao {

    /**
     * 根据 角色编号 获取角色信息
     * @param rid
     * @return
     */
    @Select("SELECT * FROM role WHERE role.role_id = #{rid}")
    @Results({
            @Result(property = "roleId",column = "role_id"),
            @Result(property = "roleName",column = "role_name"),
            @Result(property = "roleNameZh",column = "role_name_zh"),
    })
    Role queryByRid(String rid);
}
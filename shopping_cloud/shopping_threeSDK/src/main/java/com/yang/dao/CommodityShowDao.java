package com.yang.dao;

import com.yang.entity.CommodityShow;

import java.util.List;
import org.apache.ibatis.annotations.*;

/**
 * 商品图片展示表(CommodityShow)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommodityShowDao {

    /**
     * 根据商品的编号 获取商品的展示图片
     * @param commodity_id
     * @return
     */
    @Select("SELECT * FROM commodity_show WHERE commodity_id = #{commodity_id}")
    @Results({
            @Result(property = "commodityShowId", column = "commodity_show_id", id = true),
            @Result(property = "commodityId", column = "commodity_id"),
            @Result(property = "showImgOne", column = "show_img_one"),
            @Result(property = "showImgTwo", column = "show_img_two"),
            @Result(property = "showImgThree", column = "show_img_three"),
            @Result(property = "showImgFour", column = "show_img_four"),
            @Result(property = "showImgFive", column = "show_img_five"),
    })
    CommodityShow findShowByCommodityId(String commodity_id);

    @Select("SELECT * FROM commodity_show")
    @Results({
            @Result(property = "commodityShowId", column = "commodity_show_id", id = true),
            @Result(property = "commodityId", column = "commodity_id"),
            @Result(property = "showImgOne", column = "show_img_one"),
            @Result(property = "showImgTwo", column = "show_img_two"),
            @Result(property = "showImgThree", column = "show_img_three"),
            @Result(property = "showImgFour", column = "show_img_four"),
            @Result(property = "showImgFive", column = "show_img_five"),
    })
    List<CommodityShow> findAll();

    @Update("UPDATE commodity_show SET show_img_one = #{showImgOne}," +
            "show_img_two=#{showImgTwo}," +
            "show_img_three=#{showImgThree}," +
            "show_img_four=#{showImgFour}," +
            "show_img_five=#{showImgFive}" +
            "WHERE commodity_show_id=#{commodityShowId}")
    Integer updateImg(CommodityShow commodityShow);

    /**
     * 添加商品展示图片
     * @param commodityShow
     * @return
     */
    @Insert("INSERT INTO commodity_show(commodity_show_id,commodity_id,show_img_one,show_img_two," +
            "show_img_three,show_img_four,show_img_five) " +
            "VALUES(#{commodityShowId},#{commodityId},#{showImgOne},#{showImgTwo},#{showImgThree}," +
            "#{showImgFour},#{showImgFive})")
    Integer insertCommodityShow(CommodityShow commodityShow);
}
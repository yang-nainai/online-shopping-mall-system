package com.yang.dao;

import com.yang.entity.UserCart;

import java.util.List;
import org.apache.ibatis.annotations.*;

/**
 * 用户购物车表
(UserCart)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface UserCartDao {

    /**
     * 根据用户编号获取用户购物车信息
     * @param userId 用户编号
     * @return 一些购物车信息
     */
    @Select("SELECT * FROM user_cart WHERE user_id = #{userId}")
    @Results(id="cartMapper",value = {
            @Result(property = "cartId",column = "cart_id",id = true),
            @Result(property = "merchantBabyId",column = "merchant_baby_id"),
            @Result(property = "merchantBaby",column = "merchant_baby_id",one = @One(
                    select = "com.yang.dao.MerchantBabyDao.queryByMerchantBabyId"
            )),
            @Result(property = "merchantBabyNum",column = "merchant_baby_num"),
            @Result(property = "userId",column = "user_id"),
            @Result(property = "merchantBabyListId",column = "merchant_baby_list_id"),
            @Result(property = "commoditySkuList",column = "merchant_baby_list_id",one = @One(
                    select = "com.yang.dao.CommoditySkuListDao.queryBySkuListId"
            ))
    })
    List<UserCart> findCartByUserId(String userId);

    /**
     * 根据用户编号 获取用户名下购物车内商品数量
     * @param userId 用户编号
     * @return
     */
    @Select("SELECT COUNT(*) FROM user_cart WHERE user_id = #{userId}")
    Integer findAllNumber(String userId);

    /**
     * 根据商品编号查询购物车 判断是否存在
     * @param merchantBabyId 宝贝编号
     * @return
     */
    @Select("SELECT * FROM user_cart WHERE " +
            "merchant_baby_id = #{merchantBabyId} and user_id = #{userId}")
    @ResultMap("cartMapper")
    UserCart queryByMerchantBabyId(String merchantBabyId,String userId);

    /**
     * 修改购物车内中 商品的属性
     * @param cartId 购物车编号
     * @param listId 商品 skulist 集合编号
     * @return 1：成功 0：失败
     */
    @Update("UPDATE user_cart SET " +
            "merchant_baby_list_id = #{listId}," +
            "merchant_baby_num = #{buyNum} " +
            "WHERE cart_id = #{cartId}")
    Integer updateCart(String cartId,String listId,Integer buyNum);

    /**
     * 将商品添加到用户购物车
     * @param userCart
     * @return
     */
    @Insert("INSERT INTO " +
            "user_cart(cart_id,merchant_baby_id,merchant_baby_num,user_id,merchant_baby_list_id) " +
            "VALUES " +
            "(#{cartId},#{merchantBabyId},#{merchantBabyNum},#{userId},#{merchantBabyListId})")
    Integer insertCart(UserCart userCart);

    /**
     * 根据购物车编号 删除购物车
     * @param cartId 购物车编号
     * @return
     */
    @Delete("DELETE FROM user_cart WHERE cart_id = #{cartId}")
    Integer deleteByCartId(String cartId);

}
package com.yang.dao;

import com.yang.entity.UserAddress;

import java.util.List;
import org.apache.ibatis.annotations.*;

/**
 * 用户地址表(UserAddress)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@Mapper
public interface UserAddressDao {

    /**
     * 根据用户的编号获取 用户地址信息
     * @param userId
     * @return
     */
    @Select("SELECT * FROM user_address WHERE user_address_uid = #{userId}")
    @Results(id = "userAddress",value = {
            @Result(property = "userAddressId",column = "user_address_id"),
            @Result(property = "userAddressUid",column = "user_address_uid"),
            @Result(property = "userAddressAid",column = "user_address_aid"),
            @Result(property = "userAddressIsdefault",column = "user_address_isdefault"),
            @Result(property = "address",column = "user_address_aid",one = @One(
                    select = "com.yang.dao.AddressDao.queryById"
            )),
    })
    List<UserAddress> findAllUserAddress(String userId);

    /**
     * 根据用户地址编号 获取用户地址
     * @param userAddressId 用户地址编号
     * @return 用户地址类
     */
    @Select("SELECT * FROM user_address WHERE user_address_id = #{userAddressId}")
    @ResultMap("userAddress")
    UserAddress queryByUserAddressId(String userAddressId);

    /**
     * 根据用户地址编号 获取用户地址
     * @param addressId 地址编号
     * @return 用户地址类
     */
    @Select("SELECT * FROM user_address WHERE user_address_aid = #{addressId}")
    @ResultMap("userAddress")
    UserAddress queryByAddressId(String addressId);

    /**
     * 查询用户的默认地址
     * @param userId 用户编号
     * @return 用户地址信息
     */
    @Select("SELECT * FROM user_address " +
            "WHERE " +
            "user_address_uid = #{userId} " +
            "AND " +
            "user_address_isDefault = #{defaultState}")
    @ResultMap("userAddress")
    UserAddress isDefaultUserAddress(String userId,Integer defaultState);

    /**
     * 根据用户编号 获取用户指定状态地址信息
     * @param userId
     * @param defaultState
     * @return
     */
    @Select("SELECT * FROM user_address WHERE " +
            "user_address_uid = #{userId} AND user_address_isDefault = #{defaultState}")
    @ResultMap("userAddress")
    List<UserAddress> defaultAddress(String userId,Integer defaultState);

    /**
     * 添加一个新的用户地址
     * @param userAddress
     * @return
     */
    @Insert("INSERT INTO " +
            "user_address(user_address_id,user_address_uid,user_address_aid,user_address_isDefault) " +
            "VALUES (#{userAddressId},#{userAddressUid},#{userAddressAid},#{userAddressIsdefault})")
    Integer insertUserAddress(UserAddress userAddress);


    /**
     * 根据用户的编号 和地址的编号来修改地址的 默认状态 改为 0或者为1
     * @param userAddress 用户地址类
     * @return
     */
    @Update("UPDATE user_address SET " +
            "user_address_isDefault = #{userAddressIsdefault} " +
            "WHERE " +
            "user_address_uid = #{userAddressUid} " +
            "AND " +
            "user_address_aid = #{userAddressAid}")
    Integer updateDefaultAddress(UserAddress userAddress);

    /**
     * 根据用户的编号、地址编号修改默认状态
     * @param userId 用户编号
     * @param addressId 地址编号
     * @param defaultState 默认信息 1：默认 0：不是
     * @return 1：修改成功 0：修改失败
     */
    @Update("UPDATE user_address SET " +
            "user_address_isDefault = #{defaultState} " +
            "WHERE " +
            "user_address_uid = #{userId} " +
            "AND " +
            "user_address_aid = #{addressId}")
    Integer updateDefaultState(String userId,String addressId,Integer defaultState);

    /**
     * 更具用户地址编号 和用户编号 删除用户地址
     * @param userId 用户编号
     * @param addressId 地址编号
     * @return 1：删除成功 0：删除失败
     */
    @Delete("DELETE FROM user_address " +
            "WHERE user_address_uid = #{userId} AND user_address_aid = #{addressId}")
    Integer deleteUserAddress(String userId,String addressId);
}
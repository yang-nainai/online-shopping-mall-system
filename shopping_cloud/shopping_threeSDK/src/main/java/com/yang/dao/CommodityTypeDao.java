package com.yang.dao;

import com.yang.entity.CommodityType;

import java.util.List;
import org.apache.ibatis.annotations.*;

/**
 * 商品类型表用来存放不同商品的类型(CommodityType)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommodityTypeDao {
    /**
     * 根据商品的编号来查询商品的类型 一个商品可以有多个类型
     * @param commodityID 商品编号
     * @return 商品类型集合
     */
    @Select("select * from commodity_type where commodity_id = #{commodityID}")
    @Results(value = {
            @Result(property = "commodityTypeId",column = "commodity_type_id",id = true),
            @Result(property = "commodityId",column = "commodity_id"),
            @Result(property = "typeId",column = "type_id"),
            @Result(property = "type",column = "type_id",one = @One(
                    select = "com.yang.dao.TypeDao.queryById"
            ))
    },id = "commodityTypeMap")
    List<CommodityType> findCommodityTypeByCommodityId(String commodityID);

    /**
     * 查询所有的类型
     * @return
     */
    @Select("SELECT * FROM commodity_type")
    @ResultMap("commodityTypeMap")
    List<CommodityType> queryAllType();

    /**
     * 添加商品类型
     * @param commodityType
     * @return
     */
    @Insert("INSERT INTO commodity_type(commodity_type_id,commodity_id,type_id) " +
            "VALUES(#{commodityTypeId},#{commodityId},#{typeId})")
    Integer insertCommodityType(CommodityType commodityType);
}
package com.yang.dao;

import com.yang.entity.CommoditySkuColor;

import java.util.List;
import org.apache.ibatis.annotations.*;

/**
 * 商品的sku属性表（颜色）(CommoditySkuColor)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@Mapper
public interface CommoditySkuColorDao {
    /**
     * 根据商品的编号获取商品的 sku 第一个属性（颜色或者其他）
     * @param commodity_id 商品编号
     * @return
     */
    @Select("SELECT * FROM commodity_sku_color WHERE commodity_id = #{commodity_id}")
    @Results(value = {
            @Result(property = "skuColorId", column = "sku_color_id", id = true),
            @Result(property = "commodityId", column = "commodity_id"),
            @Result(property = "skuKeyName", column = "sku_key_name"),
            @Result(property = "skuColorName", column = "sku_color_name"),
            @Result(property = "skuImgUrl", column = "sku_img_url"),
            @Result(property = "skuPreviewImgUrl", column = "sku_preview_img_url"),
    }, id = "sku_color")
    List<CommoditySkuColor> findAllSkuColorByCommodityId(String commodity_id);

    /**
     * 根据 sku 编号获取 sku 属性
     * @param sku_color_id sku 编号
     * @return 颜色类对象
     */
    @Select("SELECT * FROM commodity_sku_color WHERE sku_color_id = #{sku_color_id}")
    @ResultMap(value = "sku_color")
    CommoditySkuColor find_sku_color_by_id(String sku_color_id);


    @Select("SELECT * FROM commodity_sku_color")
    @ResultMap(value = "sku_color")
    List<CommoditySkuColor> findAll();

    @Update("UPDATE commodity_sku_color SET " +
            "sku_img_url = #{skuImgUrl},sku_preview_img_url =#{skuPreviewImgUrl} " +
            "WHERE sku_color_id = #{skuColorId}")
    Integer updateSkuColor(CommoditySkuColor commoditySkuColor);

    /**
     * 批量添加商品skuColor
     * @param commoditySkuColorList
     * @return
     */
    @Insert({
            "<script>",
            "INSERT INTO commodity_sku_color(sku_color_id, commodity_id,sku_key_name,sku_color_name,sku_img_url," +
                    "sku_preview_img_url) VALUES ",
            // collection 和 value对应   item代表循环中List的这一项IpMap
            "<foreach collection='commoditySkuColorList' item='item' index='index' separator=','>",
                "(#{item.skuColorId}, #{item.commodityId},#{item.skuKeyName},#{item.skuColorName},#{item.skuImgUrl}," +
                        "#{item.skuPreviewImgUrl})",
            "</foreach>",
            "</script>"
    })
    Integer insertSkuColor(List<CommoditySkuColor> commoditySkuColorList);
}
package com.yang.entity.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author yangdacheng
 * @title: MessageMenuInfo 消息界面 消息菜单
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/25 6:49 下午
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "消息菜单列表类",description = "消息菜单列表类")
public class MessageMenuInfo implements Serializable {
    @ApiModelProperty(name = "toUserId",value = "接受者编号")
    private String toUserId; //接受者编号
    @ApiModelProperty(name = "messageUserName",value = "用户消息名称")
    private String messageUserName; //用户消息名称
    @ApiModelProperty(name = "messageImg",value = "用户头像地址")
    private String messageImg; //用户头像地址
    @ApiModelProperty(name = "messageBadge",value = "未读消息数量")
    private Integer messageBadge; //未读消息数量
    @ApiModelProperty(name = "messageLastContext",value = "最后一条消息内容")
    private String messageLastContext; //最后一条消息内容
    @ApiModelProperty(name = "messageTime",value = "消息最后发出时间")
    private String messageTime; //消息最后发出时间

}

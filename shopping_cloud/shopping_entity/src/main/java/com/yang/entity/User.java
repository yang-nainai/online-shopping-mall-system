package com.yang.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.yang.config.CustomAuthorityDeserializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 用户表 (User)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@ApiModel(value = "用户表 (User)实体类",description = "用户表 (User)实体类")
public class User implements Serializable, UserDetails {
    private static final long serialVersionUID = -32221908044370510L;
    /**
    * 用户编号
    */
    @ApiModelProperty(value = "用户编号")
    private String userId;
    /**
    * 用户账号
    */
    @ApiModelProperty(value = "用户账号")
    private String littlePigUsername;
    /**
    * 用户邮箱
    */
    @ApiModelProperty(value = "用户邮箱")
    private String userEmail;
    /**
    * 用户性别（1男 0女）
    */
    @ApiModelProperty(value = "用户性别（1男 0女）")
    private Integer userGender;
    /**
    * 用户密码
    */
    @ApiModelProperty(value = "用户密码")
    private String littlePigPassword;
    /**
    * 用户头像
    */
    @ApiModelProperty(value = "用户头像")
    private String userPhotoImg;
    /**
    * 用户网名
    */
    @ApiModelProperty(value = "用户网名")
    private String userInternetName;

    /**
     * 用户角色
     */
    @ApiModelProperty(value = "用户角色")
    private UserRole userRole;


    @Override
    @JsonDeserialize(using = CustomAuthorityDeserializer.class) //反序列化
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_"+userRole.getRole().getRoleName()));
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.littlePigPassword;
    }

    @Override
    public String getUsername() {
        return this.littlePigUsername;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
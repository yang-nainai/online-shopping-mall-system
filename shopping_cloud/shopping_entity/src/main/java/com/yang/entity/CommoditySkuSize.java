package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 商品的SKU属性--大小(CommoditySkuSize)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@ApiModel(value = "商品的SKU属性--大小(CommoditySkuSize)实体类",description = "商品的SKU属性--大小(CommoditySkuSize)实体类")
public class CommoditySkuSize implements Serializable {
    private static final long serialVersionUID = 535261025094976514L;
    /**
    * 商品sku属性大小的编号
    */
    @ApiModelProperty(value = "商品sku属性大小的编号")
    private String skuSizeId;
    /**
    * 商品sku的规格类目名称（大小/尺码）
    */
    @ApiModelProperty(value = "商品sku的规格类目名称（大小/尺码）")
    private String skuKeyName;
    /**
    * 商品sku属性大小的名称
    */
    @ApiModelProperty(value = "商品sku属性大小的名称")
    private String skuSizeName;
    /**
    * 商品的编号
    */
    @ApiModelProperty(value = "商品的编号")
    private String commodityId;

    public CommoditySkuSize(String skuKeyName, String skuSizeName) {
        this.skuKeyName = skuKeyName;
        this.skuSizeName = skuSizeName;
    }
}
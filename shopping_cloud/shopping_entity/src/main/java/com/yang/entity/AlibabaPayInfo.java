package com.yang.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author yangdacheng
 * @title: AlibabaPayInfo 回掉的实体类
 * @projectName shopping_cloud
 * @description: TODO
 * @date 2022/9/17 10:10 上午
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
@Accessors(chain = true)
public class AlibabaPayInfo {
    private String app_id;
    private String auth_app_id;
    private String charset;
    private String method;
    private String out_trade_no;
    private String seller_id;
    private String sign;
    private String sign_type;
    private String timestamp;
    private String total_amount;
    private String trade_no;
    private String version;
}

package com.yang.entity.utils;

import com.yang.entity.Commodity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author yangdacheng
 * @title: HomeAdvertisement 首页轮播图类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/27 6:48 下午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel(value = "首页轮播图类",description = "首页轮播图类")
public class HomeAdvertisement {
    @ApiModelProperty(value = "fromUserID",name = "首页展示图片")
    private String showImg;
    @ApiModelProperty(value = "fromUserID",name = "商品类")
    private Commodity commodity;
}

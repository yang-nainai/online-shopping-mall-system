package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 商家 商品表(MerchantBaby)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@ApiModel(value = "商家 商品表(MerchantBaby)实体类",description = "商家 商品表(MerchantBaby)实体类")
public class MerchantBaby implements Serializable {
    private static final long serialVersionUID = -11939458055469742L;
    /**
    * 商家商品编号
    */
    @ApiModelProperty(value = "商家商品编号")
    private String merchantBabyId;
    /**
    * 商家编号
    */
    @ApiModelProperty(value = "商家编号")
    private String merchantId;
    /**
    * 商品编号
    */
    @ApiModelProperty(value = "商品编号")
    private String commodityId;
    /**
     * 商品信息
     */
    @ApiModelProperty(value = "商品信息")
    private Commodity commodity;
    /**
     * 商家信息
     */
    @ApiModelProperty(value = "商家信息")
    private Merchant merchant;
    /**
     * 宝贝的sku属性 用来封装购物车
     */
    @ApiModelProperty(value = "宝贝的sku属性 用来封装购物车")
    private CommoditySkuList commoditySkuList;
    /**
     * 宝贝购买的数量 用来封装购物车
     */
    @ApiModelProperty(value = "宝贝购买的数量 用来封装购物车")
    private Integer merchantBabyNum;
    /**
     * 购物车编号 用来封装购物车
     */
    @ApiModelProperty(value = "购物车编号 用来封装购物车")
    private String cartId;

}
package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 商品图片展示表(CommodityShow)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@ApiModel(value = "商品图片展示表(CommodityShow)实体类",description = "商品图片展示表(CommodityShow)实体类")
public class CommodityShow implements Serializable {
    private static final long serialVersionUID = -28852866680634756L;
    /**
    * 商品图片展示编号
    */
    @ApiModelProperty(value = "商品图片展示编号")
    private String commodityShowId;
    /**
    * 商品图片展示1
    */
    @ApiModelProperty(value = "商品图片展示1")
    private String showImgOne;
    /**
    * 商品图片展示2
    */
    @ApiModelProperty(value = "商品图片展示2")
    private String showImgTwo;
    /**
    * 商品图片展示3
    */
    @ApiModelProperty(value = "商品图片展示3")
    private String showImgThree;
    /**
    * 商品图片展示4
    */
    @ApiModelProperty(value = "商品图片展示4")
    private String showImgFour;
    /**
    * 商品图片展示5
    */
    @ApiModelProperty(value = "商品图片展示5")
    private String showImgFive;
    /**
    * 商品编号
    */
    @ApiModelProperty(value = "商品编号")
    private String commodityId;

    @ApiModelProperty(value = "商品图片数组")
    private String[] showImages;

   }
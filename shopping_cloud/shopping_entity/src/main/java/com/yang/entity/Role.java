package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 角色表 (Role)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "角色表 (Role)实体类",description = "角色表 (Role)实体类")
public class Role implements Serializable {
    private static final long serialVersionUID = -92939982762735325L;
    /**
    * 角色编号
    */
    @ApiModelProperty(value = "角色编号")
    private String roleId;
    /**
    * 角色英文名称
    */
    @ApiModelProperty(value = "角色英文名称")
    private String roleName;
    /**
    * 角色中文名称
    */
    @ApiModelProperty(value = "角色中文名称")
    private String roleNameZh;
}
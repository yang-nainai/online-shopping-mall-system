package com.yang.entity.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author yangdacheng
 * @title: CommodityNumbers
 * @projectName shopping_api
 * @description: TODO
 * @date 2022/5/26 10:59 上午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CommodityNumbers implements Serializable {
    private String inputLabel;
    private Integer inputValue;

}

package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户地址表(UserAddress)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "用户地址表(UserAddress)实体类",description = "用户地址表(UserAddress)实体类")
public class UserAddress implements Serializable {
    private static final long serialVersionUID = -20647699451541515L;
    /**
    * 用户地址编号
    */
    @ApiModelProperty(value = "用户地址编号")
    private String userAddressId;
    /**
    * 用户编号
    */
    @ApiModelProperty(value = " 用户编号")
    private String userAddressUid;
    /**
    * 地址编号
    */
    @ApiModelProperty(value = "地址编号")
    private String userAddressAid;
    /**
    * 是否为默认地址（1为默认  0不是）
    */
    @ApiModelProperty(value = "是否为默认地址（1为默认  0不是）")
    private int userAddressIsdefault;

    /**
     * 地址类 存放地址信息
     */
    @ApiModelProperty(value = "地址类 存放地址信息")
    private Address address;
}
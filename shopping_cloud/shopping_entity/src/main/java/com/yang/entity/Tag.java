package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 标签表 (Tag)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "标签表 (Tag)实体类")
public class Tag implements Serializable {
    private static final long serialVersionUID = 191515260209749150L;
    /**
    * 标签编号
    */
    @ApiModelProperty(value = "标签编号")
    private String tagId;
    /**
    * 标签名称
    */
    @ApiModelProperty(value = "标签名称")
    private String tagName;
    /**
    * 标签背景颜色
    */
    @ApiModelProperty(value = "标签背景颜色")
    private String tagBackground;
    /**
    * 标签字体颜色
    */
    @ApiModelProperty(value = "标签字体颜色")
    private String tagFontColor;
}
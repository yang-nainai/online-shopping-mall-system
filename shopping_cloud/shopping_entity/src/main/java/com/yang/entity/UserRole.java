package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户角色表 (UserRole)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "用户角色表 (UserRole)实体类",description = "用户角色表 (UserRole)实体类")
public class UserRole implements Serializable {
    private static final long serialVersionUID = 417747737976365387L;
    /**
    * 用户角色编号
    */
    @ApiModelProperty(value = " 用户角色编号")
    private String userRoleId;
    /**
    * 用户编号
    */
    @ApiModelProperty(value = "用户编号")
    private String userRoleUid;
    /**
    * 用户角色编号
    */
    @ApiModelProperty(value = "用户角色编号")
    private String userRoleRid;

    /**
     * 用户 角色类
     */
    @ApiModelProperty(value = "用户 角色类")
    private Role role;
}
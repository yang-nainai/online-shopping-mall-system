package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 商品类型表用来存放不同商品的类型(CommodityType)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:57
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@ApiModel(value = "商品类型表用来存放不同商品的类型(CommodityType)实体类",description = "商品类型表用来存放不同商品的类型(CommodityType)实体类")
public class CommodityType implements Serializable {
    private static final long serialVersionUID = 673080159352697297L;
    /**
    * 商品类型编号
    */
    @ApiModelProperty(value = "商品类型编号")
    private String commodityTypeId;
    /**
    * 商品编号
    */
    @ApiModelProperty(value = "商品编号")
    private String commodityId;
    /**
    * 类型编号
    */
    @ApiModelProperty(value = "类型编号")
    private String typeId;

    /**
     * 商品类型
     */
    @ApiModelProperty(value = "商品类型")
    private Type type;

    public CommodityType(String typeId) {
        this.typeId = typeId;
    }
}
package com.yang.entity.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yangdacheng
 * @title: MessageFormat 消息格式类 用来接收发送和接收的数据
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/16 9:38 上午
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessageInfo {
    private String sendMessage; //发送过来的消息
    private String fromUserID; //发送过来的用户编号
    private String toUserID;   //发送给谁的用户编号
    private String messageType; //消息类型
    private String fileName; //文件名称
}

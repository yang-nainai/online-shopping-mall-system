package com.yang.entity.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yangdacheng
 * @title: MessageCache 消息缓存类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/25 11:17 上午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel(value = "消息缓存类",description = "消息缓存类")
public class MessageCache implements Serializable {
    @ApiModelProperty(value = "fromUserID",name = "发送消息人的编号")
    private String fromUserID;
    @ApiModelProperty(value = "toUserID",name = "接收消息人名称")
    private String toUserID;
    @ApiModelProperty(value = "sendMessage",name = "发送消息内容")
    private String sendMessage;
    @ApiModelProperty(value = "sendMessageTime",name = "发送消息时间")
    private Date sendMessageTime;
    @ApiModelProperty(value = "messageAreRead",name = "消息是否已读")
    private Boolean messageAreRead;
    @ApiModelProperty(value = "toUserImg",name = "接收消息用户头像")
    private String toUserImg;
    @ApiModelProperty(value = "fromUserImg",name = "发送消息用户头像")
    private String fromUserImg;
    @ApiModelProperty(value = "formatTime",name = "格式话后的时间")
    private String formatTime;
    @ApiModelProperty(value = "messageType",name = "消息类型")
    private String messageType;
    @ApiModelProperty(value = "fileName",name = "文件名称")
    private String fileName;

    public MessageCache(String fromUserID, String toUserID, String sendMessage, Date sendMessageTime, Boolean messageAreRead,String messageType,String fileName) {
        this.fromUserID = fromUserID;
        this.toUserID = toUserID;
        this.sendMessage = sendMessage;
        this.sendMessageTime = sendMessageTime;
        this.messageAreRead = messageAreRead;
        this.messageType = messageType;
        this.fileName = fileName;
    }

    public MessageCache(String fromUserID, String toUserID, String sendMessage, Date sendMessageTime, Boolean messageAreRead) {
        this.fromUserID = fromUserID;
        this.toUserID = toUserID;
        this.sendMessage = sendMessage;
        this.sendMessageTime = sendMessageTime;
        this.messageAreRead = messageAreRead;
    }
}

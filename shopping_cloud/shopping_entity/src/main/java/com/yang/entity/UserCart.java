package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 用户购物车表 (UserCart)实体类
 *
 * @author makejava
 * @since 2021-10-09 10:31:58
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "用户购物车表 (UserCart)实体类",description = "用户购物车表 (UserCart)实体类")
public class UserCart implements Serializable {
    private static final long serialVersionUID = 140898278221296011L;
    /**
    * 购物车编号
    */
    @ApiModelProperty(value = "购物车编号")
    private String cartId;
    /**
    * 对应的商家商品编号（可获取商品信息和商家信息）
    */
    @ApiModelProperty(value = "对应的商家商品编号（可获取商品信息和商家信息）")
    private String merchantBabyId;
    /**
    * 商品购买数量
    */
    @ApiModelProperty(value = "商品购买数量")
    private Integer merchantBabyNum;
    /**
    * 用户编号(谁的购物车)
    */
    @ApiModelProperty(value = "用户编号(谁的购物车)")
    private String userId;
    /**
     * 用户选择的sku list编号
     */
    @ApiModelProperty(value = " 用户选择的sku list编号")
    private String merchantBabyListId;
    /**
     * 宝贝详情
     */
    @ApiModelProperty(value = "宝贝详情")
    private List<MerchantBaby> merchantBaby;

    /**
     * 宝贝的sku属性
     */
    @ApiModelProperty(value = "宝贝的sku属性")
    private CommoditySkuList commoditySkuList;
}
package com.yang.entity.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author yangdacheng
 * @title: PageInfo 分页类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/10/12 3:56 下午
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageInfo implements Serializable {
    private static final long serialVersionUID = -80510401875703722L;

    /**
     * 当前页面
     */
    private Integer now_page;

    /**
     * 总页数
     */
    private Integer all_pages;

    /**
     * 页面大小
     */
    private Integer page_size;


}

package com.yang.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author yangdacheng
 * @title: UserSearchHistory 用户历史搜索记录类
 * @projectName shopping_api
 * @description: TODO
 * @date 2021/11/11 2:59 下午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "用户历史搜索记录类",description = "用户历史搜索记录类")
public class UserSearchHistory implements Serializable {
    @ApiModelProperty(value = "用户编号")
    private String userID; //用户编号
    @ApiModelProperty(value = "用户搜索记录")
    private String userSearchHistory; //用户搜索记录
}
